<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdPrerequisiteToTopicContent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_vault')->table('topic_content', function (Blueprint $table) {
            $table->increments('id')->first();
            $table->string('prerequisites')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_vault')->table('topic_content', function (Blueprint $table) {
            $table->dropColumn('id');
            $table->dropColumn('prerequisites');
        });
    }
}
