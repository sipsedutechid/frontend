@extends('layouts.front')

@section('content')
	
	<section class="cover pt-0" style="background-image: url('{{ asset('images/pit-pogi-cover.jpg') }}')">
		
		@include('layouts.navbar', ['inverseNavbar' => true])
		<div class="container pt-lg-0 py-5 text-white">
		
			<div class="row pt-5 mt-5 justify-content-center">
				<div class="col-md-8 mt-lg-0 mt-5 d-lg-none">
					<img src="{{ asset('images/pit-pogi-logo-white.svg') }}" class="w-100" alt="Logo PIT-POGI XXIII" />
				</div>
				<div class="col-md-8 mt-lg-0 mt-5">
					<h1 class="display-4 d-none d-lg-block">PIT-POGI XXIII Makassar</h1>
					<p class="lead text-lg-left text-center">
						Nonton dan unduh materi simposium Pertemuan Ilmiah Tahunan POGI ke-23 disini!
					</p>
				</div>
				<div class="col-lg-4 d-none d-lg-block">
					<img src="{{ asset('images/pit-pogi-logo-white.svg') }}" class="w-100" alt="Logo PIT-POGI XXIII" />
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container">
			<div class="row">
				@foreach ($videos as $video)
					<div class="col-lg-4 col-md-6 mb-5">
						<div class="mb-3">
							<a href="{{ route('pit-pogi.video', ['slug' => $video->name]) }}" title="Nonton video">
								<img src="{{ $video->poster }}" class="mr-3 rounded mb-lg-0 mb-3 w-100" style="border: #eee thin solid;" />
							</a>
						</div>
						<div>
							<div href="#" class="h4 m-0 text-truncate">{{ $video->title }}</div>
							<div class="small text-truncate">{{ $video->lecturer }}</div>
							<div class="small text-truncate">{{ $video->venue }}, {{ \Carbon\Carbon::parse($video->date)->format('j M, H:i') }}</div>
							<div class="mt-3 row no-gutters">
								<div class="col-6 pr-1">
									<a class="btn btn-block btn-outline-primary" href="{{ route('pit-pogi.video', ['slug' => $video->name]) }}"><i class="fa fa-play"></i> Nonton Video</a>
								</div>
								<div class="col-6 pl-1">
									<a class="btn btn-block btn-outline-secondary" target="_blank" href="{{ route('pit-pogi.file', ['slug' => $video->name]) }}"><i class="fa fa-file"></i> Unduh Materi</a>
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</section>

	<section>
		<div class="container d-flex justify-content-center">
			<a href="http://www.idionline.org/" class="mr-md-5 mr-2">
				<img src="{{ asset('images/idi-logo.svg') }}" height="80" alt="Ikatan Dokter Indonesia" />
			</a>
			<a href="http://unhas.ac.id/" class="mr-md-5 mr-2">
				<img src="{{ asset('images/unhas-logo.svg') }}" height="80" alt="Universitas Hasanuddin" />
			</a>
			<a href="http://pogi.or.id/" class="mr-md-5 mr-2">
				<img src="{{ asset('images/pogi-logo.svg') }}" height="80" alt="Perkumpulan Obstetri dan Ginekologi Indonesia" />
			</a>
			<a href="http://pogimakassar.com/pitpogi23/">
				<img src="{{ asset('images/pit-pogi-logo-min.svg') }}" height="80" alt="PIT-POGI XXIII Makassar" />
			</a>
		</div>
	</section>

@endsection