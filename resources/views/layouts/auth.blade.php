<!DOCTYPE html>
<html lang="id-ID">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <link rel="stylesheet" type="text/css" href="{{ mix('css/front.css') }}">

    <link rel="icon" href="{{ asset('images/icon.png') }}">
    <link rel="icon" sizes="48x48" href="{{ asset('images/icon@0,25x.png') }}">
    <link rel="icon" sizes="96x96" href="{{ asset('images/icon@0,5x.png') }}">
    <link rel="icon" sizes="144x144" href="{{ asset('images/icon@0,75x.png') }}">

    <link rel="apple-touch-icon" href="{{ asset('images/icon.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/touch-icon-ipad.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/touch-icon-iphone-retina.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/touch-icon-ipad-retina.png') }}">

    <meta name="msapplication-square70x70logo" content="{{ asset('images/icon_smalltile.png') }}">
    <meta name="msapplication-square150x150logo" content="{{ asset('images/icon_mediumtile.png') }}">

    <meta name="theme-color" content="#a4151a">
</head>
<body>

    <div class="container" style="height: 100vh;">

        @yield('content')
    </div>

</body>
</html>