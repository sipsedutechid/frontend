<!DOCTYPE html>
<html lang="id-ID">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name') }}</title>

	<link rel="stylesheet" type="text/css" href="{{ mix('css/front.css') }}">

	<link rel="icon" href="{{ asset('images/icon.png') }}">
	<link rel="icon" sizes="48x48" href="{{ asset('images/icon@0,25x.png') }}">
	<link rel="icon" sizes="96x96" href="{{ asset('images/icon@0,5x.png') }}">
	<link rel="icon" sizes="144x144" href="{{ asset('images/icon@0,75x.png') }}">

	<link rel="apple-touch-icon" href="{{ asset('images/icon.png') }}">
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/touch-icon-ipad.png') }}">
	<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/touch-icon-iphone-retina.png') }}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/touch-icon-ipad-retina.png') }}">

	<meta name="msapplication-square70x70logo" content="{{ asset('images/icon_smalltile.png') }}">
	<meta name="msapplication-square150x150logo" content="{{ asset('images/icon_mediumtile.png') }}">

	<meta name="theme-color" content="#a4151a">
</head>
<body>

	@yield('content')

	@include('layouts.footer')

	@if (!Auth::check())
		@include('auth.login-dialog')
	@endif

	<script type="text/javascript" src="{{ mix('js/front.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			$("[data-toggle=auth-dialog]").on('click', function (e) {
				e.preventDefault();
				$("#auth-dialog-social, #auth-dialog-email").toggleClass('hidden-xl-down');
			});
		});
	</script>
	@if (env('APP_ENV') == 'production')
		<!--Start of Zendesk Chat Script-->
		{{-- <script type="text/javascript">
			window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set._.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");$.src="https://v2.zopim.com/?4skLxMDjkpWOEU3uSrAbWoid3b68AOuN";z.t=+new Date;$.type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
		</script> --}}
		<!--End of Zendesk Chat Script-->
	@endif
	@if (env('GOOGLE_ANALYTICS_ID', false))
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', '{{ env('GOOGLE_ANALYTICS_ID') }}', 'auto');
		  @if (auth()->check())
		  	ga('set', 'userId', {{ auth()->user()->id }});
		  @endif
		  ga('send', 'pageview');

		</script>
	@endif
	@yield('scripts')

</body>
</html>