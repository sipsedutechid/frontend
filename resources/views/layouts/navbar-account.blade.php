@if (auth()->check())
	<div class="d-flex align-items-center mr-lg-0 mr-3">
		<a href="{{ route('profile') }}" class="mr-2">
			<img src="{{ auth()->user()->avatar_url ?: asset('images/user.svg') }}" class="navbar-profile rounded-circle" />
		</a>
		<a class="nav-link p-0 d-none d-lg-block" href="{{ route('profile') }}">{{ auth()->user()->name }}</a>
	</div>
@else
	<button type="button" class="btn btn-primary h6 m-0 text-uppercase px-4 py-2" data-toggle="modal" data-target="#auth-dialog">Masuk <b class="fa fa-angle-right"></b></button>
@endif
