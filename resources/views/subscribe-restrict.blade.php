@extends('layouts.front')

@section('content')
	
	@include('layouts.navbar-simple')
	<div class="container mt-4">
		<div class="py-5 d-lg-none"></div>
		
		<h1 class="display-4 text-center mb-4 pt-xl-5 mt-xl-5 d-none d-md-block">Anda sudah berlangganan.</h1>
		<h1 class="d-md-none">Anda sudah berlangganan.</h1>

		<div class="row justify-content-center pb-xl-5 mb-xl-5">
			<div class="col-lg-8 col-md-10 text-md-center">
				
				<p class="mb-5">Saat ini Anda sudah berlangganan Layanan{{ $subscription->subscription->is_trial ? " Trial" : '' }} Gakken 
				{{ auth()->user()->profession == 'doctor' ? 'P2KB' : 'P3KGB' }}&reg;
				di Gakken Indonesia hingga {{ \Carbon\Carbon::parse($subscription->expired_at)->format('j M Y') }}.</p>

				<p><a href="{{ route('app', ['route' => '/']) }}" class="btn btn-primary py-3 h6 text-uppercase">Mulai Belajar <b class="fa fa-angle-right"></b></a></p>

			</div>
		</div>

	</div>

@endsection