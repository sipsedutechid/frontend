@component('mail::message')
# Halo, {{ $order->user->name }}!

Ini adalah pemberitahuan bahwa Anda telah memesan pembelian
sertifikat di Gakken Indonesia. Segera lakukan pembayaran
untuk mendapatkan sertifikat Anda. Berikut rincian pemesanan
Anda.

Nomor Pesanan: {{ $order->order_no }}<br />
Metode Pembayaran: {{ $order->payment_method_label }}<br />
Jumlah Tagihan: Rp. {{ number_format($order->total, 0, '.', ',') }}<br />
Jatuh Tempo: {{ \Carbon\Carbon::parse($order->expired_at)->format('d/m/Y H:i') }}

@component('mail::table')
| Rincian | Jumlah (Rp) |
|:------- | -----------:|
| {{ $order->user_topic_id ? "Sertifikat " . $order->userTopic->topic->title : "Langganan " . $order->subscription->label }} | {{ number_format($order->total, 0, '.', ',') }} |
| Total   | {{ number_format($order->total, 0, '.', ',') }} |
@endcomponent

@if ($order->method == 'banktransfer')
Silakan melakukan transfer ke nomor rekening Bank Negara
Indonesia (BNI) 46: {{ $order->va_no }} sebanyak total yang tertera
diatas dan Anda dapat langsung mengakses sertifikat Anda.
@else
Silakan menyelesaikan pembayaran Anda dan sertifikat Anda dapat langsung diakses.
@endif

Pesanan akan otomatis dibatalkan jika melewati batas jatuh tempo.
Jika sertifikat Anda belum dapat diakses setelah melakukan pembayaran,
harap menghubungi kami melalui email ke support@gakken-idn.co.id, call
center 0800-1-401652, atau layanan chat di
<a href="{{ route('home') }}">situs Gakken</a>.

Terima kasih.

@endcomponent
