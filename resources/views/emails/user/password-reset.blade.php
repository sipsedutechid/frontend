@component('mail::message')
# Halo, {{ $user->name }}

Anda baru saja meminta kami untuk memulihkan kata sandi untuk akun 
Anda {{ $user->email }}. Untuk memulihkan kata sandi Anda, klik
tombol berikut dan ikuti petunjuk selanjutnya.

@component('mail::button', ['url' => route('password.reset', ['token' => $token])])

Pulihkan Kata Sandi
    
@endcomponent

Jika Anda tidak mengetahui aktivitas ini, mohon abaikan pesan ini.

Terima kasih.
@endcomponent
