@component('mail::message')

# Halo, {{ $user->name }}!

Akun Anda telah diverifikasi. Sekarang, Anda dapat membeli paket
berlangganan layanan Gakken Indonesia!

Dengan berlangganan layanan Gakken Indonesia, Anda dapat mengakses
topik pembelajaran P2KB bernilai poin SKP, jurnal premium Wiley dan
database rincian obat lengkap.

@component('mail::button', ['url' => route('subscribe')])

Berlangganan
    
@endcomponent

Terima kasih telah bergabung! Selamat belajar! 

@endcomponent