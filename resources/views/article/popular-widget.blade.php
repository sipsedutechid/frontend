<div>
	<div class="h6 text-uppercase">Artikel Populer</div>
	<hr />
	@foreach ($populars as $pop)
		<div class="mb-2">
			<div class="text-truncate"><strong><a href="{{ route('article.single', ['slug' => $pop->post->post_name]) }}">{{ $pop->post->post_title }}</a></strong></div>
			<div class="ellipsis-3 small">
				{!! clean($pop->post->subheading->content) !!}
			</div>
		</div>
	@endforeach
</div>