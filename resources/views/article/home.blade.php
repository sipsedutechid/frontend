@extends('layouts.front')

@section('content')
	
	@include('layouts.navbar')
	<div class="container mt-4">
		<div class="py-5 d-lg-none"></div>

		<div class="row">
			<div class="col-lg-8 mb-5">

				<div class="article-list-group mb-5">
					@include('article.list')
				</div>

				{!! $articles->links() !!}

			</div>

			<div class="col-lg-4">
				<div class="mb-5 d-none d-lg-block">
					<div class="small text-muted text-uppercase">{{ \Carbon\Carbon::now()->format('l, j M Y') }}</div>
				</div>
				<div class="mb-5">
					@include('article.category-widget')
				</div>
				<div class="mb-5">
					@include('article.popular-widget')
				</div>
			</div>
		</div>


	</div>

@endsection

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function () {
			$("[data-toggle=categories]").on('click', function (e) {
				e.preventDefault();
				$(".categories-list li").removeClass('d-none');
				$(this).remove();
			});
		});
	</script>
@endsection