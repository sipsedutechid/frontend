@foreach ($articles as $article)

	<div class="card p-4 mb-3 article-list-group-item">
		<div class="text-muted small text-uppercase mb-2">
			<span><b class="fa fa-clock-o"></b> {{ \Carbon\Carbon::parse($article->post_date)->format('j M Y') }}</span>
			<span class="pl-2"><b class="fa fa-pencil"></b> {{ $article->author->display_name }}</span>
		</div>
		<a href="{{ route('article.single', ['slug' => $article->post_name]) }}">
			@if ($file = $article->featuredImageSizeFile('medium_large'))
				<div class="d-flex align-items-center rounded mb-4" style="max-height: 200px; overflow: hidden;">
					<img src="https://gakken-idn.id/wp-content/uploads/{{ \Carbon\Carbon::parse($article->post_date)->format('Y/m') }}/{{ $file }}" class="w-100" />
				</div>
			@endif
			<h3>{{ $article->post_title }}</h3>
			{!! clean($article->subheading->content) !!}
		</a>
		<div class="d-flex justify-content-between align-items-center flex-nowrap">
			<div>
				@if (!isset($category))
					@foreach ($article->categoryTaxonomies as $categoryTaxonomy)
						<span class="badge badge-pill badge-secondary p-2 mb-1">{{ $categoryTaxonomy->term->name }}</span>
					@endforeach
				@endif
			</div>
			<div class="lead text-muted d-block" style="white-space: nowrap;">
				<b class="fa fa-eye"></b> {{ $article->popularPostsData->pageviews or '0' }}
			</div>
		</div>
	</div>

@endforeach