<nav class="nav nav-pills flex-lg-column flex-md-row flex-column mb-lg-0 mb-5">
	<a href="{{ route('profile') }}" class="nav-link{{ Request::path() == 'profile' ? ' active' : '' }}">Overview Profil</a>
	<a href="{{ route('profile.edit') }}" class="nav-link{{ Request::path() == 'profile/edit' ? ' active' : '' }}">Edit Profil</a>
	<a href="{{ route('profile.edit-password') }}" class="nav-link{{ Request::path() == 'profile/password' ? ' active' : '' }}">Ubah Kata Sandi</a>
	<a href="#" class="nav-link" onclick="document.getElementById('logout-form').submit();return false;">Keluar</a>
</nav>

{!! Form::open(['route' => 'logout', 'id' => 'logout-form']) !!}
{!! Form::close() !!}