@extends('layouts.profile')

@section('profile-content')
	
	{!! Form::open(['route' => 'profile.password', 'method' => 'patch']) !!}

		<div class="form-group row{{ $errors->first('password') ? ' has-danger' : '' }}">
			<div class="text-muted col-form-label col-lg-3">Kata Sandi Saat Ini</div>
			<div class="col-lg-9">
				{!! Form::password('password', ['class' => 'form-control']) !!}
				@if ($errors->first('password'))
					<span class="form-control-feedback">{{ $errors->first('password') }}</span>
				@endif
			</div>
		</div>

		<div class="form-group row{{ $errors->first('newPassword') ? ' has-danger' : '' }}">
			<div class="text-muted col-form-label col-lg-3">Kata Sandi Baru</div>
			<div class="col-lg-9">
				{!! Form::password('newPassword', ['class' => 'form-control']) !!}
				@if ($errors->first('newPassword'))
					<span class="form-control-feedback">{{ $errors->first('newPassword') }}</span>
				@endif
			</div>
		</div>

		<div class="form-group row">
			<div class="text-muted col-form-label col-lg-3">Ulangi Kata Sandi Baru</div>
			<div class="col-lg-9">
				{!! Form::password('newPassword_confirmation', ['class' => 'form-control']) !!}
			</div>
		</div>

		<div class="form-group row mt-5">
			<div class="col-lg-9 offset-lg-3">
				<button type="submit" class="btn btn-primary">Simpan</button>
			</div>
		</div>

	{!! Form::close() !!}

@endsection