<div class="modal fade" id="select-profession-modal" data-backdrop="static">
	<div class="modal-dialog mt-lg-5" role="document">
		<div class="modal-content" id="select-profession-modal-default">
			<div class="modal-body">
				
				<div class="text-center py-4">
					<p>Selamat bergabung di Gakken Indonesia!</p>
					<h4>Saya adalah seorang..</h4>
				</div>

				<div class="row align-items-stretch no-gutters mb-4 px-3">
					<div class="col-md-6 pr-2">
						<a href="#" class="btn btn-outline-primary btn-block select-profession-modal-selection" data-value="doctor">
							<img src="{{ asset('images/doctor-icon.svg') }}" class="w-50 my-3" alt="Dokter">
							<h5>Dokter</h5>
						</a>
					</div>
					<div class="col-md-6 pl-2">
						<a href="#" class="btn btn-outline-primary btn-block select-profession-modal-selection" data-value="dentist">
							<img src="{{ asset('images/dentist-icon.svg') }}" class="w-50 my-3" alt="Dokter">
							<h5>Dokter Gigi</h5>
						</a>
					</div>

				</div>

			</div>
			<div class="modal-footer d-flex flex-column text-center py-4">
				<p>Apakah Anda tidak termasuk kategori diatas, atau ingin
				mengaturnya di lain waktu?</p>

				<a href="#" id="select-profession-modal-skip">Lewatkan <b class="fa fa-angle-right"></b></a>

				<p class="mt-4 small text-muted">
					Data profesi Anda digunakan untuk memberi Anda konten yang sesuai di
					situs Gakken, dan dibutuhkan untuk berlangganan layanan Gakken P2KB&reg;.
				</p>

			</div>
		</div>
		<div class="modal-content" id="select-profession-modal-selected">
			<div class="modal-body py-4">
				<div class="mb-4">
					<a href="#" id="select-profession-modal-back-btn"><b class="fa fa-angle-left"></b> Kembali</a>
				</div>

				<h4>Selamat datang, <span data-field="profession"></span> {{ auth()->user()->name }}!</h4>
				<p>Silakan mengisi nomor ID keprofesian Anda (Nomor IDI/PDGI) pada kotak dibawah.</p>

				{!! Form::open(['route' => 'profile.patch-profession', 'id' => 'select-profession-modal-selected-form']) !!}
					<input type="hidden" name="profession" />
					<div class="form-group" id="select-profession-modal-selected-form-input">
						<input type="text" name="idNo" class="form-control form-control-lg" placeholder="Nomor ID Keprofesian" />
						<div class="form-control-feedback"></div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block">Simpan</button>
					</div>
				{!! Form::close() !!}

			</div>
		</div>
		<div class="modal-content" id="select-profession-modal-skipped">
			
			<div class="modal-body pt-5 text-center">
				<p>Jika nanti Anda berubah pikiran, Anda dapat kembali mengubah 
				pengaturan keprofesian di halaman 
				<a href="{{ route('profile.edit') }}">Edit Profil</a> Anda.</p>

				<p><a href="#" data-dismiss="modal">Oke</a></p>
			</div>

		</div>
		<div class="modal-content" id="select-profession-modal-done">
			
			<div class="modal-body pt-5 text-center">
				<p>Oke! Informasi keprofesian Anda telah kami simpan. Dengan ini
				Anda dapat membeli paket berlangganan Gakken P2KB&reg; dan layanan
				lainnya. Anda dapat kembali mengubah pengaturan keprofesian di halaman 
				<a href="{{ route('profile.edit') }}">Edit Profil</a> Anda.</p>

				<p><a href="#" data-dismiss="modal">Oke</a></p>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$("#select-profession-modal").modal('show');
		$("#select-profession-modal-selected, #select-profession-modal-skipped, #select-profession-modal-done").hide();
		$("#select-profession-modal .select-profession-modal-selection").on('click', function (e) {
			e.preventDefault();
			$("#select-profession-modal-default").hide();
			var selectedContent = $("#select-profession-modal-selected");
			selectedContent.show();
			selectedContent.find('input[name=profession]').val($(this).data('value'));
			selectedContent.find('[data-field=profession]').text($(this).data('value') == 'doctor' ? 'Dokter' : 'Dokter Gigi');
		});
		$("#select-profession-modal #select-profession-modal-skip").on('click', function (e) {
			e.preventDefault();
			$("#select-profession-modal-default").hide();
			$("#select-profession-modal-skipped").show();
		});
		$("#select-profession-modal #select-profession-modal-back-btn").on('click', function (e) {
			e.preventDefault();
			$("#select-profession-modal-default").show();
			$("#select-profession-modal-selected, #select-profession-modal-skipped").hide();
		});
		$("#select-profession-modal-selected-form").submit(function (e) {
			e.preventDefault();
			var form = $(this);
			form.find('[type=submit]').attr('disabled', 'disabled').text('Menyimpan..');
			form.find("#select-profession-modal-selected-form-input")
				.removeClass('has-danger')
				.find('.form-control-feedback').hide();
			$.post($(this).attr('action'), $(this).serialize())
				.done(function () {
					$("#select-profession-modal-selected").hide();
					$("#select-profession-modal-done").show();
				})
				.fail(function (e) {
					if (e.status == 422 && e.responseJSON.idNo.length) {
						form.find("#select-profession-modal-selected-form-input")
							.addClass('has-danger')
							.find('.form-control-feedback').text(e.responseJSON.idNo[0]).show();
					}
				})
				.always(function () {
					form.find('[type=submit]').removeAttr('disabled').text('Simpan');
				});
		});
	});
</script>