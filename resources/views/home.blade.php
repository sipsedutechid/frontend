@extends('layouts.front')

@section('content')

	<section class="cover home-cover pt-0">
		@include('layouts.navbar')

		<div class="py-5 d-lg-none"></div>

		<div class="container mt-5 py-3 text-xl-left text-center">
			<div class="row justify-content-xl-start justify-content-center">
				<div class="col-xl-8 col-lg-10">
					<h1 class="display-4 d-none d-md-block">Belajar di Mana Saja, Kapan Saja.</h1>
					<h1 class="d-md-none">Belajar di Mana Saja, Kapan Saja.</h1>
					<p class="lead mb-5">
						Tersedia topik-topik yang khusus dibuat untuk memenuhi kebutuhan dokter
						dan dokter gigi Indonesia yang dilengkapi dengan sertifikat SKP.
					</p>
					<p>
						@if (!auth()->check())
							<a href="{{ route('subscribe') }}" class="btn btn-primary p-3 h6 text-uppercase">Mulai Belajar <b class="fa fa-angle-right"></b></a>
						@elseif (!auth()->user()->userSubscriptions()->active()->first())
							<a href="{{ route('subscribe') }}" class="btn btn-primary p-3 h6 text-uppercase">Mulai Belajar <b class="fa fa-angle-right"></b></a>
						@else
							<a href="{{ route('app', ['route' => '/']) }}" class="btn btn-primary p-3 h6 text-uppercase">Mulai Belajar <b class="fa fa-angle-right"></b></a>
						@endif
					</p>
				</div>
			</div>
		</div>
	</section>

	@if ($promptPitPogiPage)
		<div class="container mb-5">
			<a class="text-white rounded p-5 d-flex flex-md-row flex-column justify-content-between align-items-center" style="background: url('{{ asset('images/pit-pogi-banner-backg.jpg') }}') center center no-repeat; background-size: cover; text-decoration: none;" href="{{ route('pit-pogi') }}">
				<div class="d-flex flex-md-row flex-column justify-content-start align-items-center pr-md-4">
					<div class="text-md-left text-center">
						<div class="h2 m-0">Jangan lewatkan materinya!</div>
						<p class="lead m-0">
							Klik disini untuk nonton dan Unduh materi simposium PIT-POGI XXIII Makassar
							di Gakken Indonesia
						</p>
					</div>
				</div>
				<img src="{{ asset('images/pit-pogi-logo-white.svg') }}" height="50" alt="PIT-POGI XXIII Makassar"  class="mt-md-0 mt-5" />
			</a>
		</div>
	@endif
	
	<div class="container">
		<section>
			<div class="mb-5">
				<div class="row mb-4">
					@foreach ($p2kbTopics as $index => $topic)
						<div class="col-lg-4 col-md-6 mb-3{{ $index == 2 ? ' d-none d-lg-block' : '' }}">
							<a href="{{ route('topic.singleP2KB', ['slug' => $topic->slug]) }}" class="topic-thumb shadowed">
								<div class="topic-thumb-img" style="background-image: url('https://vault.gakken-idn.id/topic/files/{{ $topic->featured_image }}');"></div>
								<div class="topic-thumb-text d-flex flex-column justify-content-between">
									<div>
										<div class="h5 small text-muted text-uppercase">Topik Gakken P2KB&reg; {{ \Carbon\Carbon::parse($topic->publish_at)->format('F Y') }}</div>
										<h5>{{ $topic->title }}</h5>
									</div>
									<div class="d-flex justify-content-between align-items-center">
										<span class="text-muted">
											2 SKP | 
											{{ \Carbon\Carbon::parse($topic->publish_at)->format('Ym') == date('Ym') ? "Rilis Bulan Ini" : "Backnumber" }}
										</span>
										<div><img src="{{ asset('images/idi-logo.svg') }}" height="30" /></div>
									</div>
								</div>
							</a>
						</div>
					@endforeach
				</div>
				<div class="h6 text-md-right text-center"><a href="{{ route('p2kb') }}">Apa itu Gakken P2KB&reg;? <b class="fa fa-arrow-right"></b></a></div>
			</div>
			<div class="mb-5">
				<div class="row mb-4">
					@foreach ($p3kgbTopics as $index => $topic)
						<div class="col-lg-4 col-md-6 mb-3{{ $index == 2 ? ' d-none d-lg-block' : '' }}">
							<a href="{{ route('topic.singleP3KGB', ['slug' => $topic->slug]) }}" class="topic-thumb shadowed">
								<div class="topic-thumb-img" style="background-image: url('https://vault.gakken-idn.id/topic/files/{{ $topic->featured_image }}');"></div>
								<div class="topic-thumb-text d-flex flex-column justify-content-between">
									<div>
										<div class="h5 small text-muted text-uppercase">Topik Gakken P3KGB&reg; {{ \Carbon\Carbon::parse($topic->publish_at)->format('F Y') }}</div>
										<h5>{{ $topic->title }}</h5>
									</div>
									<div class="d-flex justify-content-between align-items-center">
										<span class="text-muted">
											2 SKP | 
											{{ \Carbon\Carbon::parse($topic->publish_at)->format('Ym') == date('Ym') ? "Rilis Bulan Ini" : "Backnumber" }}
										</span>
										<div><img src="{{ asset('images/pdgi-logo.svg') }}" height="30" /></div>
									</div>
								</div>
							</a>
						</div>
					@endforeach
				</div>
				<div class="h6 text-md-right text-center"><a href="{{ route('p2kb') }}">Apa itu Gakken P3KGB&reg;? <b class="fa fa-arrow-right"></b></a></div>
			</div>

			<div class="d-md-flex justify-content-start align-items-center text-md-left text-center">
				<div class="text-muted mr-2 mb-md-0 mb-3">Sertifikasi P2KB dan P3KGB diakreditasi oleh</div>
				<div class="">
					<img src="{{ asset('images/idi-logo.svg') }}" height="50" />
					<img src="{{ asset('images/pdgi-logo.svg') }}" height="50" />
				</div>
			</div>
		</section>
	</div>

	<section class="bg-highlight py-5">
		<div class="container">
			<div class="row justify-content-center my-4">
				<div class="col-lg-9">
					<div class="p-5 bg-white rounded-lg shadowed">
						<div class="row align-items-center">
							<div class="col-lg-8 text-lg-left text-center mb-lg-0 mb-4">
								<div class="h2">Tunggu apa lagi?</div>
								<p class="lead mb-0">
									Segera dapatkan poin SKP yang Anda butuhkan untuk
									pembaruan SIP Anda, disini!
								</p>
							</div>
							<div class="col-lg-4">
								<a href="{{ route('subscribe') }}" class="btn btn-block btn-primary p-3 h6 text-uppercase">Berlangganan <b class="fa fa-angle-right"></b></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="container my-5">

		<section>
			<div class="row align-items-center">
				<div class="col-md-5 mb-md-0 mb-5">
					<div class="section-heading pb-md-3 pb-0">
						{{-- TODO put catchy icon here --}}
						<h5>Artikel Kesehatan</h5>
						<p class="lead">
							Selalu ada terobosan dan temuan mutakhir di dunia kesehatan.
							Kami hadir untuk menyajikan update berita-berita terhangat 
							untuk Anda.
						</p>
					</div>
					<h6 class="d-none d-md-block"><a href="{{ route('articles') }}">Temukan artikel lainnya <b class="fa fa-arrow-right"></b></a></h6>
				</div>
				<div class="col-md-7 pl-lg-5">
					<div class="row mb-md-0 mb-3">
						@foreach ($recentArticles as $article)
							<div class="col-md-6 mb-md-0 mb-4 article-thumb">
								<a href="{{ route('article.single', ['slug' => $article->post_name]) }}" class="article-thumb-img mb-3" style="background-image: url('{{ $article->featuredImages->count() ? $article->featuredImages[0]->url : '' }}')"></a>
								<h5 class="mb-3"><a href="{{ route('article.single', ['slug' => $article->post_name]) }}">{{ $article->post_title }}</a></h5>
								<h6 class="small text-muted text-uppercase mb-3">
									{{ \Carbon\Carbon::parse($article->post_date_gmt)->diffForHumans() }} / 
									{{ $article->author->display_name }}
								</h6>
								<p class="ellipsis-3">{!! $article->subheading->content !!}</p>
							</div>
						@endforeach
					</div>
					<h6 class="d-md-none text-center"><a href="{{ route('articles') }}">Temukan artikel lainnya <b class="fa fa-arrow-right"></b></a></h6>
				</div>
			</div>

		</section>

		<section>
			<div class="row align-items-center">
				<div class="col-xl-7 col-md-6 pr-lg-5 text-center pb-md-0 pb-5">
					<img src="{{ asset('images/home-journals.jpg') }}" class="w-100" />
				</div>
				<div class="col-xl-5 col-md-6">
					<div class="section-heading">
						<h5>Jurnal Premium</h5>
						<p class="lead">
							Terdapat lebih dari 400 jurnal yang dapat diakses dengan
							leluasa, untuk kenyamanan pembelajaran Anda. Disediakan
							oleh Wiley.
						</p>
					</div>
					<h6><a href="{{ route('service', ['service' => 'journals']) }}">Pelajari lebih lanjut <b class="fa fa-arrow-right"></b></a></h6>
				</div>
			</div>

		</section>

		<section>
			<div class="row align-items-center">
				<div class="col-xl-7 col-md-6 pl-lg-5 pb-md-0 pb-5 d-md-none">
					<img src="{{ asset('images/home-drugs.jpg') }}" class="w-100" />
				</div>
				<div class="col-xl-5 col-md-6">
					<div class="section-heading">
						<h5>Indeks Obat</h5>
						<p class="lead">
							Temukan rincian lebih dari 3000 merk obat di Indonesia
							dari lebih dari 200 produsen di database indeks obat kami.
						</p>
					</div>
					<h6><a href="{{ route('service', ['service' => 'drugs']) }}">Pelajari lebih lanjut <b class="fa fa-arrow-right"></b></a></h6>
				</div>
				<div class="col-xl-7 col-md-6 pl-lg-5 text-center d-none d-md-block">
					<img src="{{ asset('images/home-drugs.jpg') }}" class="w-100" />
				</div>
			</div>

		</section>

	</div>

@endsection
