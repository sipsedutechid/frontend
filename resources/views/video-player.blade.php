<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <style>
    body {
      margin: 0;
      padding: 0;
    }
  </style>
</head>
<body>

  <div id="player"></div>

  <script src="{{ $url }}"></script>
  <script>
    jwplayer('player').setup({
      file: '{!! $videoUrl !!}',
      image: '{!! $videoUrl !!}'.replace('manifests', 'thumbs').replace('.m3u8', '-720.jpg')
    });
  </script>

</body>
</html>