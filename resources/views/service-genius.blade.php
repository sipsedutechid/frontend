@extends('layouts.front')

@section('content')
	
	<section class="cover p-lg-0 pb-5" style="background-image: url('{{ asset('images/genius-cover.jpg') }}');background-position: bottom;">
		@include('layouts.navbar')
		<div class="container py-3 text-xl-left text-center">
			<div class="row mt-5 justify-content-xl-start justify-content-center">
				<div class="col-xl-8 col-lg-10">
					<h1 class="display-4 d-none d-md-block">GENIUS</h1>
					<h1 class="d-md-none">GENIUS</h1>
					<p class="lead mb-5">
						Gakken Educational and Customized Service. <br />
						Sesuai keinginan dan kebutuhan institusi dan bisnis Anda.
					</p>
					<p>
						<a href="#contact" class="btn btn-primary-2 p-3 h6 text-uppercase">Hubungi Kami <b class="fa fa-angle-right"></b></a>
						<a href="#about" class="btn btn-outline-secondary p-3 h6 text-uppercase">Pelajari <span class="d-none d-md-inline">Lebih Lanjut</span> <b class="fa fa-angle-right"></b></a>
					</p>
				</div>
			</div>
		</div>
	</section>

	<section id="about">
		<div class="container">
			<div class="section-heading text-center">
				<h5>Tailor-made in Every Way.</h5>
			</div>
			<div class="row justify-content-center mb-5">
				<div class="col-lg-6">
					<p>GENIUS hadir untuk memenuhi segala kebutuhan multimedia-based learning institusi dan bisnis Anda. Melalui layanan Genius, Gakken menyediakan sarana bagi institusi dan bisnis Anda untuk meningkatkan mutu kompetensi pelayanan dan sumber daya manusianya.</p>
				</div>
				<div class="col-lg-6">
					<p>Bekali manajemen, tenaga kesehatan, tenaga pengajar dan mahasiswa anda dengan Genius. Rancang materi pembelajaran berstandar tinggi bersama kami untuk dibuat khusus sesuai kebutuhan Anda!</p>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-lg-3 col-md-4">
					<div class="text-center mb-3">
						<img src="{{ asset('images/genius-icon-stat.png') }}" class="w-75">
					</div>
					<div class="h5 text-center">Pelatihan Persiapan Akreditasi Rumah Sakit dan Institusi Pendidikan</div>
				</div>
				<div class="col-lg-3 col-md-4">
					<div class="text-center mb-3">
						<img src="{{ asset('images/genius-icon-staff.png') }}" class="w-75">
					</div>
					<div class="h5 text-center">Pelatihan Staf Keperawatan dan Tenaga Kesehatan Lainnya</div>
				</div>
				<div class="col-lg-3 col-md-4">
					<div class="text-center mb-3">
						<img src="{{ asset('images/genius-icon-edu.png') }}" class="w-75">
					</div>
					<div class="h5 text-center">Perkuliahan di Institusi Pendidikan Kedokteran dan Program Pendidikan Kesehatan Lainnya</div>
				</div>

			</div>

		</div>
	</section>

	<section id="about-2">
		<div class="container">
			<div class="row justify-content-center align-items-center">
				<div class="col-6 d-none d-lg-block text-center">
					<img src="{{ asset('images/genius-about-2.png') }}" class="w-75" />
				</div>
				<div class="col-lg-6 col-md-10">
					<div class="section-heading text-lg-left text-center">
						<h5>Manfaat dengan Hasil Terukur</h5>
					</div>
					<p>Dengan Genius, tingkatkan kesempatan institusi Anda untuk mendapatkan hasil akreditasi yang membanggakan, tidak hanya dari segi pelayanan tapi juga dari segi mutu sumber daya manusianya.</p>
					<p>Bagi institusi pendidikan, asah kemampuan para staf tenaga pengajar Anda untuk menjadi pembimbing yang inovatif dengan menyusun dan membawakan materi pembelajaran yang komprehensif dan menarik dalam bentuk audiovisual dan tekstual bagi mahasiswanya.</p>
					<p>Bagi institusi kesehatan, gunakan Genius untuk melatih staf Anda dalam menguasai materi-materi pendukung kesuksesan manajemen Rumah Sakit.</p>
				</div>
			</div>
		</div>
	</section>

	<section id="about-3">
		<div class="container">
			<div class="row justify-content-center align-items-center">
				<div class="col-lg-6 col-md-10">
					<div class="section-heading text-lg-left text-center">
						<h5>Akses Dimana Saja, Kapan Saja</h5>
					</div>
					<p>Berikan kemudahan bagi institusi dan bisnis Anda dalam mengakses materi pembelajaran. Anda bisa memilih penyajian online melalui situs Gakken yang dapat diakses dimana saja, kapan saja, melalui perangkat apa saja.</p>
					<p>Selain itu, juga tersedia penyajian secara offline melalui media DVD yang diberikan langsung kepada institusi dan bisnis Anda, dan bebas digunakan dalam lingkup yang Anda inginkan.</p>
				</div>
				<div class="col-6 d-none d-lg-block text-center">
					<img src="{{ asset('images/genius-about-3.png') }}" class="w-75" />
				</div>
			</div>
		</div>
	</section>

	<section id="contact" class="bg-highlight-2 py-5">
		<div class="container">
			<div class="row justify-content-center my-4">
				<div class="col-md-6 text-white">
					<h3 class="display-4">Interested?</h3>
					<div class="h4 mb-4">Silakan isi form, atau hubungi tim kami melalui email atau call center, sekarang juga!</div>

					<div class="h4">
						<i class="fa fa-fw fa-envelope"></i> support@gakken-idn.co.id <br />
						<i class="fa fa-fw fa-phone"></i> 0800-1-401652
					</div>
				</div>
				<div class="col-md-6 text-white">

				</div>
			</div>
		</div>
	</section>

@endsection