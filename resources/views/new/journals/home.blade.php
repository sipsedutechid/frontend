@extends('layouts.front')

@section('content')

  @include('layouts.navbar')

  <div class="container">
    <div class="d-md-none py-5"></div>

    <div class="my-5">
      <h1 class="display-4">Jurnal Premium</h1>
    </div>

    <div id="journals-app"></div>
  </div>

@endsection

@section('scripts')
  <script>
    window.App = {
      baseUrl: "{{ url('/') }}",
      categories: {!! json_encode($categories) !!}
    };
  </script>
  <script src="{{ mix('js/JournalsApp.js') }}"></script>
@endsection
