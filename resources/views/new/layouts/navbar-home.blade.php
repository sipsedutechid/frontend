<section id="navbar">
    <nav class="nav nav-pills homepage-nav navbar-expand-md flex-row lg-container align-items-center no-gutters align-items-center">

            <a href="/" class="text-sm-center text-md-left col col-md-2 navbar-brand padding-sm-1">
                <img src="/images/logo.png" class="img-logo" />
            </a>

            <a class="navbar-toggler" onclick="openNav()">
                <i class="fa fa-ellipsis-v" aria-hidden="true"> </i>
            </a>


            <div class="col collapse overlay d-md-block" id="navbar-expand">

                <div class="row d-block d-md-none align-items-center">

                    <a href="#" class="d-inline-flex col-6">
                        <img src="/images/logo.png" class="img-logo" />
                    </a>

                    <a href="#" class=" close col-6 text-right" aria-label="Close" onclick="closeNav()">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>

                <ul class="text-md-right col d-flex flex-column flex-md-row justify-content-md-end align-items-md-center">
                    <li>
                        <a class="flex-sm-fill text-sm-center nav-link active" href="/p2kb">P2KB</a>
                    </li>
                    <li> <a class="flex-sm-fill text-sm-center nav-link" href="/articles">Artikel</a> </li>
                    <li class="dropdown">
                        <a class="nav-link dropdown-toggle text-sm-center flex-sm-fill" data-toggle="dropdown" href="#!" role="button" aria-haspopup="true" aria-expanded="false">
                            Media</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="/infographics">Infografis</a>
                            <a class="dropdown-item" href="/drugs">Podcast</a>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a class="nav-link dropdown-toggle text-sm-center flex-sm-fill" data-toggle="dropdown" href="#!" role="button" aria-haspopup="true" aria-expanded="false">
                            Referensi</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="/journals">Jurnal</a>
                            <a class="dropdown-item" href="/drugs">Obat</a>
                        </div>
                    </li>
                    <li> <a class="flex-sm-fill text-sm-center nav-link" href="/events">Event</a></li>
                    <li> <a class="flex-sm-fill text-sm-center nav-link" href="/genius">Kerjasama</a></li>
                    <li> <a class="flex-sm-fill text-sm-center nav-link border-right" data-toggle="modal" data-target="#auth-dialog">Masuk</a></li>
                    <li> <a class="flex-sm-fill text-sm-center nav-link btn" href="#!">Daftar</a> </li>
                </ul>
            </div>
        </div>
    </nav>
</section>
