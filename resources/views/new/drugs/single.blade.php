@extends('layouts.front')

@section('content')

  @include('layouts.navbar')

  <div class="container">
    <div class="d-md-none py-5"></div>

    <div class="mt-5">
      <a href="{{ route('drugs') }}"><i class="fa fa-chevron-left"></i> Kembali ke indeks obat</a>
    </div>

    <div id="drugs-app"></div>
  </div>

@endsection

@section('scripts')
  <script>
    window.App = {
      baseUrl: "{{ url('/') }}",
      drug: {!! json_encode($drug['data']) !!}
    };
  </script>
  <script src="{{ mix('js/DrugsSingleApp.js') }}"></script>
@endsection