@extends('layouts.front')

@section('content')

  @include('layouts.navbar')

  <div class="container">
    <div class="d-md-none py-5"></div>

    <div class="my-5">
      <h1 class="display-4">Indeks Obat</h1>
    </div>

    <div id="drugs-app"></div>
  </div>

@endsection

@section('scripts')
  <script>
    window.App = {
      baseUrl: "{{ url('/') }}",
      classes: {!! json_encode($classes) !!}
    };
  </script>
  <script src="{{ mix('js/DrugsApp.js') }}"></script>
@endsection