@extends('new.layouts.front')

@section('content')

    @include('layouts.navbar')

    <div style="background-color: #F2F5F7;">
        <div class="container container-fluid">
            <div class="row mx-0 px-0 border-bottom py-4"> 
                <div class="col-md-6 py-4"> 
                    <div class="d-block text-uppercase py-4 text-strong h6 color-red"> Infografis Terbaru </div>
                    <div class="h5">  
                        <a 
                            href="/articles/#" 
                            class="text-black text-strong h5 d-block"> 
                            Test
                        </a>
                       
                    </div>
                    <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci dolorem saepe nisi ullam blanditiis minima fuga nulla tempore. Nobis numquam, vero laborum maiores sequi tenetur quo sapiente accusamus et maxime. <p>
                </div>
                <div class="col-md-6 justify-content-center align-items-center d-flex p-4"> 
                    <a 
                        href="/articles/#" 
                        class="text-black text-strong h5 d-block" style="max-width: 406px; width: 100%; height: 564px;"> 
                        <div style="display: block;
                            position: relative;
                            height: 100%;
                            width: 100%;
                            background-image: url('/images/paramount-bed-banner.png');
                            background-size: cover;
                            background-position: center center;
                            background-repeat: no-repeat;" class="shadowed rounded">  
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div> 

   <section>
        <div class="container py-5 mb-5">
          <div class="d-block"> 
                <div class="d-block px-4 pb-2 opacity-half strong"> Infografis Lainnya </div>
                <div class="row mx-0 px-0"> 
                    <div class="col-md-3 p-4 p-md-2"> 
                        <div style="display: block;
                                position: relative;
                                height: 300px;
                                width: 100%;
                                background-image: url('/images/paramount-bed-banner.png');
                                background-size: cover;
                                background-color: #666;
                                background-position: center center;
                                background-repeat: no-repeat;" class="shadowed rounded">  

                                <h5 class="px-3 py-2 text-white" style="position: absolute; left: 0; bottom: 0;"> 
                                    Lorem Ipsum
                                </h5>
                        </div>
                    </div> 

                    <div class="col-md-3 p-4 p-md-2"> 
                        <div style="display: block;
                                position: relative;
                                height: 300px;
                                width: 100%;
                                background-image: url('/images/paramount-bed-banner.png');
                                background-size: cover;
                                background-color: #666;
                                background-position: center center;
                                background-repeat: no-repeat;" class="shadowed rounded">  

                                <h5 class="px-3 py-2 text-white" style="position: absolute; left: 0; bottom: 0;"> 
                                    Lorem Ipsum
                                </h5>
                        </div>
                    </div> 

                    <div class="col-md-3 p-4 p-md-2"> 
                        <div style="display: block;
                                position: relative;
                                height: 300px;
                                width: 100%;
                                background-image: url('/images/paramount-bed-banner.png');
                                background-size: cover;
                                background-color: #666;
                                background-position: center center;
                                background-repeat: no-repeat;" class="shadowed rounded">  

                                <h5 class="px-3 py-2 text-white" style="position: absolute; left: 0; bottom: 0;"> 
                                    Lorem Ipsum
                                </h5>
                        </div>
                    </div> 
                </div>
            </div> 
        </div>
   </section>
@endsection
