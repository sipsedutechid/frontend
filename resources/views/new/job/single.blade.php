@extends('new.layouts.front')

@section('content')

    @include('layouts.navbar')

    <section id="job-single" class="container-single overlay-relative">
        <div class="wrapper padding-lr1 padding-tb5 lg-container">
            <div class="page-nav">
                <a href="/jobs" class="d-block"> <i class="fa fa-angle-left"> </i> Back to List </a>
                <h4> Lowongan Kerja </h4>
            </div>
            <div class="row no-gutters">
                <div class="col-12 col-lg-8">
                    <div class="box-wrap border-box white-container padding-1 radius-normal shadow">
                        <div class="row no-gutters border-bottom">
                            @if($job->logo)
                                <div class="d-inline-block">
                                    <img src="{{ env('VAULT_URL') . '/job/files/' . $job->logo}}" class="d-inline-block" height="150px"/>
                                </div>
                                
                            @endif
                            <div class="d-block padding-half w-100">
                                <p class="category mb-0"> {{ $job->position }} </p>
                                <h5 class="mb-0"> {{ $job->institution }} </h5>
                                <p class="location mb-0">
                                    <i class="fa fa-map-marker"> </i>
                                    {{ $job->city }}, {{ $job->province }}
                                </p>
                            </div>
                        </div>

                        <div class="row no-gutters desc">
                            <div class="col-lg-8 col-12 padding-tb1 desc-container p-4 mx-0">

                                @if($job->desc)
                                <div class="mb-4"> 
                                    <h6> Deskripsi Pekerjaan </h6>
                                    {!! $job->desc !!}
                                </div>
                                @endif

                                @if($job->requirements)

                                <div>
                                    <h6> Syarat </h6>
                                    <p>
                                        {!! $job->requirements !!}
                                    </p>
                                </div>

                                @endif
                                
                            </div>

                            <div class="col-12 col-lg row no-gutters">
                                <div class="py-4 col-lg col-md-6">
                                    <h6> Lokasi </h6>
                                    <p> {{ $job->address }} <a href="#" class="d-block"> <i class="fa fa-map-marker"> </i> Lihat Peta </a></p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="padding-lr1 padding-tb1-xs">
                        <h6> Mungkin Anda tertarik dengan ini </h6>
                        <ul class="event-related row no-gutters">
                            @foreach($relatedJob as $job)
                                <li class="col-12 col-lg-12 col-md-6">
                                    <a href="/job/{{ $job->slug }}" class="d-block">
                                        <h6> {{ $job->position }} : {{ $job->institution }} </h6>
                                    </a>
                                    <p> {{$job->city}}, {{$job->province}} </p>
                                </li>
                            @endforeach
                        </ul>
                        <a href="/jobs" class="text-center d-block"> Lihat semuanya </a>
                    </div>
                </div>
            </div>
        </div>
        <img src="/images/vacancy-bg.jpg" class="overlay-bg" />
    </section>
@endsection
