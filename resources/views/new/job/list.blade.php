@extends('new.layouts.front')

@section('content')

    @include('layouts.navbar')

    <section id="job-vacancy">
        <div class="container page-heading align-items-center padding-tb5">
            <div class="text-wrapper col-12 col-md-8">
                <h2 class="weight-600"> Lowongan Kerja </h2>
                <div class="row no-gutters">
                    <div class="form-input search col-12 col-md-8"> <input type="text" class="shadow" placeholder="Cari lowongan pekerjaan . . ."/> </div>
                    <div class="form-input search col"> <input class="shadow" type="text" placeholder="Cari lokasi . . ."/> </div>
                </div>
            </div>

            <div class="row margin-reset near-me padding-tb5">
                <h6 class="padding-1 col-12 border-bottom margin-bottom-1"> Anda mungkin tertarik dengan ... </h6>
                    @foreach($jobs as $job)
                        <div class="col-6 col-md-3 margin-bottom-1" onclick="window.location.href='{{ env('BASE_URL') . '/job/' . $job->slug }}'">
                            <div class="border-box padding-1 hover">
                                @if($job->logo)
                                    <img src="{{ env('VAULT_URL') . '/job/files/' . $job->logo}}" class="img-fluid"/>
                                @else
                                    <img src="/images/image-jobs.png"  class="img-fluid" />
                                @endif
                                <h6> {{ $job->position }} </h6>
                                <p> {{ $job->institution }} </p>

                                <span class="d-block"> {{ \Carbon\Carbon::parse($job->created_at)->diffForHumans() }}  </span>
                            </div>
                        </div>
                    @endforeach
            </div>

            <div class="no-gutters">
                <h6 class="w-100 padding-1 margin-bottom-1 border-bottom"> Dekat lokasi Anda </h6>
                <div class="row mx-0 px-0">

                    @foreach($nearJobs as $job)
                        <div class="col-6 col-md-3 margin-bottom-1" onclick="window.location.href='{{ env('BASE_URL') . '/job/' . $job->slug }}'">
                            <div class="border-box padding-1 hover">
                                @if($job->logo)
                                    <img src="{{ env('VAULT_URL') . '/job/files/' . $job->logo}}" class="img-fluid" />
                                @else
                                    <img src="/images/image-jobs.png"  class="img-fluid" />
                                @endif
                                <div class="col padding-1">
                                    <h6 class="padding-tbhalf"> {{ $job->position }} </h6>
                                    <p> {{ $job->institution }} </p>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>

            </div>


        </div>
    </section>
@endsection
