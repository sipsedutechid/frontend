@extends('new.layouts.front')

@section('content')
    <section id="article-single" class="padding-tb5">
        <div class="lg-container">
            <div class="d-md-flex flex-md-row">
                <div class="col-md-8 col-12">
                    <div class="row no-gutters stats-row">
                        <div class="col">
                            <a href="#" class="d-inline-block">
                                <i class="fa fa-clock-o"> </i>
                                {{ \Carbon\Carbon::parse($article->post_date_gmt)->format('d F Y') }}
                            </a>
                            <a href="#" class="d-inline-block">
                                <i class="fa fa-user"> </i>
                                {{$article->author->display_name}}
                            </a>
                        </div>
                        <div class="col text-right">
                            <a href="#" class="d-inline-block"> <i class="fa fa-print"> </i> </a>
                            <a href="#" class="d-inline-block"> <i class="fa fa-share-alt"> </i></a>
                        </div>
                    </div>
                    <div class="img-cont main">
                        <img src="{{$article->featuredImages[0]->url}}"/>
                    </div>
                    <h2>{{ $article->post_title }}</h2>
                    <div class="content-wrap">
                        {!! clean($article->post_content) !!}
                    </div>
                </div>

                <div class="col">
                    <div class="related-articles">
                        <h6> Artikel Terkait </h6>
                        @foreach ($relatedArticle as $index => $article)
                            <div class="row">
                                <div class="d-inline-block img-cont related">
                                    <img src="{{$article->featuredImages[0]->url}}">
                                </div>
                                <div class="col">
                                    <a href="/articles/{{$article->post_name}}">{{ $article->post_title }}</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
