@extends('new.layouts.front')

@section('content')
    <section id="article-list">
        <div class="featured-articles lg-container padding-tb5">
            <div class="row no-gutters justify-content-center">
                @foreach($populars as $index => $article)
                    @if($index == 1)
                        <div class="col-lg-3 col-md-6 col-12 d-flex flex-column max-height">
                            <div class="grey-bg featured-article-wrap max-article-height">
                                <span class="category">
                                    <a href="/articles/{{ $article->post_name }}"> {{$article->categoryTaxonomies[0]->term->name}} </a>
                                </span>
                                <a href="/articles/{{ $article->post_name }}">
                                    <h6> {{ $article->post_title }} </h6>
                                    <p>{!! $article->subheading->content !!} </p>
                                </a>
                            </div>
                    @elseif($index == 2)
                        <div class="grey-bg featured-article-wrap max-article-height">
                            <span class="category">
                                <a href="/articles/{{ $article->post_name }}"> {{$article->categoryTaxonomies[0]->term->name}} </a>
                            </span>
                            <a href="/articles/{{ $article->post_name }}">
                                <h6> {{ $article->post_title }} </h6>
                                <p>{!! $article->subheading->content !!} </p>
                            </a>
                        </div>
                    </div>
                    @else
                        <div class="grey-bg featured-article-wrap featured col-12 col-lg-4 col-md-5">
                            <div class="img-cont row no-gutters">
                                <span class="category">
                                    <a href="{{ $article->post_name }}"> {{$article->categoryTaxonomies[0]->term->name}} </a>
                                </span>
                                <h6 class="align-self-end text-left">
                                    <a href="/articles/{{ $article->post_name }}"> {{$article->post_title }} </a>
                                </h6>
                                <img src="{{$article->featuredImages[0]->url}}">
                            </div>

                            <p>{!! $article->subheading->content !!} </p>
                        </div>
                    @endif
                @endforeach
            </div>

            @foreach($categories as $category)
                <div class="body-article-list padding-1">
                    <h3 class="border-bottom padding-1"> {{ $category->term->name }} </h3>
                    <div class="row padding-1">

                        @foreach($category->posts as $article)
                            <div class="col-md-6 col-12 padding-1">
                                <div class="article-list grey-bg row no-gutters">
                                    <div class="col-12  col-md-4 post-images">
                                        <div class="img-cont">
                                            <img src="{{$article->featuredImages[0]->url}}">
                                        </div>
                                    </div>

                                    <div class="col">
                                        <a href="/articles/{{$article->post_name}}" class="d-block">
                                            <h6> {{ $article->post_title }} </h6>
                                            <p>{!! $article->subheading->content !!} </p>
                                        </a>

                                        <div class="row no-gutters align-items-center">
                                            <a href="#" class="sub-info"> {{$article->author->display_name}} </a> ,
                                            <a href="#" class="sub-info"> {{ \Carbon\Carbon::parse($article->post_date_gmt)->format('d F Y') }} </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
            </div>
            @endforeach
        </div>
    </section>
@endsection
