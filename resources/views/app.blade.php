<!DOCTYPE html>
<html lang="id-ID">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name') }}</title>

	<link rel="stylesheet" type="text/css" href="{{ mix('css/app.css') }}">

	<link rel="icon" href="{{ asset('images/icon.png') }}">
	<link rel="icon" sizes="48x48" href="{{ asset('images/icon@0,25x.png') }}">
	<link rel="icon" sizes="96x96" href="{{ asset('images/icon@0,5x.png') }}">
	<link rel="icon" sizes="144x144" href="{{ asset('images/icon@0,75x.png') }}">

	<link rel="apple-touch-icon" href="{{ asset('images/icon.png') }}">
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/touch-icon-ipad.png') }}">
	<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/touch-icon-iphone-retina.png') }}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/touch-icon-ipad-retina.png') }}">

	<meta name="msapplication-square70x70logo" content="{{ asset('images/icon_smalltile.png') }}">
	<meta name="msapplication-square150x150logo" content="{{ asset('images/icon_mediumtile.png') }}">

	<meta name="theme-color" content="#a4151a">
	<script type="text/javascript">
		window.App = {
			url: {
				app: '{{ route('app', ['route' => '/']) }}',
				base: '{{ route('home') }}',
				logout: '{{ route('logout') }}',
				rest: '{{ route('app', ['route' => '/']) }}/rest'
			},
			user: {
				id: '{{ auth()->user()->id }}',
				email: '{{ auth()->user()->email }}',
				name: '{{ auth()->user()->name }}',
				profession: '{{ auth()->user()->profession }}'
			},
			promptEnableTrialTopic: {!! $promptEnableTrialTopic ? json_encode($promptEnableTrialTopic) : 'false' !!},
			analytics: {
				active: {{ config('app.env') == 'production' && env('GOOGLE_ANALYTICS_ID', false) ? 'true' : 'false' }},
				trackingId: '{{ env('GOOGLE_ANALYTICS_ID', '') }}'
			}
		};
	</script>
</head>
<body>

	<div id="app" class="d-none d-lg-block">
		<div class="d-flex flex-column justify-content-center align-items-center" style="height: 500px;">
			<img src="{{ asset('/images/loading.svg') }}" />
			<div class="lead">Memuat aplikasi...</div>
		</div>
	</div>
	<div class="d-lg-none">
		<div class="container">

			<p class="mb-4">
				<a href="{{ route('home') }}"><img src="{{ asset('images/GakkenIndonesia-Original.svg') }}" height="50" /></a>
			</p>
			<p class="lead">
				Ups, sepertinya layar Anda kurang besar. Untuk mengakses layanan Gakken Indonesia, silakan:
			</p>
			<ul class="mb-5">
				<li>Gunakan browser di komputer Anda dalam keadaan maximized,</li>
				<li>Jika menggunakan tablet, coba mengubah ke mode landscape, atau</li>
				<li>
					Unduh aplikasi mobile GakkenP2KB di Play Store atau App Store.<br />
				</li>
			</ul>

			<div class="mb-4">
				<a href='https://play.google.com/store/apps/details?id=com.wasdlabs.apps.gakken&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Temukan di Google Play' src='{{ asset('images/google-play-badge.svg') }}' height="40" /></a>
				<a href='https://play.google.com/store/apps/details?id=com.wasdlabs.apps.gakken&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Download di App Store' src='{{ asset('images/Download_on_the_App_Store_Badge_ID_135x40.svg') }}' height="40" /></a>
			</div>
			<p class="small text-muted mb-5">
				Google Play dan logo Google Play adalah merek dagang Google Inc.<br />
				Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.
			</p>
			<p class="mb-5"><a href="{{ route('home') }}">Kembali ke beranda</a></p>
		</div>
	</div>

	<script type="text/javascript" src="{{ mix('js/App.js') }}"></script>
	@if (env('APP_ENV') == 'production')
		<!--Start of Zendesk Chat Script-->
		{{-- <script type="text/javascript">
			window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set._.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");$.src="https://v2.zopim.com/?4skLxMDjkpWOEU3uSrAbWoid3b68AOuN";z.t=+new Date;$.type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
		</script> --}}
		<!--End of Zendesk Chat Script-->
	@endif
	
	@if (env('GOOGLE_ANALYTICS_ID', false))
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', '{{ env('GOOGLE_ANALYTICS_ID') }}', 'auto');
		  @if (auth()->check())
		  	ga('set', 'userId', {{ auth()->user()->id }});
		  @endif
		  ga('send', 'pageview');

		</script>
	@endif

</body>
</html>