@extends('layouts.front')

@section('content')
	
	<section class="cover p-0" style="background-image: url('{{ asset('images/p2kb-cover.jpg') }}');background-position: bottom;">
		@include('layouts.navbar')
		
		<div class="py-5 d-lg-none"></div>
		
		<div class="container mt-5 py-3">
			<div class="row justify-content-lg-start justify-content-center">
				<div class="col-10 text-lg-left text-center">
					<div class="mb-3 d-none d-md-block">
						<img src="{{ asset('images/logo-p2kb.png') }}" style="width: 200px;" />
					</div>
					<h1 class="display-4 d-none d-md-block">Program P2KB Berkualitas untuk Dokter dan Dokter Gigi.</h1>
					<h1 class="d-md-none">Program P2KB Berkualitas untuk Dokter dan Dokter Gigi.</h1>
					<p class="lead mb-5">
						Dapatkan topik pembelajaran berkualitas Program Pengembangan Pendidikan
						Keprofesian Berkelanjutan untuk Dokter dan Dokter Gigi di Gakken Indonesia.
					</p>
					<p>
						<a href="{{ route('subscribe') }}" class="btn btn-primary p-3 h6 text-uppercase">Berlangganan <b class="fa fa-angle-right"></b></a>
						<a href="#about" class="btn btn-outline-secondary p-3 h6 text-uppercase">Pelajari <span class="d-none d-md-inline">Lebih Lanjut</span> <b class="fa fa-angle-right"></b></a>
					</p>
				</div>
			</div>
		</div>
	</section>

	<section id="about">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="section-heading">
						<h5 class="text-center">Apa Itu Gakken P2KB&reg;?</h5>
					</div>
				</div>
			</div>
			<div class="row mb-5">
				<div class="col-md-6">
					<p>Sebagai salah satu syarat pembaruan SIP, dokter kini diwajibkan
					untuk memperoleh poin SKP dari berbagai kegiatan seminar, pelatihan,
					simposium, dan lain-lain. Namun, padatnya jadwal praktek dan kegiatan
					lainnya menyebabkan banyak dokter tidak memiliki waktu untuk itu.</p>
					<p>Gakken Indonesia dengan layanan Gakken P2KB&reg; hadir untuk
					mempermudah pada dokter untuk dapat memperoleh poin SKP, kapan dan dimana
					saja, secara nyaman dan fleksibel.</p>
				</div>
				<div class="col-md-6">
					<p>Kami merilis satu topik tiap bulannya dengan tema yang berbeda-beda,
					dan secara langsung dapat diakses oleh pengguna berlangganan. Dengan
					berlangganan, pengguna mendapatkan topik-topik untuk 12 bulan kedepan.</p>
					<p>Diakreditasi oleh Ikatan Dokter Indonesia (IDI) Wilayah Sulselbar dan
					Persatuan Dokter Gigi Indonesia (PDGI),
					tiap-tiap topik pembelajaran yang dilulusi dapat bernilai poin SKP
					untuk dapat digunakan sebagai syarat pembaruan SIP.</p>
				</div>
			</div>
			<div class="text-center">
				Diakreditasi oleh
				<img src="{{ asset('images/idi-logo.svg') }}" height="50" />
				<img src="{{ asset('images/pdgi-logo.svg') }}" height="50" />

			</div>
		</div>
	</section>

	<section>

		<div class="container">
			<div class="row justify-content-center align-items-center mb-5">
				<div class="col-lg-6 d-lg-none pb-lg-0 pb-5">
					<img src="{{ asset('images/topic-release.jpg') }}" class="w-100">
				</div>
				<div class="col-lg-6 col-md-10">
					<div class="section-heading text-lg-left text-center">
						<h5>Rilis Tiap Bulannya</h5>
					</div>
					<p class="lead">
						Setiap bulannya, kami merilis satu topik pembelajaran
						yang secara otomatis dapat Anda akses jika sedang
						berlangganan.
					</p>
					<p>
						Topik-topik P2KB dan P3KGB diakreditasi oleh IDI dan PDGI
						dan bernilai SKP, dan berisi materi-materi pembelajaran
						berkualitas. Di akhir topik, Anda dapat mengambil tes, dan
						memperoleh sertifikat dan poin SKP. Tes dapat diambil
						berulang kali, kapan saja selama masa berlangganan.
					</p>
					<p>
						@if ($newReleaseP2KB)
							<a href="#newReleaseP2KB">Topik P2KB bulan ini <b class="fa fa-arrow-right"></b></a><br />
						@endif
						@if ($newReleaseP3KGB)
							<a href="#newReleaseP3KGB">Topik P3KGB bulan ini <b class="fa fa-arrow-right"></b></a>
						@endif
					</p>
				</div>
				<div class="col-lg-6 d-none d-lg-block">
					<img src="{{ asset('images/topic-release.jpg') }}" class="w-100">
				</div>
			</div>
		</div>
	</section>

	<section>

		<div class="container">
			<div class="row justify-content-center align-items-center mb-5">
				<div class="col-lg-6 pb-lg-0 pb-5">
					<img src="{{ asset('images/topic-backnumber.jpg') }}" class="w-100">
				</div>
				<div class="col-lg-6 col-md-10">
					<div class="section-heading text-lg-left text-center">
						<h5>Topik Backnumber</h5>
					</div>
					<p class="lead">
						Dengan berlangganan Gakken P2KB&reg;, 
						Anda dapat melangkah lebih jauh dengan membeli Topik 
						Backnumber yang juga bernilai poin SKP.
					</p>
					<p>
						Topik Backnumber adalah kumpulan topik yang rilis pada
						bulan-bulan sebelumnya yang dapat dibeli secara satu-per-satu
						sesuai kebutuhan.
					</p>
					<p class="d-none d-md-block">
						@if ($backnumbersP2KB)
							<a href="#backnumbersP2KB">Katalog P2KB <b class="fa fa-arrow-right"></b></a><br />
						@endif
						@if ($backnumbersP3KGB)
							<a href="#backnumbersP3KGB">Katalog P3KGB <b class="fa fa-arrow-right"></b></a>
						@endif
					</p>
					<p class="d-md-none text-center">
						@if ($backnumbersP2KB)
							<a href="#backnumbersP2KB" class="btn btn-secondary py-4">Katalog P2KB <b class="fa fa-angle-right"></b></a>
						@endif
						@if ($backnumbersP3KGB)
							<a href="#backnumbersP3KGB" class="btn btn-secondary py-4">Katalog P3KGB <b class="fa fa-angle-right"></b></a>
						@endif
					</p>

				</div>
			</div>
		</div>
	</section>

	@if ($newReleaseP2KB)
		<section class="pb-5" id="newReleaseP2KB">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-8">
						<div class="section-heading">
							<h5 class="text-center">Gakken P2KB&reg;: <br class="d-md-none" />Rilis Bulan Ini</h6>
						</div>
						<a href="{{ route('topic.singleP2KB', ['slug' => $newReleaseP2KB->slug]) }}">
							<img src="https://vault.gakken-idn.id/topic/files/{{ $newReleaseP2KB->featured_image }}" class="w-100 rounded-lg mb-4" />
						</a>

						<div class="row justify-content-between align-items-center">
							<div class="col-lg-8 col-md-7 text-md-left text-center">
								<div class="lead">Mulai Belajar Sekarang!</div>
								<p>
									Beli paket langganan Gakken P2KB&reg; untuk mendapatkan konten
									pembelajaran topik bulan ini, dan 12 bulan kedepan.
								</p>
							</div>
							<div class="col-lg-4 col-md-5">
								<a href="{{ route('subscribe') }}" class="btn btn-block btn-primary h6 text-uppercase">
									Berlangganan <b class="fa fa-angle-right"></b>
								</a>
							</div>
						</div>
					</div>
				</h5>
			</div>
		</section>
	@endif

	@if ($backnumbersP2KB)

		<section id="backnumbersP2KB">

			<div class="container">
				<div class="section-heading">
					<h5 class="text-center">Gakken P2KB&reg;: Katalog Backnumber</h5>
				</div>

				<div class="row mb-5">
					@foreach ($backnumbersP2KB as $topic)
						<div class="col-lg-4 col-md-6 mb-4">
							<div class="topic-thumb shadowed">
								<div class="topic-thumb-img" style="background-image: url('https://vault.gakken-idn.id/topic/files/{{ $topic->featured_image }}');"></div>
								<div class="topic-thumb-text d-flex flex-column justify-content-between">
									<div>
										<div class="h5 small text-muted text-uppercase">Topik Gakken P2KB&reg; {{ \Carbon\Carbon::parse($topic->publish_at)->format('F Y') }}</div>
										<h5>{{ $topic->title }}</h5>
									</div>
									<div class="d-flex justify-content-between align-items-center">
										<span class="text-muted">2 SKP</span>
										<div>
											<a href="{{ route('topic.singleP2KB', ['slug' => $topic->slug]) }}" class="btn btn-outline-secondary">Rincian</a>
											{{-- <a href="#" class="btn btn-outline-primary">Beli <b class="fa fa-shopping-cart"></b></a> --}}
										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>

			</div>

		</section>

	@endif

	@if ($promptP2KBSubscription)

		<section class="bg-highlight py-5">
			<div class="container text-lg-left text-center">
				<div class="row justify-content-center my-4">
					<div class="col-lg-9">
						<div class="p-5 bg-white rounded-lg shadowed">
							<div class="row align-items-center">
								<div class="col-lg-8 mb-lg-0 mb-4">
									<div class="h2">Tunggu apa lagi?</div>
									<p class="lead mb-0">
										Segera dapatkan poin SKP yang Anda butuhkan untuk
										pembaruan SIP Anda, disini!
									</p>
								</div>
								<div class="col-lg-4">
									<a href="{{ route('subscribe') }}" class="btn btn-block btn-primary p-3 h6 text-uppercase">Berlangganan <b class="fa fa-angle-right"></b></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	@endif

	@if ($newReleaseP3KGB)
		<section id="newReleaseP3KGB">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-8">
						<div class="section-heading">
							<h5 class="text-center">Gakken P3KGB&reg;:<br class="d-md-none" /> Rilis Bulan Ini</h6>
						</div>
						<a href="{{ route('topic.singleP3KGB', ['slug' => $newReleaseP3KGB->slug]) }}">
							<img src="https://vault.gakken-idn.id/topic/files/{{ $newReleaseP3KGB->featured_image }}" class="w-100 rounded-lg mb-4" />
						</a>

						<div class="row justify-content-between align-items-center">
							<div class="col-lg-8 col-md-7 text-md-left text-center">
								<div class="lead">Mulai Belajar Sekarang!</div>
								<p>
									Beli paket langganan Gakken P3KGB&reg; untuk mendapatkan konten
									pembelajaran topik bulan ini, dan 12 bulan kedepan.
								</p>
							</div>
							<div class="col-lg-4 col-md-5">
								<a href="{{ route('subscribe') }}" class="btn btn-block btn-primary-2 h6 text-uppercase">
									Berlangganan <b class="fa fa-angle-right"></b>
								</a>
							</div>
						</div>
					</div>
				</h5>
			</div>
		</section>
	@endif

	@if ($backnumbersP3KGB)

		<section id="backnumbersP3KGB">

			<div class="container">
				<div class="section-heading">
					<h5 class="text-center">Gakken P3KGB&reg;: Katalog Backnumber</h5>
				</div>

				<div class="row mb-5">
					@foreach ($backnumbersP3KGB as $topic)
						<div class="col-lg-4 col-md-6 mb-4">
							<div class="topic-thumb shadowed">
								<div class="topic-thumb-img" style="background-image: url('https://vault.gakken-idn.id/topic/files/{{ $topic->featured_image }}');"></div>
								<div class="topic-thumb-text d-flex flex-column justify-content-between">
									<div>
										<div class="h5 small text-muted text-uppercase">Topik Gakken P3KGB&reg; {{ \Carbon\Carbon::parse($topic->publish_at)->format('F Y') }}</div>
										<h5>{{ $topic->title }}</h5>
									</div>
									<div class="d-flex justify-content-between align-items-center">
										<span class="text-muted">2 SKP</span>
										<div>
											<a href="{{ route('topic.singleP3KGB', ['slug' => $topic->slug]) }}" class="btn btn-outline-secondary">Rincian</a>
											{{-- <a href="#" class="btn btn-outline-primary-2">Beli <b class="fa fa-shopping-cart"></b></a> --}}
										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>

			</div>

		</section>

	@endif

	@if ($promptP2KBSubscription)

		<section class="bg-highlight-2 py-5">
			<div class="container text-lg-left text-center">
				<div class="row justify-content-center my-4">
					<div class="col-lg-9">
						<div class="p-5 bg-white rounded-lg shadowed">
							<div class="row align-items-center">
								<div class="col-lg-8 mb-lg-0 mb-4">
									<div class="h2">Tunggu apa lagi?</div>
									<p class="lead mb-0">
										Segera dapatkan poin SKP yang Anda butuhkan untuk
										pembaruan SIP Anda, disini!
									</p>
								</div>
								<div class="col-lg-4">
									<a href="{{ route('subscribe') }}" class="btn btn-block btn-primary-2 p-3 h6 text-uppercase">Berlangganan <b class="fa fa-angle-right"></b></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
	@endif

@endsection