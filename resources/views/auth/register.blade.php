@extends('layouts.front')

@section('content')
<div class="container">
    @include('layouts.navbar')

    <div class="py-md-5 py-4 d-lg-none"></div>

    <div class="row mt-5 justify-content-center">
        <div class="col-xl-5 col-lg-5 col-md-6 pr-lg-5 mb-md-0 mb-5">
            <h1>Buat Akun Gakken Anda.</h1>
            <p>
                Bergabung dengan Gakken Indonesia untuk pembelajaran medis 
                yang berkualitas. Isi form atau masuk via akun media sosial
                Anda untuk bergabung.
            </p>
            <div>
                <a href="{{ route('provider.login', ['provider' => 'facebook']) }}" class="btn btn-facebook d-flex align-items-center py-3 mb-2">
                    <div class="mr-3"><img src="{{ asset('images/facebook-logo.svg') }}" height="25" width="25" /></div>
                    <div>Daftar dengan Facebook</div>
                </a>
                <a href="{{ route('provider.login', ['provider' => 'twitter']) }}" class="btn btn-twitter d-flex align-items-center py-3 mb-2">
                    <div class="mr-3"><img src="{{ asset('images/twitter-logo.svg') }}" height="25" width="25" /></div>
                    <div>Daftar dengan Twitter</div>
                </a>
                <a href="{{ route('provider.login', ['provider' => 'google']) }}" class="btn btn-outline-secondary d-flex align-items-center py-3">
                    <div class="mr-3"><img src="{{ asset('images/google-logo.svg') }}" height="25" width="25" /></div>
                    <div>Daftar dengan Google</div>
                </a>
            </div>
        </div>
        <div class="col-xl-4 col-lg-5 col-md-6">
            <div class="card">
                <div class="card-body">
                    
                    <form accept-charset="utf-8" role="form" method="POST" action="{{ route('register') }}">
                        @if ($errors->any())
                            <div class="alert alert-danger small">
                                Ada yang salah di masukan Anda. Silakan dicoba lagi.
                            </div>
                        @endif
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                            <label for="name">Nama Lengkap</label>
                            <input id="name" type="text" class="form-control form-control-minimal" name="name" value="{{ old('name') }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="form-control-feedback small">
                                    {{ $errors->first('name') }}
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                            <label for="email">Alamat Email</label>
                            <input id="email" type="email" class="form-control form-control-minimal" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="form-control-feedback small">
                                    {{ $errors->first('email') }}
                                </span>
                            @endif
                        </div>

                        <div class="form-group mb-5{{ $errors->has('password') ? ' has-danger' : '' }}">
                            <label for="password">Kata Sandi</label>
                            <input id="password" type="password" class="form-control form-control-minimal" name="password" required>

                            @if ($errors->has('password'))
                                <span class="form-control-feedback small">
                                    {{ $errors->first('password') }}
                                </span>
                            @else
                                <span class="form-text small text-muted">
                                    Minimal 8 karakter
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-danger' : '' }}">
                            {!! Recaptcha::render() !!}
                            @if ($errors->has('g-recaptcha-response'))
                                <span class="form-control-feedback small">
                                    {{ $errors->first('g-recaptcha-response') }}
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">
                                Daftar <b class="fa fa-angle-right"></b>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
