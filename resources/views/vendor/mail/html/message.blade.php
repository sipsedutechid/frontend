@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="{{ env('APP_URL') . '/images/GakkenIndonesia-Original.svg' }}" height="40" alt="{{ config('app.name') }}" />
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            &copy; {{ date('Y') }} PT. Gakken Health and Education Indonesia. All rights reserved.<br />
            Kompleks Ruko Ratulangi, Jl. Dr. Sam Ratulangi, No. 7/C9<br />
            Makassar, Sulawesi Selatan<br />
            Indonesia - 90113
        @endcomponent
    @endslot
@endcomponent
