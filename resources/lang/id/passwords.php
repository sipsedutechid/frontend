<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Kata sandi harus memiliki minimal 6 karakter.',
    'reset' => 'Kata sandi Anda telah dipulihkan.',
    'sent' => 'Petunjuk pemulihan kata sandi Anda telah kami kirim ke email Anda.',
    'token' => 'Token pemulihan sudah tidak valid.',
    'user' => 'Kami tidak dapat menemukan akun dengan email tersebut.',

];
