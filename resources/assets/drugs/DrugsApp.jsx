import React, { Component } from 'react';
import { render } from 'react-dom';
import Axios from 'axios';

import Filter from './Filter';
import List from './List';
import Search from './Search';

class DrugsApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterClass: false,
      filterTerm: '',
      drugs: [],
      fetching: false,
      error: false,
    };
    this.cancelToken = false;
  }

  componentDidMount() {
    this.load({ filterClass: this.state.filterClass, filterTerm: this.state.filterTerm});
  }

  componentWillUnmount() {
    this.cancelToken && this.cancelToken();
  }

  load({ filterClass = false, filterTerm = '' }) {
    this.cancelToken && this.cancelToken();

    this.setState({
      fetching: true,
      error: false,
      filterClass,
      filterTerm,
    });
    Axios.get(window.App.baseUrl + '/drugs', {
      params: {
        json: 'true',
        filterClass: filterClass || null,
        term: filterTerm.length ? filterTerm : null,
      },
      cancelToken: new Axios.CancelToken(c => { this.cancelToken = c; })
    })
      .then(response => {
        this.setState({ drugs: response.data.data, fetching: false });
        this.cancelToken = false;
      })
      .catch(() => {
        this.setState({ fetching: false, error: true });
        this.cancelToken = false;
      });
  }

  render() {
    return (
      <div className="row justify-content-between">

        <div className="d-none d-md-block col-md-4">
          <Filter active={ this.state.filterClass } onChange={ filterClass => this.load({ filterClass, filterTerm: this.state.filterTerm }) } />
        </div>
        <div className="col-md-8">
          <Search onSearch={ filterTerm => this.load({ filterTerm, filterClass: this.state.filterClass }) } />
          <List drugs={ this.state.drugs } />
        </div>

      </div>
    );
  }
}

render(<DrugsApp />, document.getElementById('drugs-app'));