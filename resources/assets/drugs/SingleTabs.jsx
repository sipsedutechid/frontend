import React from 'react';

const tabs = [
  { section: 'overview', label: 'Overview' },
  { section: 'comps', label: 'Komposisi' },
  { section: 'dosages', label: 'Dosis' },
  { section: 'packs', label: 'Kemasan/Harga' },
];

const SingleTabs = props => (
  <div className="nav nav-pills mb-5">
  {
    tabs.map(tab => (
      <a key={ tab.section } className={"nav-item nav-link" + (props.active === tab.section ? ' active' : '')} href="#" onClick={ e => { e.preventDefault(); props.onChange({ section: tab.section }); } }>{ tab.label }</a>
    ))
  }
  </div>
);

export default SingleTabs;