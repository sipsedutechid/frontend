import React, { Component } from 'react';
import { render } from 'react-dom';
import SingleOverview from './SingleOverview';
import SingleComposition from './SingleComposition';
import SingleDosages from './SingleDosages';
import SinglePackaging from './SinglePackaging';
import SingleTabs from './SingleTabs';

class DrugSingleApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      section: 'overview'
    };
  }
  render() {
    return (
      <div>
        <div className="mb-5">
          <h1 className="display-4 mb-5">{ window.App.drug.name }</h1>

          <SingleTabs active={ this.state.section } onChange={ ({ section }) => this.setState({ section })} />

          { this.state.section === 'overview' && <SingleOverview { ...window.App.drug } /> }
          { this.state.section === 'comps' && <SingleComposition { ...window.App.drug } /> }
          { this.state.section === 'dosages' && <SingleDosages { ...window.App.drug } /> }
          { this.state.section === 'packs' && <SinglePackaging { ...window.App.drug } /> }
        </div>
      </div>
    );
  }
}

render(<DrugSingleApp />, document.getElementById('drugs-app'));