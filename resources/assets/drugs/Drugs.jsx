import React from 'react'
import Axios from 'axios'
import { Link, Route } from 'react-router-dom'

import DetailView from '../../../component/masterDetailView/DetailView'
import MasterDetailView from '../../../component/masterDetailView/MasterDetailView'
import MasterView from '../../../component/masterDetailView/MasterView'
import SearchBox from '../../../component/SearchBox'

import DrugsBrowse from './browse/DrugsBrowse'
import DrugSingle from './DrugSingle'
import DrugsList from './DrugsList'

class Drugs extends React.Component {

	constructor(props) {
		super(props)
		this.cancelRequest = false;
		this.state = {
			drugs: [],
			loading: true,
			nextUrl: null,
			searchTerm: ''
		};
	}

	componentDidMount() {
		this.load();
	}

	componentWillUnmount() {
		if (this.cancelRequest != false)
			this.cancelRequest();
	}

	handleLoadMore() {
		if (!this.state.loading && this.state.nextUrl !== null)
			this.load(false, true);
	}

	handleSearchTermChange(e) {
		this.setState({ searchTerm: e.target.value });
	}

	handleSearchClear() {
		this.setState({ searchTerm: '' });
		this.load(true);
	}

	load(clear, next) {
		this.setState({ loading: true });

		if (this.cancelRequest != false)
			this.cancelRequest();

		let params = this.state.searchTerm == '' || clear ? {} : { term: this.state.searchTerm };
		Axios.get(next ? this.state.nextUrl : window.App.url.rest + '/drugs', {
			cancelToken: new Axios.CancelToken((c) => { this.cancelRequest = c; }),
			params: params
		}).then((response) => {
			this.cancelRequest = false;
			let drugs = next ? this.state.drugs : [];
			for (var i = 0; i < response.data.data.length; i++) {
				drugs.push(response.data.data[i]);
			}
			this.setState({ 
				drugs: drugs, 
				loading: false,
				nextUrl: response.data.next_page_url
			});
		}).catch((error) => {
			this.cancelRequest = false;
			this.setState({ loading: false });
		});
	}

	render() {
		return (
			<MasterDetailView>
				<MasterView width="400">
					<div className="master-header">
						<SearchBox 
							onSearch={ this.load.bind(this) } 
							onChange={ this.handleSearchTermChange.bind(this) } 
							onClear={ this.handleSearchClear.bind(this) }
							term={ this.state.searchTerm } />
					</div>

					<DrugsList drugs={ this.state.drugs } onLoadMore={ this.handleLoadMore.bind(this) } loading={ this.state.loading } />
					<Route exact path="/drugs" children={ ({match}) => (
						<Link to="/drugs" className={ 'btn btn-block btn-primary btn-lg rounded-0' + (match ? ' hidden-xl-down' : '') }>Jelajahi Indeks Obat</Link>
						) } />

				</MasterView>
				<DetailView>
					<Route exact path="/drugs/:drug" component={ DrugSingle } />
				</DetailView>
			</MasterDetailView>
			);
	}

}

export default Drugs