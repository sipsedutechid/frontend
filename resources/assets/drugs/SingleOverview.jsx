import React from 'react'

const SingleOverview = (props) => {

	return (
		<div className="row">
			<div className="col-lg-6">
				<div className="mb-4">
					<div className="text-muted text-uppercase h6">Indikasi</div>
					<p>{ props.indication }</p>
				</div>
				<div className="mb-4">
					<div className="text-muted text-uppercase h6">Kontra-Indikasi</div>
					<p>{ props.contraindication.length ? props.contraindication : 'Tidak ada' }</p>
				</div>
				<div className="mb-4">
					<div className="text-muted text-uppercase h6">Perhatian Khusus</div>
					<p>{ props.precautions.length ? props.precautions : 'Tidak ada' }</p>
				</div>
				<div className="mb-4">
					<div className="text-muted text-uppercase h6">Sediaan</div>
					<p>
						{
							props.forms == null ?
								'Tidak ada' :
								props.forms.map((form, i) => {
									return (
										<span key={ form.slug }>{ form.name + (props.forms[i + 1] != undefined ? ', ' : '') }</span>
										);
								})
						}
					</p>
				</div>
			</div>
			<div className="col-lg-6">
				<div className="mb-4">
					<div className="text-muted text-uppercase h6">Efek Samping</div>
					<p>{ props.adverseReactions.length ? props.adverseReactions : 'Tidak ada' }</p>
				</div>
				<div className="mb-4">
					<div className="text-muted text-uppercase h6">Interaksi Obat</div>
					<p>{ props.interactions.length ? props.interactions : 'Tidak ada' }</p>
				</div>
				<div className="mb-4">
					<div className="text-muted text-uppercase h6">Kategori Kehamilan</div>
					<p>{ props.pregnancyCategory.length ? props.pregnancyCategory.toUpperCase() : 'N/A' }</p>
				</div>
			</div>
		</div>
		);

}

export default SingleOverview