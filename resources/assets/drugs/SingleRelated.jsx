import React from 'react'
import Axios from 'axios'
import { Link } from 'react-router-dom'

import Icon from '../../../component/Icon'

class DrugRelated extends React.Component {

	constructor(props) {
		super(props)
		this.cancelRequest = false;
		this.state = {
			subclassDrugs: [],
			manufacturerDrugs: [],
			loading: true
		};
	}

	componentDidMount() {
		Axios.get(window.App.url.rest + '/drugs/' + this.props.drug + '/related', {
			cancelToken: new Axios.CancelToken((c) => { this.cancelRequest = c; })
		}).then((response) => {
			this.cancelRequest = false;
			this.setState({
				subclassDrugs: response.data.subclass,
				manufacturerDrugs: response.data.manufacturer,
				loading: false
			});
		}).catch((error) => {
			this.cancelRequest = false;
			this.setState({ loading: false });
		});
	}

	componentWillUnmount() {
		if (this.cancelRequest != false)
			this.cancelRequest();
	}

	render() {
		const subclassDrugs = this.state.subclassDrugs;
		const manufacturerDrugs = this.state.manufacturerDrugs;

		return (
			<div className="row">
				<div className="col-lg-5 col-md-6">
					<h5 className="text-uppercase mb-4">Obat sejenis ({ this.props.subclass })</h5>
					{
						this.state.loading ?
							<div className="text-center"><Icon name="circle-o-notch" spin size="2" /></div> :
							(
								<ul className="list-unstyled">
									{
										Object.keys(subclassDrugs).map((key, index) => {
											return (
												<li key={ subclassDrugs[key].slug } className="mb-3">
													<Link to={ '/drugs/' + subclassDrugs[key].slug }>
														<div className="h6 m-0">{ subclassDrugs[key].name }</div>
													</Link>
													<div className="small">{ subclassDrugs[key].manufacturer }</div>
													<div className="small text-truncate">{ subclassDrugs[key].indication }</div>
												</li>
												);
										})
									}
								</ul>
								)
					}
				</div>
				<div className="col-lg-5 col-md-6">
					<h5 className="text-uppercase mb-4">Obat { this.props.manufacturer } Lainnya</h5>
					{
						this.state.loading ?
							<div className="text-center"><Icon name="circle-o-notch" spin size="2" /></div> :
							(
								<ul className="list-unstyled">
									{
										Object.keys(manufacturerDrugs).map((key, index) => {
											return (
												<li key={ manufacturerDrugs[key].slug } className="mb-3">
													<Link to={ '/drugs/' + manufacturerDrugs[key].slug }>
														<div className="h6 m-0">{ manufacturerDrugs[key].name }</div>
													</Link>
													{
														manufacturerDrugs[key].drugSubclass != null ?
															<div className="small">{ manufacturerDrugs[key].drugSubclass }</div> : ''
													}
													<div className="small text-truncate">{ manufacturerDrugs[key].indication }</div>
												</li>
												);
										})
									}
								</ul>
								)
					}
				</div>
			</div>
			);
	}

}

export default DrugRelated