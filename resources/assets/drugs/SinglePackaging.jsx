import React from 'react'

const DrugPackaging = (props) => {

	let form = {};

	return (
		<div>
			{
				props.packagings.map((pack, i) => {
					let skipForm = false;
					if (i > 0) {
						skipForm = pack.form.slug == form;
					}
					form = pack.form.slug;

					return (
						<div key={ pack.slug } className={'mb-3 row' + (!skipForm && i > 0 ? ' pt-3' : '') }>
							<div className="col-lg-2 col-md-4">
								{
									!skipForm ?
										<span className="h6 text-muted text-uppercase">{ pack.form.name }</span> : null
								}
							</div>
							<div className="col-lg-10 col-md-8">
								{ pack.name.length ? <span className="mr-2">{ pack.name } x</span> : null }
								<span className="mr-2">{ pack.quantity }</span>
								{ pack.quantity2 != 0 ? <span className="mr-2">x { pack.quantity2 }</span> : null }
								{ pack.quantity3 != 0 ? <span className="mr-2">x { pack.quantity3 }</span> : null }
								{ pack.price != null ? <span className="mr-2">@ { pack.price }</span> : null }
							</div>
						</div>
						);
				})
			}
		</div>
		);

}

export default DrugPackaging