import React from 'react'

const DrugOverview = (props) => {

	return (
		<div className="row">
			<div className="col-lg-6">
				<div className="mb-4">
					<div className="text-muted text-uppercase h6">Indikasi</div>
					<p>{ props.drug.indication }</p>
				</div>
				<div className="mb-4">
					<div className="text-muted text-uppercase h6">Kontra-Indikasi</div>
					<p>{ props.drug.contraindication.length ? props.drug.contraindication : 'Tidak ada' }</p>
				</div>
				<div className="mb-4">
					<div className="text-muted text-uppercase h6">Perhatian Khusus</div>
					<p>{ props.drug.precautions.length ? props.drug.precautions : 'Tidak ada' }</p>
				</div>
				<div className="mb-4">
					<div className="text-muted text-uppercase h6">Sediaan</div>
					<p>
						{
							props.drug.forms == null ?
								'Tidak ada' :
								props.drug.forms.map((form, i) => {
									return (
										<span key={ form.slug }>{ form.name + (props.drug.forms[i + 1] != undefined ? ', ' : '') }</span>
										);
								})
						}
					</p>
				</div>
			</div>
			<div className="col-lg-6">
				<div className="mb-4">
					<div className="text-muted text-uppercase h6">Efek Samping</div>
					<p>{ props.drug.adverseReactions.length ? props.drug.adverseReactions : 'Tidak ada' }</p>
				</div>
				<div className="mb-4">
					<div className="text-muted text-uppercase h6">Interaksi Obat</div>
					<p>{ props.drug.interactions.length ? props.drug.interactions : 'Tidak ada' }</p>
				</div>
				<div className="mb-4">
					<div className="text-muted text-uppercase h6">Kategori Kehamilan</div>
					<p>{ props.drug.pregnancyCategory.length ? props.drug.pregnancyCategory.toUpperCase() : 'N/A' }</p>
				</div>
			</div>
		</div>
		);

}

export default DrugOverview