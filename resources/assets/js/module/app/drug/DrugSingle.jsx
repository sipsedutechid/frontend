import React from 'react'
import Axios from 'axios'

import Icon from '../../../component/Icon'
import Loading from '../../../component/Loading'

import DrugComposition from './DrugComposition'
import DrugDosages from './DrugDosages'
import DrugOverview from './DrugOverview'
import DrugPackaging from './DrugPackaging'
import DrugRelated from './DrugRelated'
import DrugSingleNav from './DrugSingleNav'

class DrugSingle extends React.Component {

	constructor(props) {
		super(props);
		this.cancelRequest = false;
		this.state = {
			drug: {},
			loading: true,
			tab: 'overview'
		};
	}

	componentDidMount() {
		this.load(this.props.match.params.drug);
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.match.params.drug != nextProps.match.params.drug) {
			this.setState({ tab: 'overview' });
			this.load(nextProps.match.params.drug);
		}
	}

	componentWillUnmount() {
		if (this.cancelRequest != false)
			this.cancelRequest();
	}

	handleTabChange(e) {
		e.preventDefault();
		this.setState({ tab: e.target.dataset.tab });
	}

	load(drug) {
		if (this.cancelRequest != false)
			this.cancelRequest();
		this.setState({ loading: true });
		Axios.get(window.App.url.rest + '/drugs/' + drug, {
			cancelToken: new Axios.CancelToken((c) => { this.cancelRequest = c; })
		}).then((response) => {
			this.cancelRequest = false;
			this.setState({
				loading: false,
				drug: response.data.data
			});
		}).catch((error) => {
			this.cancelRequest = false;
			this.setState({ loading: false });
		});
	}

	render() {
		if (this.state.loading)
			return (
				<div className="container py-5">
					<Loading />
				</div>
				);

		if (this.state.drug.slug == undefined)
			return (
				<div className="container py-5">
					<p className="lead text-center">
						Rincian obat dapat diakses jika Anda sedang berlangganan layanan Gakken P2KB.<br />
						Anda dapat membeli paket berlangganan di <a href={ window.App.url.base + '/subscribe' }>halaman berlangganan</a>.
					</p>
				</div>
				);

		let view = null;
		switch (this.state.tab) {
			case 'overview': view = <DrugOverview drug={ this.state.drug } />; break;
			case 'comp': view = <DrugComposition generics={ this.state.drug.generics } />; break;
			case 'dosage': view = <DrugDosages dosages={ this.state.drug.dosages } />; break;
			case 'pack': view = <DrugPackaging packagings={ this.state.drug.packagings } />; break;
			case 'related': view = <DrugRelated drug={ this.state.drug.slug } manufacturer={ this.state.drug.manufacturer } subclass={ this.state.drug.drugSubclass } />; break;
		}


		return (
			<div className="container py-5">
				<div className="mb-4">
					<div className="d-flex justify-content-between align-items-start">
						<div>
							<h1 className="display-4">{ this.state.drug.name }</h1>
							<h5 className="text-muted">
								Diproduksi oleh { this.state.drug.manufacturer }
								{ this.state.drug.marketer != null ? ', dipasarkan oleh ' + this.state.drug.marketer : '' }
							</h5>
							<p>
								{ this.state.drug.drugClass }
								{ 
									this.state.drug.drugSubclass != null ? (
										<span>
											<Icon name="angle-right" className="mx-3" />{ this.state.drug.drugSubclass }
										</span>
										) : '' 
								}
							</p>
						</div>
						{
							this.state.drug.classificationImage != null ?
								(
									<div>
										<img src={ this.state.drug.classificationImage } height="70" />
									</div>
									) : null
						}
					</div>
				</div>

				<DrugSingleNav tab={ this.state.tab } onChange={ this.handleTabChange.bind(this) } />
				{ view }
			</div>
			);
	}

}

export default DrugSingle