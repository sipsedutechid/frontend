import React from 'react'

const DrugDosages = (props) => {

	let form = null;

	return (
		<div>
			{
				props.dosages.map((dosage, i) => {
					let skipForm = false;
					if (i > 0) {
						skipForm = dosage.form == null ? form == null : dosage.form.slug == form;
					}
					form = dosage.form == null ? null : dosage.form.slug;

					return (
						<div key={ dosage.slug } className={ "mb-3 row" + (!skipForm && i > 0 ? ' pt-3' : '') }>
							<div className="col-lg-2 col-md-4">
								{
									!skipForm ?
										<span className="h6 text-muted text-uppercase">{ dosage.form == null ? 'Semua Sediaan' : dosage.form.name }</span> : null
								}
							</div>
							<div className="col-lg-10 col-md-8">
								<span className="h6">
									{ 
										!dosage.name.length ?
											'Semua Kondisi' : dosage.name
									}
									{
										dosage.administration != null ?
											<small>{ dosage.administration }</small> : null
									}
								</span>
								<p>{ dosage.description }</p>
							</div>
						</div>
						);
				})
			}
		</div>
		);

}

export default DrugDosages