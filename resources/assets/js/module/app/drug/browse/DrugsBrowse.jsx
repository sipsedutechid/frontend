import React from 'react'

import ByManufacturer from './ByManufacturer'

class DrugsBrowse extends React.Component {

	render() {
		return (
			<div className="container py-5">
				<div className="mb-5">
					<h1>Jelajahi Indeks Obat</h1>
					<p className="lead">Cari obat pada kolom di sebelah kiri, atau jelajahi database indeks obat dibawah.</p>
				</div>

				<h6 className="section-header text-muted mb-3">Obat berdasarkan pabrik</h6>
				<ByManufacturer />

			</div>
			);
	}

}

export default DrugsBrowse