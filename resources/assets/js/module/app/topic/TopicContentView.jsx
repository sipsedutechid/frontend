import React from 'react'

import TopicContentIframe from './TopicContentIframe'
import TopicContentImage from './TopicContentImage'
import TopicContentVideo from './TopicContentVideo'

class TopicContentView extends React.Component {

	render() {
		let view = null;

		if (this.props.contents != undefined) {

			let content = null;
			for (var i = this.props.contents.length - 1; i >= 0; i--) {
				if (this.props.contents[i].slug == this.props.selection)
					content = this.props.contents[i];
			}

			if (content.type == 'video')
				view = <TopicContentVideo content={ content } videoUrl={ content.data } onComplete={ this.props.onComplete } />;
			else if (content.type == 'image')
				view = <TopicContentImage url={ content.data } content={ content } onComplete={ this.props.onComplete } />;
			else
				view = <TopicContentIframe url={ content.data } content={ content } onComplete={ this.props.onComplete } />;
		}
		return view;
	}

}

export default TopicContentView