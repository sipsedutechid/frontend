import React from 'react'
import Axios from 'axios'
import { Route } from 'react-router-dom'

import SidebarView from '../../../component/SidebarView'
import Container from '../../../component/Container'

import TopicOverview from './TopicOverview'
import TopicContentView from './TopicContentView'
import TopicContentListSidebar from './TopicContentListSidebar'
import TopicHeaderBar from './TopicHeaderBar'

class TopicSingle extends React.Component {

	constructor(props) {
		super(props);

		this.cancelRequest = false;
		this.state = {
			loading: true,
			topic: {},
		};
	}

	componentDidMount() {
		this.setState({ loading: true });
		Axios.get(window.App.url.rest + '/topics/' + this.props.match.params.topic + '/overview', {
			cancelToken: new Axios.CancelToken((c) => { this.cancelRequest = c; })
		}).then((response) => {
			this.cancelRequest = false;
			this.setState({ 
				topic: response.data.data,
				loading: false
			});
		}).catch((error) => {});
	}

	componentWillUnmount() {
		if (this.cancelRequest != false)
			this.cancelRequest();
	}

	updateContentsList(content) {
		Axios.post(window.App.url.rest + '/content/view', { content, topic: this.props.match.params.topic })
			.then(response => this.setState({ topic: Object.assign({}, response.data.data) }))
			.catch(error => {});
	}

	render() {
		return (
			<SidebarView>
				<TopicContentListSidebar topic={ this.state.topic.slug } contents={ this.state.topic.contents } passed={ this.state.topic.passedAt } isTrial={ this.state.topic.isTrial } />

				<TopicHeaderBar topic={ this.state.topic } location={ this.props.location } />
				<Container>
					<Route exact path="/topics/:topic" render={ () => <TopicOverview topic={ this.state.topic } loading={ this.state.loading } firstContent={ this.state.topic.contents != undefined ? this.state.topic.contents[0].slug : null } /> } />
					<Route exact path="/topics/:topic/content/:content" render={ (props) => <TopicContentView onComplete={ this.updateContentsList.bind(this) } topic={ this.state.topic.slug } contents={ this.state.topic.contents } selection={ props.match.params.content } /> } />
				</Container>
			</SidebarView>
			);
	}

}

export default TopicSingle