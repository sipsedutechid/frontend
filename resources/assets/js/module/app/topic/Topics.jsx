import React from 'react'
import Axios from 'axios'

import Loading from '../../../component/Loading'

import TopicsList from './TopicsList'

class Topics extends React.Component {

	constructor(props) {
		super(props);
		this.cancelRequest = false;
		this.state = {
			loading: true,
			topics: []
		};
	}

	componentDidMount() {
		return this.onLoadList();
	}

	componentWillUnmount() {
		if (this.cancelRequest !== false)
			this.cancelRequest();
	}

	onLoadList() {
		this.setState({ loading: true });
		Axios.get(App.url.rest + '/topics/my', {
			cancelToken: new Axios.CancelToken((c) => { this.cancelRequest = c; })
		})
		.then((response) => {
			this.cancelRequest = false;
			this.setState({ 
				topics: response.data.data.slice(0),
				loading: false 
			});
		}).catch((error) => {
			this.setState({ loading: false });
		});
	}

	render() {
		return (
			<div className="container py-5">
				<div className="row justify-content-center">
					{
						this.state.loading ?
							<Loading /> : <TopicsList topics={ this.state.topics } onReload={ this.onLoadList.bind(this) } />
					}
				</div>
			</div>
			);
	}

}

export default Topics