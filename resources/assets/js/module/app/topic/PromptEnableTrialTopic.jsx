import React from 'react'
import Axios from 'axios'
import Loading from '../../../component/Loading'

export default class PromptEnableTrialTopic extends React.Component {

	constructor(props) {
		super(props);
		this.cancelRequest = false;
		this.state = {
			isSaved: false,
			highlight: null,
			isEnabling: false,
			isSelecting: false
		};
	}

	componentWillUnmount() {
		if (this.cancelRequest !== false)
			this.cancelRequest();
	}

	onEnable(topic) {
		this.setState({ isEnabling: true });
		Axios.patch(App.url.rest + '/topics/enable-trial', { topic }, {
			cancelToken: new Axios.CancelToken(c => { this.cancelRequest = c; })
		})
		.then(r => {
			this.cancelRequest = false; 
			this.setState({ isEnabling: false });
			return this.onSave();
		})
		.catch(e => {
			this.cancelRequest = false; 
			this.setState({ isEnabling: false });
		});
	}

	onSave() {
		this.setState({ isSaved: true });
		App.promptEnableTrialTopic = false;
		setTimeout(() => this.props.onSave(), 3000);
	}

	render() {
		if (this.state.isSaved)
			return (
				<div className="card border-info mb-5">
					<div className="card-body text-info">
						<p className="mb-0">
							Terima kasih! Sertifikat Anda dapat diunduh dari halaman profil pada
							bagian Sertifikat.
						</p>
					</div>
				</div>
				);
		return (
			<div className="card border-info mb-5">
				<div className="card-body text-info">
					<div className="row align-items-center">
						<div className="col-9">
							<div className="h5">Masa trial Anda akan segera berakhir.</div>
							<p className="mb-0">
								Anda dapat memilih salah satu dari topik trial yang Anda lulusi untuk
								mengunduh sertifikat bernilai SKP sebelum masa trial Anda berakhir.
							</p>
						</div>
						<div className="col-3">
							{
								!this.state.isSelecting ?
									<button type="button" className="btn btn-info btn-block" style={{ cursor: 'pointer' }} onClick={ () => this.setState({ isSelecting: true }) }>Pilih Topik</button> : null
							}
						</div>
					</div>
					{
						this.state.isSelecting ?
							(
								<div className="row no-gutters mt-4" style={{ position: 'relative' }}>
									{
										this.props.options.map((topic, index) => {
											return (
												<div key={ index } className="col-4 pr-3">
													<div 
														style={{ cursor: 'pointer', transition: 'background 0.15s ease-out' }} 
														onClick={ () => this.onEnable(topic) }
														className={"p-3 rounded" + (this.state.highlight === index ? ' bg-info text-light' : ' bg-light')} 
														onMouseOut={ () => this.setState({ highlight: null }) }
														onMouseOver={ () => this.setState({ highlight: index }) }>
														<div className="text-truncate">{ topic.title }</div>
														<div className={ "small" + (this.state.highlight === index ? ' text-light' : ' text-muted') }>Topik { topic.releaseMonth } / Nilai Tes: { topic.grade }</div>
													</div>
												</div>
												);
										})
									}
									{ 
										this.state.isEnabling ? 
											<div className="bg-white text-center" style={{ opacity: 0.5, position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }} /> : null
									}
								</div>
								) : null
					}
				</div>
			</div>
			);
	}

}