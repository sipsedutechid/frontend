import React from 'react'
import { NavLink } from 'react-router-dom'
import { ListGroup } from 'reactstrap'

import Loading from '../../../component/Loading'

class JournalsList extends React.Component {

	constructor(props) {
		super(props);
	}

	handleListScrolled(e) {
		if ((100 * e.target.scrollTop / (e.target.scrollHeight - e.target.clientHeight)) === 100)
			return this.props.onLoadMore();
	}

	render() {
		return (
			<div className="master-list" onScroll={ this.handleListScrolled.bind(this) }>
				<ListGroup className="journals-list">
					{
						this.props.journals.map((journal) => {
							return (
								<NavLink key={ journal.slug } to={ '/journals/' + journal.slug } className="list-group-item d-flex flex-column flex-nowrap align-items-start">
									<div className="h6 mb-1">{ journal.title } <span className="small text-muted">({ journal.publisher })</span></div>
									<div className="small ellipsis">ISSN: { journal.issn }</div>
								</NavLink>
								);
						})
					}
				</ListGroup>
				{
					this.props.loading ?
						<div className="my-4"><Loading /></div> : ''
				}
			</div>
			);

	}

}

export default JournalsList