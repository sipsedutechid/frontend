import React from 'react'
import Axios from 'axios'
import { Link, Route } from 'react-router-dom'

import Icon from '../../../component/Icon'
import Loading from '../../../component/Loading'

import JournalRead from './JournalRead'

class JournalSingle extends React.Component {

	constructor(props) {
		super(props);
		this.cancelRequest = false;
		this.state = {
			descriptionExpanded: false,
			journal: {},
			loading: true
		};
	}

	componentDidMount() {
		this.load(this.props.match.params.journal);
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.match.params.journal != nextProps.match.params.journal) {
			this.setState({ descriptionExpanded: false });
			this.load(nextProps.match.params.journal);
		}
	}

	componentWillUnmount() {
		if (this.cancelRequest != false)
			this.cancelRequest();
	}

	handleDescriptionExpand(e) {
		e.preventDefault();
		this.setState({ descriptionExpanded: true });
	}

	load(journal) {
		if (this.cancelRequest != false)
			this.cancelRequest();
		this.setState({ loading: true });
		Axios.get(window.App.url.rest + '/journals/' + journal, {
			cancelToken: new Axios.CancelToken((c) => { this.cancelRequest = c; })
		}).then((response) => {
			this.cancelRequest = false;
			this.setState({
				loading: false,
				journal: response.data.data
			});
		}).catch((error) => {
			this.cancelRequest = false;
			this.setState({ loading: false });
		});
	}

	render() {
		if (this.state.loading)
			return (
				<div className="container py-5">
					<Loading />
				</div>
				);

		return (
			<div className="container py-5">
				<div className="mb-5">
					<div className="d-flex justify-content-start align-items-start">
						<div className="rounded mr-3" style={{ overflow: 'hidden' }}>
							<img src={ this.state.journal.imageUrl } height="150" />
						</div>
						<div>
							<h1 className="display-4">{ this.state.journal.title }</h1>
							<h5 className="text-muted">
								Dipublikasi oleh { this.state.journal.publisher }
							</h5>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-lg-4">
						<div className="mb-4">
							<div className="text-muted text-uppercase h6">International Standard Serial Number (ISSN)</div>
							<p>{ this.state.journal.issn !== null ? this.state.journal.issn : 'Tidak ada' }</p>
						</div>
						<div className="mb-4">
							<div className="text-muted text-uppercase h6">Electronic International Standard Serial Number (E-ISSN)</div>
							<p>{ this.state.journal.eIssn !== null ? this.state.journal.eIssn : 'Tidak ada' }</p>
						</div>
						<div className="mb-4">
							<div className="text-muted text-uppercase h6">Penyunting</div>
							<p>{ this.state.journal.editor !== null ? this.state.journal.editor : 'Tidak ada' }</p>
						</div>

						{
							this.state.journal.readUrl.length ?
								(
									<a href={ this.state.journal.readUrl } target="_blank" className="btn btn-outline-primary btn-block">
										Baca Jurnal <Icon name="play" className="ml-1" />
									</a>
									) : null
						}
					</div>
					<div className="col-lg-8">
						<div className={ 'journal-description' + (this.state.descriptionExpanded ? ' expanded' : '') } dangerouslySetInnerHTML={{ __html: this.state.journal.description }}></div>
						{
							!this.state.descriptionExpanded ?
								(
									<div className="text-center">
										<Icon name="angle-down" /> 
											<a href="#" className="mx-2" onClick={ this.handleDescriptionExpand.bind(this) }>
												Baca Selengkapnya
											</a>
										<Icon name="angle-down" />
									</div>
									) : null
						}
					</div>
				</div>

			</div>
			);
	}

}

export default JournalSingle