import React from 'react'
import Axios from 'axios'
import { Route } from 'react-router-dom'

import DetailView from '../../../component/masterDetailView/DetailView'
import MasterDetailView from '../../../component/masterDetailView/MasterDetailView'
import MasterView from '../../../component/masterDetailView/MasterView'
import SearchBox from '../../../component/SearchBox'

import JournalsList from './JournalsList'
import JournalSingle from './JournalSingle'

class Journals extends React.Component {

	constructor(props) {
		super(props);
		this.cancelRequest = false;
		this.state = {
			journals: [],
			loading: true,
			nextUrl: null,
			searchTerm: ''
		};
	}

	componentDidMount() {
		this.load();
	}

	componentWillUnmount() {
		if (this.cancelRequest != false)
			this.cancelRequest();
	}

	handleLoadMore() {
		if (!this.state.loading && this.state.nextUrl !== null)
			this.load(false, true);
	}

	handleSearchTermChange(e) {
		this.setState({ searchTerm: e.target.value });
	}

	handleSearchClear() {
		this.setState({ searchTerm: '' });
		this.load(true);
	}

	load(clear, next) {
		this.setState({ loading: true });

		if (this.cancelRequest != false)
			this.cancelRequest();

		let params = this.state.searchTerm == '' || clear ? {} : { term: this.state.searchTerm };
		Axios.get(next ? this.state.nextUrl : window.App.url.rest + '/journals', {
			cancelToken: new Axios.CancelToken((c) => { this.cancelRequest = c; }),
			params: params
		}).then((response) => {
			this.cancelRequest = false;
			let journals = next ? this.state.journals : [];
			for (var i = 0; i < response.data.data.length; i++) {
				journals.push(response.data.data[i]);
			}
			this.setState({ 
				journals: journals, 
				loading: false,
				nextUrl: response.data.next_page_url
			});
		}).catch((error) => {
			this.cancelRequest = false;
			this.setState({ loading: false });
		});
	}

	render() {
		return (
			<MasterDetailView>
				<MasterView width="400">
					<div className="master-header">
						<SearchBox 
							onSearch={ this.load.bind(this) } 
							onChange={ this.handleSearchTermChange.bind(this) } 
							onClear={ this.handleSearchClear.bind(this) }
							term={ this.state.searchTerm } />
					</div>

					<JournalsList journals={ this.state.journals } onLoadMore={ this.handleLoadMore.bind(this) } loading={ this.state.loading } />

				</MasterView>
				<DetailView>
					<Route path="/journals/:journal" component={ JournalSingle } />
				</DetailView>
			</MasterDetailView>
		);
	}

}

export default Journals