import React from 'react'
import Axios from 'axios'
import { Link } from 'react-router-dom'
import { 
	Button, 
	FormFeedback, 
	FormGroup, 
	Input, 
	Label
} from 'reactstrap'

class ProfilePassword extends React.Component {

	constructor(props) {
		super(props);

		this.cancelRequest = false;
		this.state = {
			password: '',
			newPassword: '',
			passwordConfirm: '',

			errors: {},
			saving: false
		};
	}

	handleChange(e) {
		const field = e.target.name;
		const newState = { [field]: e.target.value};
		this.setState(newState);
	}

	handleSave() {
		this.setState({ 
			errors: {},
			saving: true 
		});
		Axios.patch(window.App.url.rest + '/password', {
			password: this.state.password,
			newPassword: this.state.newPassword,
			newPassword_confirmation: this.state.passwordConfirm
		}, {
			cancelToken: new Axios.CancelToken((c) => { this.cancelRequest = c; })
		}).then((response) => {
			this.cancelRequest = false;
			this.setState({ 
				saving: false,
				password: '',
				newPassword: '',
				passwordConfirm: ''
			});
			if (response.data.status == 'success') {
				// TODO show notification
			}
		}).catch((error) => {
			this.cancelRequest = false;
			this.setState({ 
				errors: error.response.data,
				saving: false
			});
		});
	}

	render() {

		return (
			<div>
				<h1 className="mb-4">Ubah Kata Sandi</h1>
				<FormGroup color={ this.state.errors.password != undefined ? "danger" : ''}>
					<Label className="small text-uppercase text-muted">Kata Sandi Saat Ini</Label>
					<Input type="password" name="password" value={ this.state.password } size="lg" onChange={ this.handleChange.bind(this) } disabled={ this.state.saving } />
					{
						this.state.errors.password != undefined ? 
							<FormFeedback>{ this.state.errors.password[0] }</FormFeedback> : null
					}
				</FormGroup>
				<FormGroup color={ this.state.errors.newPassword != undefined ? "danger" : ''}>
					<Label className="small text-uppercase text-muted">Kata Sandi Baru</Label>
					<Input type="password" name="newPassword" value={ this.state.newPassword } size="lg" onChange={ this.handleChange.bind(this) } disabled={ this.state.saving } />
					{
						this.state.errors.newPassword != undefined ? 
							<FormFeedback>{ this.state.errors.newPassword[0] }</FormFeedback> : null
					}
				</FormGroup>
				<FormGroup>
					<Label className="small text-uppercase text-muted">Ulangi Kata Sandi Baru</Label>
					<Input type="password" name="passwordConfirm" value={ this.state.passwordConfirm } size="lg" onChange={ this.handleChange.bind(this) } disabled={ this.state.saving } />
				</FormGroup>
				<hr className="my-5" />
				<FormGroup>
					<Button size="lg" color="primary" disabled={ this.state.saving } onClick={ this.handleSave.bind(this) }>Simpan</Button>
					<Link to="/profile" className="btn btn-lg btn-link" disabled={ this.state.saving }>Batal</Link>
				</FormGroup>
			</div>
			);

	}

}

export default ProfilePassword