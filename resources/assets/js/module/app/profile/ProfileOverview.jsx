import React from 'react'
import { Link } from 'react-router-dom'

import Icon from '../../../component/Icon'

const ProfileOverview = (props) => {

	if (props.profile.email == undefined)
		return null;

	return (
		<div>
			<div className="d-flex justify-content-start align-items-center mb-5">
				<img src={ props.profile.avatar === null ? '/images/user.svg' : props.profile.avatar } style={{ height: 100, borderRadius: "50%" }} />
				<div className="ml-4">
					<h1 className="display-4">{ props.profile.name }</h1>
					<div className="text-muted h5">{ props.profile.email }</div>
				</div>
			</div>
			<div className="row">
				<div className="col-md-4">
					<h4>Profil</h4>
					<hr />
					<div className="mb-3">
						<div className="small text-muted text-uppercase">Bergabung sejak</div>
						<div className="lead">{ props.profile.joinedAt }</div>
					</div>
					<div className="mb-3">
						<div className="small text-muted text-uppercase">Profesi</div>
						<div className="lead">
							{ 
								props.profile.profession == null ?
									"Belum diatur" : (
										props.profile.profession == 'doctor' ?
											"Dokter" : "Dokter Gigi"
										) 
							}
						</div>
					</div>
					<div className="mb-3">
						<div className="small text-muted text-uppercase">Nomor ID Keprofesian</div>
						<div className="lead">{ props.profile.idNo != null ? props.profile.idNo : 'Belum diatur' }</div>
					</div>
					<Link to="/profile/edit"><Icon name="pencil" /> Edit profil</Link>
				</div>
				<div className="col-md-8">
					<h4>Status Berlangganan</h4>
					<hr />

					{
						props.profile.currentSubscription != null ?
							(
								<div>
									<div className="text-success d-flex justify-content-start align-items-center mb-3">
										<Icon name="check-circle" size="2" className="mr-2" />
										<div>{ props.profile.currentSubscription.subscription }</div>
									</div>
									<p>
										Saat ini Anda sedang berlangganan paket Gakken P2KB
										yang akan berakhir pada { props.profile.currentSubscription.expiredAt }.
									</p>
								</div>
								) : 
							(
								<div>
									<p>
										Saat ini, Anda belum berlangganan paket Gakken P2KB. Anda 
										dapat membeli paket berlangganan di <a href={ window.App.url.base + '/subscribe' }>Halaman berlangganan</a>.
									</p>
								</div>
								)
					}
				</div>
			</div>
		</div>
		);

}

export default ProfileOverview