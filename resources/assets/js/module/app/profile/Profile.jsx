import React from 'react'
import Axios from 'axios'
import { NavLink, Route } from 'react-router-dom'
import { ListGroup } from 'reactstrap'

import Container from '../../../component/Container'
import Loading from '../../../component/Loading'
import Sidebar from '../../../component/Sidebar'
import SidebarView from '../../../component/SidebarView'

import ProfileEdit from './ProfileEdit'
import ProfileOverview from './ProfileOverview'
import ProfilePassword from './ProfilePassword'
import ProfileCertificates from './ProfileCertificates'

class Profile extends React.Component {

	constructor(props) {
		super(props);
		this.cancelRequest = false;
		this.state = {
			loading: true,
			saving: false,
			editErrors: {},
			profile: {}
		};
	}

	componentDidMount() {
		Axios.get(window.App.url.rest + '/profile', {
			cancelToken: new Axios.CancelToken((c) => { this.cancelRequest = c; })
		}).then((response) => {
			this.cancelRequest = false;
			this.setState({
				loading: false,
				profile: response.data
			});
		}).catch((error) => {
			this.cancelRequest = false;
		});
	}

	componentWillUnmount() {
		if (this.cancelRequest != false)
			this.cancelRequest();
	}

	handleSaveEdit(newProfile) {
		this.setState({ saving: true });
		Axios.patch(window.App.url.rest + '/profile', newProfile, {
			cancelToken: new Axios.CancelToken((c) => { this.cancelRequest = c; })
		}).then((response) => {
			this.cancelRequest = false;
			this.setState({
				saving: false,
				profile: response.data
			});
		}).catch((error) => {
			this.cancelRequest = false;
			this.setState({
				saving: false,
				editErrors: error.response.data
			});
		});
	}

	handlePageChange() {
		this.setState({ editErrors: {}});
	}

	render() {
		return (
			<SidebarView>
				<Sidebar>
					<ListGroup>
						<NavLink exact to="/profile" className="list-group-item">Overview Profil</NavLink>
						<NavLink exact to="/profile/edit" className="list-group-item">Edit Profil</NavLink>
						<NavLink exact to="/profile/password" className="list-group-item">Ubah Kata Sandi</NavLink>
						<NavLink exact to="/profile/certificates" className="list-group-item">Sertifikat</NavLink>
					</ListGroup>
				</Sidebar>
				<Container>
					{
						this.state.loading ?
							(
								<div className="container py-5">
									<Loading />
								</div>
								) :
							(
								<div className="container py-5">
									<div className="row justify-content-center">
										<div className="col-xl-10">
											<Route exact path="/profile" render={ () => <ProfileOverview profile={ this.state.profile } /> } />
											<Route exact path="/profile/edit" render={ () => <ProfileEdit profile={ this.state.profile } onSave={ this.handleSaveEdit.bind(this) } onUnmount={ this.handlePageChange.bind(this) } saving={ this.state.saving } errors={ this.state.editErrors } /> } />
											<Route exact path="/profile/password" render={ () => <ProfilePassword /> } />
											<Route exact path="/profile/certificates" component={ () => <ProfileCertificates profile={ this.state.profile } /> } />
										</div>
									</div>
								</div>
								)
					}
				</Container>
			</SidebarView>
			);
	}

}

export default Profile