import React from 'react'

const Icon = (props) => {

	return (
		<b
			className={
				'fa fa-' + props.name +
				(props.fixed != undefined ? ' fa-fw' : '') +
				(props.spin != undefined ? ' fa-spin' : '') +
				(props.size != undefined ? ' fa-' + props.size + 'x' : '') +
				(props.className != undefined ? ' ' + props.className : '')
			} />
		);

}

export default Icon