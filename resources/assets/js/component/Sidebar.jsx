import React from 'react'

const Sidebar = (props) => {

	let style = {};
	if (props.width)
		style.width = props.width + 'px';

	return (
		<div className="sidebar" style={ style }>
			{ props.children }
		</div>
		);

}

export default Sidebar