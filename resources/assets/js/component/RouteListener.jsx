import React from 'react'
import { withRouter } from 'react-router-dom'

class RouteListener extends React.Component {
	
	constructor(props) {
		super(props);
	}

	componentWillReceiveProps(nextProps) {
		return this.props.onChange(nextProps.location.pathname);
	}

	render() {
		return null;
	}

}

export default withRouter(RouteListener)