import React from 'react'

const Headerbar = (props) => {

	return (
		<div className="headerbar d-flex justify-content-between align-items-center px-3">
			{ props.children }
		</div>
		);

}

export default Headerbar