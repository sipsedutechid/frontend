import React from 'react'
import { Button, Input } from 'reactstrap'

import Icon from './Icon'

class SearchBox extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			searched: false
		}
	}

	handleKeyPress(e) {
		if (e.key == 'Enter') {
			this.setState({ searched: e.target.value.length });
			return this.props.onSearch();
		}
	}

	handleClear() {
		this.setState({ searched: false });
		this.searchInput.focus();
		return this.props.onClear();
	}

	render() {
		return (
			<div className="d-flex align-items-center">
				<Input 
					placeholder="Cari.." 
					size="lg" 
					className="border-0 py-4" 
					style={{ boxShadow: "none" }} 
					value={ this.props.term } 
					onKeyPress={ (e) => this.handleKeyPress(e) }
					onChange={ (e) => this.props.onChange(e) }
					getRef={ (input) => { this.searchInput = input; }} />
				{
					this.state.searched ?
						<Button className="border-0 px-3" color="link" style={{ boxShadow: "none" }} onClick={ this.handleClear.bind(this) }><Icon name="remove" className="text-muted" /></Button> : null
				}
				<Button className="border-0 mr-3 px-3" color="link" style={{ boxShadow: "none" }}><Icon name="search" className="text-muted" /></Button>
			</div>
			);
	}

}

export default SearchBox