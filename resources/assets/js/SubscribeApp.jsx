import React from 'react'
import ReactDOM from 'react-dom'
import Axios from 'axios'
import Accounting from 'accounting'
import { Button, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'

import Icon from './component/Icon'

import PaymentOptions from './module/subscribe/PaymentOptions'
import BillingDetails from './module/subscribe/BillingDetails'

class SubscribeApp extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			options: [
				{
					id: 'mt-cc',
					label: 'Kartu Kredit / Debit',
					description: 'Pembayaran menggunakan kartu kredit MasterCard, VISA dan JCB. Paket langganan Anda akan aktif secara instan.'
				},
				{
					id: 'mt-ccinstallment',
					label: 'Cicilan Kartu Kredit BNI',
					description: 'Pembayaran cicilan menggunakan kartu kredit BNI selama 3, 6 atau 9 bulan. Paket langganan Anda akan aktif secara instan.'
				},
				{
					id: 'banktransfer',
					label: 'Transfer Bank',
					description: 'Pembayaran melalui transfer bank. Paket langganan Anda akan aktif setelah pembayaran diverifikasi dalam 2 hari kerja.'
				}
			],
			errors: {},
			subs: window.SubscribeApp.subscriptions,
			sub: false,
			method: false,
			billingDetails: {},
			loading: false,
			selectingSub: false
		};
	}

	handleSelectingSub() {
		this.setState({ selectingSub: !this.state.selectingSub });
	}

	handleSelectSub(sub) {
		this.setState({ sub: sub });
	}

	handleSelectMethod(id) {
		this.setState({ method: id });
	}

	handleBillingDetailsChange(details) {
		this.setState({ billingDetails: details });
	}

	handleContinue() {
		this.setState({ 
			loading: true,
			errors: {} 
		});
		Axios.post(this.state.method == 'banktransfer' ? 
			window.SubscribeApp.bniBillingUrl : 
			window.SubscribeApp.snapTokenUrl, 
			{
				subscription: this.state.sub.name,
				method: this.state.method,
				phone: this.state.billingDetails.phone,
				address: this.state.billingDetails.address,
				city: this.state.billingDetails.city,
				postalCode: this.state.billingDetails.postalCode
			}
		).then((response) => {
			this.setState({ loading: false });

			function changeResult(type, data) {
				$("#payment-form #result-type").val(type);
				$("#payment-form #result-data").val(JSON.stringify(data));
			}
			if (this.state.method == 'banktransfer') {
				$("#payment-form #payment-method").val('banktransfer');
				changeResult(response.data.status, response.data.result);
				$("#payment-form").submit();
			} else {
				$("#payment-form #payment-method").val('midtrans');
				snap.pay(response.data, {
					onSuccess: function(result){
						changeResult('success', result);
						$("#payment-form").submit();
					},
					onPending: function(result){
						changeResult('pending', result);
						$("#payment-form").submit();
					},
					onError: function(result){
						changeResult('error', result);
						$("#payment-form").submit();
					}
				});
			}
		}).catch((error) => {
			this.setState({ loading: false });
			if (error.response) {
				this.setState({ errors: error.response.data });
			}
		});
	}

	render() {
		return (
			<div>
				<div className="mb-3 lead"><span className="h1 text-primary">1.</span> Pilih periode berlangganan</div>
				<Dropdown isOpen={ this.state.selectingSub } toggle={ this.handleSelectingSub.bind(this) } className="mb-4">
					<DropdownToggle className="btn-block btn-light text-left px-4 py-3">
						{
							this.state.sub != false ? 
								(
									<div className="w-100 d-flex flex-wrap justify-content-between align-items-center">
										<div>{ this.state.sub.label }</div>
										<div className="h5 mb-0 text-primary">IDR { Accounting.formatNumber(this.state.sub.price) }</div>
									</div>
								) :
								(
									<div className="w-100 d-flex justify-content-between align-items-center">
										<div>Pilih periode berlangganan</div>
										<div><Icon name="angle-down" size="2" /></div>
									</div>
								)
						}
					</DropdownToggle>
					<DropdownMenu className="w-100">
						{
							this.state.subs.map((sub) => {
								return (
									<DropdownItem key={ sub.name } onClick={ () => { this.handleSelectSub(sub) }} className="d-flex flex-wrap justify-content-start">
										<div className="mr-2">{ sub.label }</div>
										<div>@ IDR { Accounting.formatNumber(sub.price) }</div>
									</DropdownItem>
									);
							})
						}
					</DropdownMenu>
				</Dropdown>

				{
					this.state.sub != false ?
						(
							<div>
								<div className="mb-3 lead"><span className="h1 text-primary">2.</span> Pilih metode pembayaran Anda</div>
								<PaymentOptions options={ this.state.options } selection={ this.state.method } onSelect={ this.handleSelectMethod.bind(this) } />
							</div>
						) : ''
				}

				{
					this.state.method != false ?
						(
							<div>
								<div>
									<div className="mb-3 lead"><span className="h1 text-primary">3.</span> Masukkan rincian pembayaran</div>
									<BillingDetails method={ this.state.method } onChange={ this.handleBillingDetailsChange.bind(this) } errors={ this.state.errors } />
								</div>
								<div>
									<Button color="primary" onClick={ this.handleContinue.bind(this) } disabled={ this.state.loading && "disabled" }>
										<span className="mr-2">Lanjutkan ke Pembayaran</span>
										{
											this.state.loading ?
											<Icon spin name="circle-o-notch" /> :
											<Icon name="angle-right" />
										}
									</Button>
								</div>
							</div>
							) : ''
				}
			</div>
			);
	}
};

ReactDOM.render(<SubscribeApp />, $('#subscribe-app').get(0));