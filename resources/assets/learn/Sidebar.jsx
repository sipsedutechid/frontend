import React from 'react';

const Sidebar = props => (
  <div className="sidebar">
    <div className="list-group">
      <a href="#" className={"list-group-item d-flex" + (!props.active ? ' active' : '')} onClick={ e => { e.preventDefault(); props.onChange(false); }}>
        <i className="fa fa-check fa-fw mr-3 text-success" />
        <div>1. Pendahuluan</div>
      </a>
      {
        props.topic.contents.map((content, index) => {
          return (
            <a
              href="#"
              key={ index }
              className={
                "list-group-item d-flex" +
                (props.active === index + 1 ? ' active' : '') +
                ((content.type === 'certificate' && !props.topic.passedAt) ? ' disabled' : '')}
              onClick={ e => {
                e.preventDefault();
                if (content.type === 'certificate' && !props.topic.passedAt)
                  return false;
                props.onChange(index + 1);
              }}>
              {
                content.type === 'certificate' ?
                  props.topic.certified && <i className="fa fa-check fa-fw mr-3 text-success" />
                  : <i className={"fa fa-fw mr-3 text-success" + (props.learnt.filter(content => content === index + 1).length ? ' fa-check' : '')} />
              }
              <div>
                <div>{ content.type !== 'certificate' && (index + 2 + '. ') }{ content.label }</div>
                {
                  content.type === 'certificate' &&
                  (
                    <div className="small">
                      { props.topic.certified && 'Unduh sertifikat Anda disini' }
                      { !props.topic.certified && props.topic.passedAt && 'Anda sudah dapat mengunduh sertifikat' }
                      { !props.topic.passedAt && 'Silakan mengerjakan tes soal untuk melulusi topik' }
                    </div>
                  )
                }
              </div>
            </a>
          );
        })
      }
    </div>
  </div>
);

export default Sidebar;