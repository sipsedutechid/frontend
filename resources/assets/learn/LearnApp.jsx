import React from 'react';
import { render } from 'react-dom';
import Main from './Main.jsx';

const LearnApp = () => (
  <div>
    <Main />
  </div>
)

render(<LearnApp />, document.getElementById('learn-app'));