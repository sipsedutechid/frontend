import React from 'react';

const ContentOverview = props => (
  <div className="container py-5">
    <div className="row justify-content-center">
      {
        props.loading == true ?
          <Loading /> :
          (
            <div className="col-lg-10">
              <h6 className="section-header text-muted">Topik Gakken P2KB&reg;</h6>

              <div className="mb-5">
                <h1>{ props.title }</h1>
              </div>

              <div className="d-flex mb-5">
                <div className="mr-4">
                  <div className="h6 text-uppercase text-muted ">Jumlah SKP</div>
                  <div className="lead">2</div>
                </div>
                <div className="mx-4">
                  <h6 className="text-uppercase text-muted">Standar Kelulusan</h6>
                  <div className="lead">80%</div>
                </div>
                <div className="mx-4">
                  <h6 className="text-uppercase text-muted">Pengajar</h6>
                  <div className="lead">{ props.lecturer }</div>
                </div>
              </div>

              <div className="mb-5" dangerouslySetInnerHTML={{ __html: props.summary }} />

            </div>
            )
      }
    </div>
  </div>
);

export default ContentOverview;