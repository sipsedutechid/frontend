import React from 'react'

const Loading = (props) => {

	return (
		<div className="d-flex flex-column justify-content-start align-items-center">
			<img src={ window.App.url.base + '/images/loading.svg' } style={{ width: 150 }} />
			<div>
				Memuat...
			</div>
		</div>
		);

}

export default Loading