import React, { Component } from 'react';
import ContentIframe from './ContentIframe'
import ContentImage from './ContentImage'
import ContentVideo from './ContentVideo'
import ContentPayment from './ContentPayment';

class ContentView extends Component {
  constructor(props) {
    super(props);
    this.state = {  };
  }

  componentWillReceiveProps(nextProps) {

  }

  render() {
    let view = null;

    if (this.props.type == 'video')
      view = <ContentVideo content={ this.props } videoUrl={ this.props.data } onComplete={ this.props.onComplete } />;
    else if (this.props.type == 'image')
      view = <ContentImage url={ this.props.data } content={ this.props } onComplete={ this.props.onComplete } />;
    else if (this.props.type == 'certificate' && this.props.topic.passedAt && !this.props.topic.certified)
      view = <ContentPayment topic={ this.props.topic } onSuccess={ this.props.onPaymentSuccess } />
    else
      view = <ContentIframe url={ this.props.data } content={ this.props } onComplete={ this.props.onComplete } />;

    return view;
  }
}

export default ContentView;