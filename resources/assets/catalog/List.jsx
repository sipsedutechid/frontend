import React, { Component } from 'react';
import Axios from 'axios';
import ListItem from './ListItem';
import ListFilter from './ListFilter';

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fetching: false,
      error: false,
      topics: [],
      filterCategory: false,
      filterTerm: '',
    };
    this.cancelToken = false;
  }

  componentDidMount() {
    this.load();
  }

  componentWillUnmount() {
    this.cancelToken && this.cancelToken();
  }

  load() {
    if (this.state.fetching)
      return;

    this.setState({ fetching: true, error: false });
    Axios.get(window.CatalogApp.baseUrl + '/p2kb', {
      params: {
        json: 'true'
      },
      cancelToken: new Axios.CancelToken(c => { this.cancelToken = c; })
    })
      .then(response => {
        this.setState({ topics: response.data.data.slice(0), fetching: false });
        this.cancelToken = false;
      })
      .catch(() => {
        this.setState({ error: true, fetching: false });
        this.cancelToken = false;
      });
  }

  filter({ category = null, term = null }) {
    if (category !== null)
      this.setState({ filterCategory: category });
    if (term !== null)
      this.setState({ filterTerm: term });
  }

  render() {
    return (
      <div>
        <ListFilter { ...this.state } filter={ this.filter.bind(this) } />
        <div className="row">
          {
            this.state.topics.map(topic => {
              if (this.state.filterCategory && (topic.category !== this.state.filterCategory))
                return null;
              if (this.state.filterTerm.length && topic.title.toLowerCase().indexOf(this.state.filterTerm.toLowerCase()) < 0)
                return null;
              return (
                <ListItem key={ topic.slug } { ...topic } />
              );
            })
          }
        </div>
      </div>

    );
  }
}

export default List;