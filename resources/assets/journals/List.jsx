import React from 'react';

const List = props => (
  <div className="row">
    {
      props.journals.map(journal => (
        <div className="col-3 mb-4" key={ journal.slug }>
          <img src={ journal.imageUrl } className="w-100 rounded mb-2" />
          <div className="text-muted small mb-1">{ journal.publisher } / { journal.issn }</div>
          <div><a href={ window.App.baseUrl + '/journals/' + journal.slug } className="h6 text-dark">{ journal.title }</a></div>
        </div>
      ))
    }
  </div>
);

export default List;