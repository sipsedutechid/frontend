import React, { Component } from 'react';
import { render } from 'react-dom';
import Axios from 'axios';

import Filter from './Filter';
import List from './List';
import Search from './Search';

class JournalsApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterCategory: false,
      filterTerm: '',
      journals: [],
      fetching: false,
      error: false,
    };
    this.cancelToken = false;
  }

  componentDidMount() {
    this.load({ filterCategory: this.state.filterCategory, filterTerm: this.state.filterTerm});
  }

  componentWillUnmount() {
    this.cancelToken && this.cancelToken();
  }

  load({ filterCategory = false, filterTerm = '' }) {
    this.cancelToken && this.cancelToken();

    this.setState({
      fetching: true,
      error: false,
      filterCategory,
      filterTerm,
    });
    Axios.get(window.App.baseUrl + '/journals', {
      params: {
        json: 'true',
        category: filterCategory || null,
        term: filterTerm.length ? filterTerm : null,
      },
      cancelToken: new Axios.CancelToken(c => { this.cancelToken = c; })
    })
      .then(response => {
        this.setState({ journals: response.data.data, fetching: false });
        this.cancelToken = false;
      })
      .catch(() => {
        this.setState({ fetching: false, error: true });
        this.cancelToken = false;
      })
  }

  render() {
    return (
      <div className="row">

        <div className="d-none d-md-block col-md-4">
          <Filter active={ this.state.filterCategory } onChange={ filterCategory => this.load({ filterCategory, filterTerm: this.state.filterTerm }) } />
        </div>
        <div className="col-md-8">
          <Search onSearch={ filterTerm => this.load({ filterTerm, filterCategory: this.state.filterCategory }) } />
          <List journals={ this.state.journals } />
        </div>

      </div>
    );
  }
}

render(<JournalsApp />, document.getElementById('journals-app'));