import plans from './Plans'
import roles from './Roles'
import topics from './Topics'
import users from './Users'
import apps from './Apps'
import { combineReducers } from 'redux'

export default combineReducers({ plans, roles, topics, users, apps });