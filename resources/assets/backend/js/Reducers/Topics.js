import { TOPICS_LIST_POPULATE } from '../Actions/Types'

export default (state = {}, action) => {

	let newState = Object.assign({}, state);

	switch(action.type) {
		case TOPICS_LIST_POPULATE:
			newState.list.data = action.topics.slice(0);
			break;
	}

	return newState;

}