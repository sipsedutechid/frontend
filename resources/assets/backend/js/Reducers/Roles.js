import { ROLES_LIST_POPULATE, ROLES_LIST_INVALIDATE } from '../Actions/Types'

export default (state = {}, action) => {

	let newState = Object.assign({}, state);

	switch(action.type) {
		case ROLES_LIST_POPULATE:
			newState.list.data = action.roles.slice(0); 
			newState.list.isFetching = false;
			break;
		case ROLES_LIST_INVALIDATE:
			newState.list.isFetching = true;
	}

	return newState;

}