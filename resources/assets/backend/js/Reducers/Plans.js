import { PLANS_LIST_INVALIDATE, PLANS_LIST_POPULATE } from '../Actions/Types'

export default (state = {}, action) => {

	let newState = Object.assign({}, state);

	switch(action.type) {
		case PLANS_LIST_INVALIDATE:
			newState.list.isFetching = true;
			return newState;

		case PLANS_LIST_POPULATE: 
			newState.list.data = action.data.data.slice(0);
			newState.list.isFetching = false;

	}

	return newState;

}