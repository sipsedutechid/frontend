import Axios from 'axios'

export default class TopicsApi {

	static get() {
		return Axios.get(App.restUrl + '/topics')
			.then(response => response.data.data)
			.catch(error => error.response.data);
	}

}