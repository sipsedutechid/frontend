import Axios from 'axios'

export default class UsersApi {

	static get(params = {}, url = null) {
		return Axios.get(url == null ? App.restUrl + '/backend-users' : url, { params }).then(response => {
			return response.data.data;
		}).catch(error => error);
	}

	static single(email) {
		return Axios.get(App.restUrl + '/backend-users/' + email)
			.then(response => response.data)
			.catch(error => error);
	}

	static post(data) {
		return Axios.post(App.restUrl + '/backend-users', data)
			.then(response => response.data)
			.catch(error => error);
	}

	static patch(email, data) {
		return Axios.patch(App.restUrl + '/backend-users/' + email, data)
			.then(response => response.data)
			.catch(error => error);
	}

}