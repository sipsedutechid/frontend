import Axios from 'axios'

export default class UsersApi {

	static post(data) {
		return Axios.post(App.restUrl + '/certificate/redeem', data)
			.then(response => response.data)
			.catch(error => error);
	}
}