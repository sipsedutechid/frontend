import React from 'react'
import { NavLink, Redirect, Route, Switch } from 'react-router-dom'

import Users from './Users/Users'
import Apps from './Apps/Apps'

export default class Main extends React.Component {

	render() {
		return (
			<div>
				<div className="sidebar bg-dark">
					<nav className="nav flex-column">
						<li className="nav-item">
							<NavLink className="nav-link text-white text-uppercase py-3" to="/users">
								<strong>Users and Roles</strong>
							</NavLink>
						</li>
						<li className="nav-item">
							<NavLink exact className="nav-link text-white" to="/users/all">
								All Users
							</NavLink>
						</li>
						<li className="nav-item">
							<NavLink exact className="nav-link text-white" to="/users/roles/all">
								All Roles
							</NavLink>
						</li>
						<li className="nav-item">
							<NavLink exact className="nav-link text-white" to="/apps/all">
								All Apps
							</NavLink>
						</li>
					</nav>
				</div>
				<div className="main-container bg-light" style={{ overflowY: 'auto' }}>
					<Switch>
						<Route path="/users" render={ () => <Users { ...this.props } /> } />
						<Route path="/apps" render={ () => <Apps { ...this.props } /> } />
						<Redirect to="/users" />
					</Switch>
				</div>
			</div>
			);
	}

}