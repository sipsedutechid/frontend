import React from 'react'
import { Link } from 'react-router-dom'

export default class List extends React.Component {

	componentDidMount() {
		return this.props.loadList();
	}

	render() {
		return (
			<div>
				<div className="headerbar bg-white">
					<div className="container-fluid py-3 d-flex justify-content-between align-items-center">
						<div className="btn-toolbar align-items-center">
							<div className="btn-group">
								<button 
									type="button" 
									className="btn btn-light" 
									onClick={ e => this.props.loadList({ type: 'reload' }) }
									disabled={ this.props.isFetching }>
									<i className="fa fa-fw fa-refresh" />
								</button>
							</div>
						</div>
						<div className="h5 m-0">
							All Roles
						</div>
						<div>
						</div>
					</div>
				</div>
				<div className="main-container">
					<div className="container py-5">
						{ this.props.isFetching ? <p className="text-center">Loading roles list..</p> : null }

						<div className={ "card fade" + (!this.props.isFetching ? ' show' : '' )}>
							<div className="card-body">
								{ this.props.data.length } roles found
							</div>
							<table className="table table-hover">
								<thead className="small text-uppercase text-muted">
									<tr>
										<th>Role</th>
										<th width="150">Created</th>
									</tr>
								</thead>
								<tbody>
									{
										this.props.data.map((role, i) => {
											return (
												<tr key={ i } style={{ transition: 'background 0.1s ease-out' }}>
													<td>
														<Link to={ '/users/roles/' + role.id }>{ role.name }</Link>
														<div className="text-muted small">
															{ role.usersCount == 0 ? 'No' : role.usersCount } assigned user(s)
														</div>
													</td>
													<td className="align-middle">
														{ role.createdAt }
													</td>
												</tr>
												);
										})
									}
								</tbody>
								<tfoot className="small text-uppercase text-muted">
									<tr>
										<th>Role</th>
										<th>Created</th>
									</tr>
								</tfoot>
							</table>
						</div>

					</div>
				</div>
			</div>
			);
	}

}