import React from 'react'

export default class Edit extends React.Component {

	render() {
		let disabled = this.props.isSaving || this.props.isFetching;
		return (
			<div className="mb-5">
				<div className="row">
					<div className="col-6">
						<div className="form-group">
							<label className="col-form-label">Role Name</label>
							<input type="text" className="form-control" disabled={ disabled } value={ this.props.role.name } onChange={ e => this.props.onChange({ name: e.target.value }) } />
						</div>
						<div className="form-group">
							<button type="button" className="btn btn-primary" onClick={ () => this.props.onSave() } disabled={ disabled }>Save Changes</button>
						</div>
					</div>
				</div>
			</div>
			);
	}

}