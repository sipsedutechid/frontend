import React from 'react'

export default props => 

	(
		<div>
			{
				!props.user.topics.length ?
					<p>{ props.user.name } is not enrolled to any topic.</p> :
					props.user.topics.map((topic, i) => {
						return (
							<div key={ i }>
								<div className="py-2">
									<div className="lead">{ topic.topic }</div>
									<div className="d-flex justify-content-start">
										<span className="mr-3">
											<span className="text-muted">Status</span> { topic.status }
										</span>
										{
											topic.grade > 0 ?
												(
													<span className="mr-3">
														<span className="text-muted">Grade</span> { topic.grade }
													</span>
													) : null
										}
										{ 
											topic.certificate !== null ? 
												(
													<span className="mr-3">
														<span className="text-muted">Certificate No</span> { topic.certificate }
														<a className="ml-2" href={ App.rootUrl + '/topics/certificate/' + topic.slug + '?user=' + props.user.email } target="_blank" title="Print certificate"><i className="fa fa-print" /></a>
													</span>
													) : null
										}
										{
											topic.status === 'Passed' && !topic.certificate && (
												props.certificateClaimSlug === topic.slug ? 
												<a>Please wait... </a> :
												<a onClick={ props.claimCertificate } data-topic={ topic.slug } style={{cursor:'pointer'}}>Claim certificate</a>
											)
										}
									</div>
								</div>
								<hr />
							</div>
							);
						})
			}
			</div>
		)