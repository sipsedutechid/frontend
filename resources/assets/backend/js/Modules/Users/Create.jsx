import React from 'react'
import { Link, Redirect } from 'react-router-dom'
import { Typeahead } from 'react-typeahead'

export default class Create extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			user: {
				email: '',
				name: '',
				role: { backendCapable: false }
			},
			isSearchingRole: false,
			donePosting: false
		};
	}

	componentDidMount() {
		this.props.loadRoles();
	}

	handleChange(field, value) {
		let user = this.state.user;
		user[field] = value;
		return this.setState({ user: Object.assign({}, user) });
	}

	render() {
		if (this.props.donePosting)
			return <Redirect to={ '/users/' + this.state.user.email } />;

		return (
			<div>
				<div className="headerbar bg-white d-flex align-items-center">
					<div className="container-fluid py-3 d-flex justify-content-between align-items-center">
						<div><Link to="/users/all"><i className="fa fa-angle-left" /> Users List</Link></div>
						<div className="h5 m-0">
							Create New User
						</div>
						<div />
					</div>
				</div>
				
				<div className="container py-5">
					<div className="row justify-content-center">
						<div className="col-6">
							<div className="mb-4">
								<div className="form-group row">
									<label htmlFor="email" className="col-md-3 col-form-label">Email Address</label>
									<div className="col-md-9">
										<input type="email" className="form-control" value={ this.state.user.email } onChange={ e => this.handleChange('email', e.target.value) } disabled={ this.props.isPosting } />
									</div>
								</div>
								<div className="form-group row">
									<label htmlFor="name" className="col-md-3 col-form-label">Full Name</label>
									<div className="col-md-9">
										<input type="text" className="form-control" value={ this.state.user.name } onChange={ e => this.handleChange('name', e.target.value) } disabled={ this.props.isPosting } />
									</div>
								</div>
								<div className="form-group row">
									<label htmlFor="role" className="col-md-3 col-form-label">Role</label>
									<div className="col-md-9">
										<div style={{ position: 'relative' }}>
											<Typeahead 
												maxVisible={ 5 } 
												filterOption="name" 
												displayOption="name"
												placeholder="Search roles.."
												disabled={ this.props.isPosting }
												options={ this.props.roles.data } 
												onBlur={ () => this.setState({ isSearchingRole: false }) }
												onOptionSelected={ role => this.setState({ user: Object.assign(this.state.user, { role }) }) } 
												onKeyDown={ () => this.setState({ isSearchingRole: true, user: Object.assign(this.state.user, { role: { backendCapable: false } }) }) }
												customClasses={{ 
													input: 'form-control', 
													listItem: 'dropdown-item', 
													results: 'dropdown-menu w-100' + (this.state.isSearchingRole ? ' show' : '')
												}} />
										</div>
										{
											this.state.user.role.backendCapable ?
												<div className="text-muted small pt-2">This user will have access to backend resources</div> : null
										}
									</div>
								</div>
							</div>

							<a href="#">Advanced options <i className="fa fa-angle-down" /></a>
							<div className="mb-5">

							</div>

							<button className="btn btn-primary" disabled={ this.props.isPosting } onClick={ () => this.props.post(this.state.user) }>Save New User</button>
						</div>
					</div>
				</div>
			</div>
			);
	}

}