import List from './List'
import React from 'react'
import Create from './Create'
import Single from './Single'
import Roles from '../Roles/Roles'
import { Redirect, Route, Switch } from 'react-router-dom'

export default props => 
	(
		<Switch>
			<Route exact path="/users/all" render={ () => {
				return <List { ...props.users.list } { ...props.actions.Users } /> ;
			}} />

			<Route path="/users/roles" render={ () => {
				return <Roles { ...props } /> ;
			}} />

			<Route path="/users/new" render={ () => {
				return (
					<Create 
						{ ...props.users.create } 
						{ ...props.actions.Users } 
						roles={ props.roles.list } 
						loadRoles={ props.actions.Roles.loadList }
						donePosting={ props.users.single.isNewlyAdded } />
						);
			}} />

			<Route path="/users/:email" render={ (route) => {
				return (
					<Single 
						{ ...{ route } }
						{ ...props.users.single } 
						{ ...props.actions.Users }
						topics={ props.topics.list }
						loadTopics={ props.actions.Topics.loadList }
						plans={ props.plans } 
						onLoadPlans={ props.actions.Plans.loadList } />
						);
			}} />

			<Redirect to="/users/all" />
		</Switch>
		);

