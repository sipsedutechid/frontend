import React from 'react'
import { Link, NavLink, Route, Switch } from 'react-router-dom'
import { GLOBAL_FETCHING, USERS_SINGLE_TITLE, USERS_SINGLE_SECTIONS } from '../../Lang'

import EditForm from './Edit'
import Learning from './Learning'
import Subscription from './Subscription'

export default class Single extends React.Component {

	constructor(props) {
		super(props);
	}

	loadTopic() {
		this.props.loadTopics();
	}

	componentDidMount() {
		this.props.load(this.props.route.match.params.email);
	}

	handleSave() {
		if (this.props.isFetching)
			return;
		return this.props.patch();
	}

	render() {
		return (
			<div>
				<div className="headerbar bg-white d-flex align-items-center">
					<div className="container-fluid py-3 d-flex justify-content-between align-items-center">
						<div><Link to="/users/all"><i className="fa fa-angle-left" /> Users List</Link></div>
						<div className="h5 m-0">
							{ USERS_SINGLE_TITLE } { this.props.isFetching ? GLOBAL_FETCHING : this.props.user.email }
						</div>
						<div>
						</div>
					</div>
				</div>
				
				<div className="container py-5">
					<div className="row">
						<div className="col-2 sticky-top">
							<ul className="nav nav-pills flex-column">
								<li className="nav-item"><NavLink exact to={ '/users/' + this.props.route.match.params.email } className="nav-link">{ USERS_SINGLE_SECTIONS.OVERVIEW }</NavLink></li>
								<li className="nav-item"><NavLink exact to={ '/users/' + this.props.route.match.params.email + '/subs' } className="nav-link">{ USERS_SINGLE_SECTIONS.SUBSCRIPTIONS }</NavLink></li>
								<li className="nav-item"><NavLink exact to={ '/users/' + this.props.route.match.params.email + '/learning' } className="nav-link">{ USERS_SINGLE_SECTIONS.LEARNING }</NavLink></li>
							</ul>
						</div>
						<div className="col-10">
							<Switch>
								<Route exact path={ '/users/:email' } render={ () => <EditForm { ...this.props } /> } />
								<Route exact path={ '/users/:email/subs' } render={ () => <Subscription { ...this.props } /> } />
								<Route exact path={ '/users/:email/learning' } render={ () => <Learning { ...this.props } loadTopic={ this.loadTopic.bind(this) }/> } />
							</Switch>
						</div>
					</div>
				</div>
			</div>
			);
	}

}