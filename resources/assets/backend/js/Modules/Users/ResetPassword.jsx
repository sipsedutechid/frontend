import React from 'react'
import { GLOBAL_RESET, USERS_SINGLE_PASSWORD_RESET_HELP } from '../../Lang'

export default (props) => {

	return (
		<div className="my-5 d-flex justify-content-between align-items-center">
			<div>
				<h4>Reset Password</h4>
				<p className="m-0">{ USERS_SINGLE_PASSWORD_RESET_HELP }</p>
			</div>
			<div>
				<button type="button" onClick={ () => props.onReset() } disabled={ props.isFetching } className="btn btn-primary">{ GLOBAL_RESET }</button>
			</div>
		</div>
		)

}