import React from 'react'
import { Link , Redirect } from 'react-router-dom'
import {
	APPS_LIST_TITLE ,
	APPS_FORM
} from '../../Lang'

export default class Edit extends React.Component {
  constructor(props) {
		super(props);
		this.state = {
			app: {
				name: '',
				url: ''
			},
			donePosting: false
		};
	}

	handleChange(field, value) {
		let app = this.state.app;
		app[field] = value;
		return this.setState({ app: Object.assign({}, app) });
	}

  render() {
		if (this.props.donePosting)
			return <Redirect to={ '/apps/all' } />;

    return (
      <div>
        <div className="headerbar bg-white d-flex align-items-center">
          <div className="container-fluid py-3 d-flex justify-content-between align-items-center">
            <div>
              <Link to="/apps/all">
                <i className="fa fa-angle-left" /> { APPS_LIST_TITLE }
              </Link>
            </div>
            <div className="h5 m-0">Create New App</div>
            <div />
          </div>
        </div>

        <div className="container py-5">
          <div className="row justify-content-center">
            <div className="col-6">
              <div className="mb-4">
                <div className="form-group row">
                  <label className="col-md-3 col-form-label">
                    { APPS_FORM.NAME }
                  </label>
                  <div className="col-md-9">
                    <input
                      type="text"
                      className="form-control"
                      // value={this.state.user.email}
                      onChange={e => this.handleChange("name", e.target.value)}
                      disabled={this.props.isPosting}
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="name" className="col-md-3 col-form-label">
                    { APPS_FORM.URL }
                  </label>
                  <div className="col-md-9">
                    <input
                      type="text"
                      className="form-control"
                      // value={this.state.user.name}
                      onChange={e => this.handleChange("url", e.target.value)}
                      disabled={this.props.isPosting}
                    />
                  </div>
                </div>
              </div>

              <button
                className="btn btn-primary"
                disabled={this.props.isPosting}
                onClick={() => this.props.post(this.state.app)}
              >
                { APPS_FORM.SAVE }
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
