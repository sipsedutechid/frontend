import List from './List'
import React from 'react'
import Form from './Form'
import Single from './Single'
// import Roles from '../Roles/Roles'
import { Redirect, Route, Switch } from 'react-router-dom'

export default props =>  (
  <Switch>
    <Route exact path="/apps/all" render={ () => {
      return <List  { ...props.apps.list } { ...props.actions.Apps }  /> ;
    }} />

    <Route path="/apps/new" render={ () => {
      return (
        <Form 
          { ...props.apps.create } 
          { ...props.actions.Apps }
          donePosting={ props.apps.single.isNewlyAdded } />
          );
    }} />

    <Route path="/apps/:id" render={ (route) => {
      return (
        <Single 
          { ...{ route } }
          { ...props.apps } 
          { ...props.actions.Apps }
        />
          );
    }} />

    <Redirect to="/apps/all" />
  </Switch>
);

