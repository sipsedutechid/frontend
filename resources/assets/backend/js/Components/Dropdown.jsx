import React from 'react'

export default class Dropdown extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			isDropped: false,
			selection: Object.assign({}, props.selection)
		};
	}

	componentWillReceiveProps(nextProps) {
		this.setState({ selection: Object.assign({}, nextProps.selection) });
	}

	handleToggle(toggle = null) {
		this.setState({ isDropped: toggle === null ? !this.state.isDropped : toggle });
	}

	handleSelect(selection) {
		this.handleToggle(false);
		return this.props.onSelect(selection);
	}

	handleLeave() {
		return this.handleToggle(false);
	}

	render() {
		return (
			<div className={ 'dropdown' + (this.state.isDropped ? ' show' : '') }>
				<button 
					type="button" 
					className={ 
						'btn dropdown-toggle d-flex justify-content-between align-items-center btn-' + this.props.color + 
						(this.props.block ? ' btn-block text-left' : '') }
					onClick={ this.handleToggle.bind(this) }
					onBlur={ this.handleLeave.bind(this) }>
					<div>{ this.state.selection[this.props.labelKey] != undefined ? this.state.selection[this.props.labelKey] : this.props.children }</div>
					<div><i className={ 'fa fa-angle-' + (this.state.isDropped ? 'up' : 'down') } /></div>
				</button>
				<div className={ 
					'dropdown-menu' + 
					(this.state.isDropped ? ' show' : '') +
					(this.props.block ? ' w-100' : '')
				}>
					{
						this.props.options.map((option, i) => {
							return (
							<a key={ i } href="#" onMouseDown={ e => this.handleSelect(option) } className="dropdown-item">
								{ option[this.props.labelKey] }
							</a>
							)
						})
					}
				</div>
			</div>
			);

	}

}