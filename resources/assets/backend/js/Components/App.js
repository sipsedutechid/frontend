import Main from '../Modules/Main'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter } from 'react-router-dom'
import ActionCreators from '../Actions/Index'

function mapStateToProps(state) {
	return {
		plans: state.plans,
		roles: state.roles,
		topics: state.topics,
		users: state.users ,
		apps: state.apps
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: {
			Plans: bindActionCreators(ActionCreators.Plans, dispatch),
			Roles: bindActionCreators(ActionCreators.Roles, dispatch),
			Topics: bindActionCreators(ActionCreators.Topics, dispatch),
			Users: bindActionCreators(ActionCreators.Users, dispatch),
			Apps: bindActionCreators(ActionCreators.Apps, dispatch)
		}
	};
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));