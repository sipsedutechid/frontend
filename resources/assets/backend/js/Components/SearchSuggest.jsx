import React from "react";
import Autosuggest from 'react-autosuggest';

export default class Single extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      suggestions: [] ,
      value: ""
    };
    this.delayCallSuggestion = false;
  }

  onSuggestionsFetchRequested ({ value }) {
    const { getSuggestions } = this.props;
    if (getSuggestions) {
      if (this.delayCallSuggestion) clearTimeout( this.delayCallSuggestion );

      if (value.length > 2) {
        this.delayCallSuggestion = setTimeout( async () => { 
          const suggestions = value.length === 0 ? [] : await getSuggestions( value );
          this.setState({
            suggestions
          });
        }, 1000);
      }
    }
  };

  onSuggestionsClearRequested () {
    this.setState({
      suggestions: []
    });
  };

  onSuggestionSelected(event, { suggestion }){
    const { onChange } = this.props;
    this.setState({
      value: suggestion.label
    });
    if ( onChange ) onChange( suggestion.value );
  }

  onChange(e , { newValue }) {
    const value = String(newValue);
    this.setState({
      value
    });
  }

  clear() {
    const { onChange } = this.props;
    this.setState({
      value: "" ,
    });

    if ( onChange ) onChange( null );
  }

  renderSuggestion ( suggestion ) {
    return <div> { suggestion.label } </div>;
  }

  render() {    
    const { suggestions , value } = this.state;
    const { placeholder , className , disabled } = this.props;
    return (
      <Autosuggest
        suggestions={ suggestions }
        onSuggestionsFetchRequested={ this.onSuggestionsFetchRequested.bind(this) }
        onSuggestionsClearRequested={ this.onSuggestionsClearRequested.bind(this) }
        getSuggestionValue={ suggestion => suggestion.value}
        onSuggestionSelected= { this.onSuggestionSelected.bind(this) }
        renderSuggestion={ this.renderSuggestion }
        inputProps={
          {
            placeholder: placeholder || "",
            value,
            className: className || "" ,
            onChange: this.onChange.bind(this) ,
            disabled
          }
        }
      />
    );
  }
}
