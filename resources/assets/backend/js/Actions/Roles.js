import RolesApi from '../Api/Roles'
import { ROLES_LIST_POPULATE, ROLES_LIST_INVALIDATE, ROLES_LIST_SET_FETCH_ERROR } from './Types'

export const loadList = (context = {type: 'init'}) =>
	(dispatch, getState) => {
		if (getState().roles.list.data.length && context.type !== 'reload')
			return;

		dispatch(invalidateRoles());
		RolesApi.get()
			.then(roles => dispatch(populateRoles(roles.data)))
			.catch(e => { type: ROLES_LIST_SET_FETCH_ERROR });
	}

export const populateRoles = roles => {
	return {
		type: ROLES_LIST_POPULATE,
		roles
	}
}

export const invalidateRoles = () => {
	return {
		type: ROLES_LIST_INVALIDATE
	}
}