import UsersApi from '../Api/Users'
import { 
	USERS_LIST_CHANGE_PAGE,
	USERS_LIST_INVALIDATE, 
	USERS_LIST_POPULATE, 
	USERS_LIST_SET_SEARCH_TERM,
	USERS_POST,
	USERS_SINGLE_ENROL,
	USERS_SINGLE_INVALIDATE,
	USERS_SINGLE_SET_USER,
	USERS_SINGLE_SUBSCRIBE
} from './Types'

export const loadList = (context = {type: 'init'}) =>
	(dispatch, getState) => {
		if (!getState().users.list.data.length || context.type == 'reload') {
			dispatch(invalidateList());
			UsersApi.get({
				term: getState().users.list.searchTerm.length ? getState().users.list.searchTerm : null
			})
				.then(users => dispatch(populateList(users)))
				.catch(error => {
					throw(error);
				});
		}
	}

export const load = (email, context = { type: 'init' }) => 
	(dispatch, getState) => {
		if (
			getState().users.single.user.email === email
			&& getState().users.single.user.name !== undefined
			&& context.type !== 'reload'
			)
			return;

		dispatch(invalidate());
		UsersApi.single(email)
			.then(data => dispatch(set(data)))
			.catch(error => {
				throw(error);
			});
	}

export const post = (data) => 
	(dispatch, getState) => {
		dispatch(processPost());
		UsersApi.post(data)
			.then(user => dispatch(set(user, true)))
			.catch(error => { throw(error) });
	}

export const patch = (email, data) =>
	(dispatch, getState) => {
		dispatch(invalidate());
		UsersApi.patch(email, Object.assign({}, data))
			.then(user => dispatch(set(user)))
			.catch(error => { dispatch(set(getState().users.single)) });
	}

export const resetPassword = email => 
	(dispatch, getState) => {
		if (getState().users.single.isFetching)
			return;
		dispatch(invalidate());
		UsersApi.patch(email, { action: 'resetPassword' })
			.then(() => dispatch(set(getState().users.single)))
			.catch(error => dispatch(set(getState().users.single)));
	}

export const cancelSubscription = () =>
	(dispatch, getState) => {
		if (getState().users.single.isFetching)
			return;
		UsersApi.patch(getState().users.single.user.email, {
			action: 'unsubscribe'
		})
		.then(() => dispatch(load(getState().users.single.user.email, { type: 'reload' })))
		.catch(e => { throw(e) });
	}

export const setSearchTerm = term => {
	return {
		type: USERS_LIST_SET_SEARCH_TERM,
		term
	};
}

export const invalidateList = () => {
	return {
		type: USERS_LIST_INVALIDATE
	};
}

export const populateList = users => {
	return {
		type: USERS_LIST_POPULATE,
		users
	};
}

export const invalidate = () => {
	return {
		type: USERS_SINGLE_INVALIDATE
	};
}

export const pager = (next = true) => {
	return {
		type: USERS_LIST_CHANGE_PAGE,
		next
	}
}

export const set = (data, newlyAdded = false) => {
	return {
		type: USERS_SINGLE_SET_USER,
		newlyAdded,
		data
	};
}

export const subscribe = plan =>
	(dispatch, getState) => {
		dispatch(processSubscription());
		return UsersApi.patch(getState().users.single.user.email, {
			action: 'subscribe',
			plan
		})
		.then(() => dispatch(load(getState().users.single.user.email, {type: 'reload'})))
		.catch(e => { throw(e) });
	}

export const enrol = topic =>
	(dispatch, getState) => {
		dispatch(processEnrolment());
		return UsersApi.patch(getState().users.single.user.email, {
			action: 'enrol',
			topic
		})
		.then(() => dispatch(load(getState().users.single.user.email, {type: 'reload'})))
		.catch(e => { throw(e) });
	}

export const processPost = () => {
	return {
		type: USERS_POST
	}
}

export const processSubscription = () => {
	return {
		type: USERS_SINGLE_SUBSCRIBE
	}
}

export const processEnrolment = () => {
	return {
		type: USERS_SINGLE_ENROL
	}
}