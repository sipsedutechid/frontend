import AppsApi from '../Api/Apps'
import { 
	APPS_LIST_CHANGE_PAGE,
	APPS_LIST_INVALIDATE, 
	APPS_LIST_POPULATE, 
	APPS_LIST_SET_SEARCH_TERM,
	APPS_POST,
  APPS_SINGLE_INVALIDATE,
	APPS_SINGLE_SET_APP ,
	APP_USERS_LIST_POPULATE ,
	APP_USERS_LIST_INVALIDATE
} from './Types'

export const loadList = (context = {type: 'init'}) =>
	(dispatch, getState) => {
		if (!getState().apps.list.data.length || context.type == 'reload') {
			dispatch(invalidateList());
			AppsApi.get({
				term: getState().apps.list.searchTerm.length ? getState().apps.list.searchTerm : null
			})
				.then(apps => dispatch(populateList(apps)))
				.catch(error => {
					throw(error);
				});
		}
	}

export const load = (id, context = { type: 'init' }) => 
	(dispatch, getState) => {
		if (
			getState().apps.single.app.id === id
			&& getState().apps.single.app.name !== undefined
			&& context.type !== 'reload'
			)
			return;

		dispatch(invalidate());
		AppsApi.single(id)
			.then(data => dispatch(set(data)))
			.catch(error => {
				throw(error);
			});
	}

export const post = (data) => 
	(dispatch, getState) => {
		dispatch(processPost());
		AppsApi.post(data)
			.then(app => dispatch(set(app, true)))
			.catch(error => { throw(error) });
	}

export const patch = (id, data) =>
	(dispatch, getState) => {
		dispatch(invalidate());
		AppsApi.patch(id, Object.assign({}, data))
			.then(app => dispatch(set(app)))
			.catch(error => { dispatch(set(getState().apps.single)) });
	}

export const loadUsers = (id, context = {type: 'init'}, page = 1) =>
	(dispatch, getState) => {
		if (!getState().apps.users.list.data.length || context.type == 'reload') {
			dispatch(invalidateUserList());
			AppsApi.getAvailableUsers(id, { page })
				.then(users => dispatch(populateListUsers(users)))
				.catch(error => {
					throw(error);
				});
		}
	}

export const pagerUser = (id, next = true) =>
	(dispatch, getState) => {
		const page = getState().apps.users.list.pagination.page + (next ? 1 : -1);

		dispatch(invalidateUserList());
		AppsApi.getAvailableUsers(id, { page })
			.then(users => dispatch(populateListUsers(users)))
			.catch(error => {
				throw(error);
			});
	}

export const setSearchTerm = term => {
	return {
		type: APPS_LIST_SET_SEARCH_TERM,
		term
	};
}

export const invalidateList = () => {
	return {
		type: APPS_LIST_INVALIDATE
	};
}

export const populateList = apps => {
	return {
		type: APPS_LIST_POPULATE,
		apps
	};
}

export const invalidate = () => {
	return {
		type: APPS_SINGLE_INVALIDATE
	};
}

export const pager = (next = true) => {
	return {
		type: APPS_LIST_CHANGE_PAGE,
		next
	}
}

export const set = (data, newlyAdded = false) => {
	return {
		type: APPS_SINGLE_SET_APP,
		newlyAdded,
		data
	};
}

export const processPost = () => {
	return {
		type: APPS_POST
	}
}

export const populateListUsers = users => {
	return {
		type: APP_USERS_LIST_POPULATE,
		users
	};
}

export const invalidateUserList = () => {
	return {
		type: APP_USERS_LIST_INVALIDATE
	};
}