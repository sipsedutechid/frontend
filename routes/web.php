<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('infographics', 'InfographicController@index')->middleware('pageview');

Route::get('podcasts', 'PodcastController@index')->middleware('pageview');

Route::get('paramountbed', 'SponsorController@view')->middleware('pageview');
Route::get('prodia', 'SponsorController@prodia')->middleware('pageview');

Route::name('home')->get('/', 'HomeController@index')->middleware('pageview');

// Auth::routes();
Route::name('verify')->get('auth/verify', 'ProfileController@verify');
Route::name('provider.login')->get('/auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::name('provider.callback')->get('/auth/callback/{provider}', 'Auth\LoginController@handleProviderCallback');
Route::name('login')->get('login', 'Auth\LoginController@login');
Route::name('login')->post('login', 'Auth\LoginController@doLogin');
Route::name('logout')->post('logout', 'Auth\LoginController@logout');
Route::name('logout')->get('logout', 'Auth\LoginController@logout');
Route::name('password.email')->post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::name('password.request')->get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('/password/reset', 'Auth\ForgotPasswordController@reset');
Route::name('password.reset')->post('/password/reset/{token}', 'Auth\ForgotPasswordController@showResetForm');
Route::name('register')->get('/register', 'Auth\RegisterController@showRegisterForm');
Route::post('/register', 'Auth\RegisterController@register');
Route::post('/callback/auth/{token}', 'Auth\LoginController@handleCheckToken');

Route::group(['prefix' => 'app/rest'], function() {
  Route::get('status', 'AppController@getUserStatus');

  Route::group(['middleware' => 'auth'], function() {
    Route::get('topics/my', 'TopicController@own');
    Route::get('topics/certificate/my', 'TopicController@ownCertificates');
    Route::get('topics/{slug}/overview', 'TopicController@single');
    Route::post('content/view', 'TopicController@viewContent');
    Route::patch('topics/enable-trial', 'TopicController@enableTrial');
    Route::get('users', 'UserController@index');
    Route::get('users/{email}', 'UserController@single');
    Route::patch('users/{email}', 'UserController@patch');
    Route::patch('users/{email}/{action}', 'UserController@patchAction');
    Route::get('profile', 'ProfileController@index');
    Route::patch('profile', 'ProfileController@patch');
    Route::patch('password', 'Auth\PasswordController@patch');

    Route::get('jwplayer-script', 'AppController@getJWPlayerScript');

    Route::group(['prefix' => 'admin', 'middleware' => 'auth.admin'], function() {
      Route::resource('backend-users', 'Backend\UserController', ['only' => ['index', 'show', 'update', 'store']]);
      Route::resource('plans', 'Backend\PlanController', ['only' => ['index']]);
      Route::resource('roles', 'Backend\RoleController', ['only' => ['index', 'show']]);
      Route::resource('topics', 'Backend\TopicController', ['only' => ['index']]);
      Route::resource('apps', 'Backend\AppController', ['only' => ['index', 'show', 'update', 'store']]);
      Route::get('apps/{id}/users', 'Backend\AppController@users');
      Route::post('apps/assignuser', 'Backend\AppController@assignuser');
      Route::delete('apps/removeuser', 'Backend\AppController@removeUser');
      Route::post('certificate/redeem', 'API\TopicController@redeemCertificate');
    });
  });
});

Route::group(['middleware' => 'auth'], function() {
  // Route::name('app')->get('/app{route}', 'AppController@index')->where('route', '.*');

  Route::name('profile')->get('profile', 'ProfileController@index');
  Route::name('profile.edit')->get('profile/edit', 'ProfileController@edit');
  Route::name('profile.edit-password')->get('profile/password', 'Auth\PasswordController@edit');
  Route::name('profile.patch')->patch('profile', 'ProfileController@patch');
  Route::name('profile.patch-profession')->post('profile', 'ProfileController@patchProfession');
  Route::name('profile.password')->patch('password', 'Auth\PasswordController@patch');
  Route::name('p2kb.learn')->get('/p2kb/{slug}/learn', 'P2KBController@learn');
  Route::name('journal.read')->get('/journals/read/{slug}', 'JournalController@getTicketedURL');
});

Route::name('p2kb')->get('/p2kb', 'P2KBController@index')->middleware('pageview');
Route::name('p2kb.single')->get('/p2kb/{slug}', 'P2KBController@show')->middleware('pageview');
Route::name('service')->get('/services/{service}', 'HomeController@service')->middleware('pageview');

// Route::name('topic.singleP3KGB')->get('/p3kgb/{slug}', 'TopicController@single');
Route::name('topic.certificate')->get('topics/certificate/{topic}', 'TopicController@certificate');
Route::name('topic.file')->get('topics/files/{filename}', 'TopicController@file');

Route::name('articles')->get('/articles', 'ArticleController@index')->middleware('pageview');
Route::name('article.single')->get('/articles/{slug}', 'ArticleController@single')->middleware('pageview');
Route::name('article.category')->get('/articles/category/{slug}', 'ArticleController@category')->middleware('pageview');

Route::name('journals')->get('/journals', 'JournalController@index')->middleware('pageview');
Route::name('journal.single')->get('/journals/{slug}', 'JournalController@single')->middleware('pageview');
// Route::name('journal.read')->get('/journals/read/{slug}', 'JournalController@read');
Route::name('drugs')->get('/drugs', 'DrugController@index')->middleware('pageview');
Route::name('drugs.single')->get('/drugs/{slug}', 'DrugController@single')->middleware('pageview');

Route::group(['middleware' => 'auth'], function() {
  // Route::name('app')->get('/app{route}', 'AppController@index')->where('route', '.*');
  Route::name('profile')->get('profile', 'ProfileController@index')->middleware('pageview');
  Route::name('profile.edit')->get('profile/edit', 'ProfileController@edit')->middleware('pageview');
  Route::name('profile.edit-password')->get('profile/password', 'Auth\PasswordController@edit')->middleware('pageview');
  Route::name('profile.patch')->patch('profile', 'ProfileController@patch');
  Route::name('profile.patch-profession')->post('profile', 'ProfileController@patchProfession');
  Route::name('profile.password')->patch('password', 'Auth\PasswordController@patch');
});

// Route::name('subscribe')->get('/subscribe', 'SubscribeController@index');
// Route::name('subscribe.redeem')->post('/subscribe/redeem', 'SubscribeController@redeem');
Route::name('pay.redeem')->post('/pay/redeem', 'SubscribeController@redeemCertificate')->middleware('auth');
Route::name('pay.done')->post('/pay/done', 'TransactionController@done');
Route::name('mt.snaptoken')->post('/pay/midtrans/token', 'Midtrans\SnapController@token');
Route::name('mt.notification')->post('/callback/midtrans', 'TransactionController@handleMidtransCallback');
Route::name('bni.billing')->post('/pay/bni/va', 'BNI\ECollectionController@createBilling');
Route::name('bni.notification')->post('/callback/bni', 'TransactionController@handleBNICallback');

Route::name('backend')->get('/backend{route}', 'BackendController@index')->where('route', '.*')->middleware(['auth.admin']);

Route::get('login-dev', function ()
{
	if (env('APP_ENV') == 'local') {
		auth()->loginUsingId(Request::input('id') ?: 3);
		return redirect(route('home'));
	}
	return abort(404);
});

Route::name('pit-pogi')->get('/pit-pogi', 'PitPogiController@index')->middleware('pageview');
Route::name('pit-pogi.video')->get('/pit-pogi/watch/{slug}', 'PitPogiController@video')->middleware('pageview');
Route::name('pit-pogi.file')->get('/pit-pogi/download/{slug}', 'PitPogiController@file')->middleware('pageview');

//new route
Route::name('events')->get('events', 'EventController@index')->middleware('pageview');
Route::get('event/{slug}', 'EventController@single')->middleware('pageview');

Route::name('jobs')->get('jobs', 'JobController@index')->middleware('pageview');
Route::get('job/{slug}', 'JobController@single')->middleware('pageview');

Route::name('marine')->get('/marine', 'MarineController@index')->middleware('pageview');
Route::name('marine.video')->get('/marine/watch/{slug}', 'MarineController@video')->middleware('pageview');

/**
 * The following are routes for the deprecated version
 * of the site. Please remove once the Google Search
 * realizes they are all gone with the wind...
 */
Route::get('artikel-terbaru', function ()
{
  return redirect(route('articles'), 301);
});
// Route::get('berlangganan', function ()
// {
//   return redirect(route('subscribe'), 301);
// });
Route::get('jurnal', function ()
{
  return redirect(route('service', ['service' => 'journals']), 301);
});
Route::get('jurnal/{journal}', function ($journal)
{
  return redirect(route('service', ['service' => 'journals']), 301);
});
Route::get('obat', function ()
{
  return redirect(route('service', ['service' => 'drugs']), 301);
});
Route::get('obat/{drug}', function ($drug)
{
  return redirect(route('service', ['service' => 'drugs']), 301);
});
Route::get('topik', function ()
{
  return redirect(route('p2kb'), 301);
});
Route::name('topic.singleP2KB')->get('topik/judul/{topic}', function ()
{
  return redirect(route('p2kb'), 301);
});
Route::get('{article}', function ($article)
{
  $article = \App\WP\Post::articles()->published()->where('post_name', $article)->firstOrFail();
  return redirect(route('article.single', ['slug' => $article->post_name]), 301);
});




