<nav class="navbar navbar-light navbar-fixed fixed-top bg-white shadowed navbar-expand-sm py-md-3 py-1">
	<div class="container d-flex justify-content-between mx-md-auto mx-0">
		<div class="d-flex align-items-md-center align-items-start">
			<div class="d-md-block d-flex align-items-center">
				<button class="navbar-toggler border-0 mr-2 pl-0" type="button" data-toggle="collapse" data-target="#navbar-collapsible-menu" style="outline: none;">
			    <b class="fa fa-bars fa-2x"></b>
			  </button>
				<a class="navbar-brand" href="<?php echo e(route('home')); ?>">
					<img src="<?php echo e(asset('images/GakkenIndonesia-Original.svg')); ?>" height="40" class="d-none d-md-block" />
					<img src="<?php echo e(asset('images/G-Original.svg')); ?>" height="40" class="d-md-none" />
				</a>
			</div>
		</div>

		<div class="d-flex align-items-center py-md-0 py-1">
			<div class="d-none d-md-block mr-3">
				<ul class="nav navbar-nav">
					
					<?php echo $__env->make('layouts.navbar-apps', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</ul>
			</div>
			<?php echo $__env->make('layouts.navbar-account', ['ignoreInverse' => true], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		</div>
	</div>
	<div class="collapse w-100" id="navbar-collapsible-menu">
		<ul class="nav navbar-nav text-center py-4">
			<?php echo $__env->make('layouts.navbar-nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		</ul>
	</div>
</nav>