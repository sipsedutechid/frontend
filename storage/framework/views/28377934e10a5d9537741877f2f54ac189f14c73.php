<!DOCTYPE html>
<html lang="id-ID">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name')); ?></title>

    <link rel="stylesheet" type="text/css" href="<?php echo e(mix('css/front.css')); ?>">

    <link rel="icon" href="<?php echo e(asset('images/icon.png')); ?>">
    <link rel="icon" sizes="48x48" href="<?php echo e(asset('images/icon@0,25x.png')); ?>">
    <link rel="icon" sizes="96x96" href="<?php echo e(asset('images/icon@0,5x.png')); ?>">
    <link rel="icon" sizes="144x144" href="<?php echo e(asset('images/icon@0,75x.png')); ?>">

    <link rel="apple-touch-icon" href="<?php echo e(asset('images/icon.png')); ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo e(asset('images/touch-icon-ipad.png')); ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo e(asset('images/touch-icon-iphone-retina.png')); ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo e(asset('images/touch-icon-ipad-retina.png')); ?>">

    <meta name="msapplication-square70x70logo" content="<?php echo e(asset('images/icon_smalltile.png')); ?>">
    <meta name="msapplication-square150x150logo" content="<?php echo e(asset('images/icon_mediumtile.png')); ?>">

    <meta name="theme-color" content="#a4151a">
</head>
<body>

    <div class="container" style="height: 100vh;">

        <?php echo $__env->yieldContent('content'); ?>
    </div>

</body>
</html>