<?php

namespace App\Moodle;

use Illuminate\Database\Eloquent\Model;

class GradeItem extends Model
{

	protected $connection = 'mysql_moodle';

	public function course()
	{
		return $this->belongsTo('App\Moodle\Course', 'courseid');
	}

	public function gradeGrades()
	{
		return $this->hasMany('App\Moodle\GradeGrade', 'itemid');
	}

	public function scopeQuiz($query)
	{
		return $query->where('itemmodule', 'quiz');
	}
}
