<?php

namespace App\Moodle;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	protected $connection = 'mysql_moodle';
	protected $table = 'role';
}
