<?php

namespace App\Moodle;

use Illuminate\Database\Eloquent\Model;

class GradeGrade extends Model
{

	protected $connection = 'mysql_moodle';

	public function user()
	{
		return $this->belongsTo('App\Moodle\User', 'userid');
	}

	public function gradeItem()
	{
		return $this->belongsTo('App\Moodle\GradeItem', 'itemid');
	}
}
