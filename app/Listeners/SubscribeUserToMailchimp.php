<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Newsletter;

class SubscribeUserToMailchimp
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        $isEmail = filter_var($event->user->email, FILTER_VALIDATE_EMAIL );
        if ($isEmail) {
            Newsletter::subscribeOrUpdate($event->user->email, ['FNAME' => $event->user->name]);
        }
    }
}
