<?php

namespace App\Listeners;

use App\Events\UserEnrolled;
use App\Moodle\Course;
use App\Moodle\Role;
use App\Moodle\User;
use Carbon\Carbon;
use Curl;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EnrolUserToMoodle
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserEnrolled  $event
     * @return void
     */
    public function handle(UserEnrolled $event)
    {
        if (!env('P2KB_URL', false) || env('APP_ENV', 'local') != 'production' || !filter_var($event->userTopic->user->email, FILTER_VALIDATE_EMAIL ))
            return;

        $user = $event->userTopic->user;
        $course = Course::where('idnumber', $event->userTopic->topic->p2kb_url)->first();
        $role = Role::where('shortname', 'student')->first();
        $user = User::where('email', $event->userTopic->user->email)->first();
        $userSubscription = $event->userTopic->user->userSubscriptions()->active()->first();
        if (
            !$course
            || !$user
            || !$role
            // || !$userSubscription
            )
            return;

        $apiUrl = env('P2KB_URL') . '/moodle/webservice/rest/server.php';

        $enrolment = [
            'userid' => $user->id,
            'roleid' => $role->id,
            'courseid' => $course->id,
            'timestart' => Carbon::now()->timestamp,
            'timeend' => Carbon::now()->addYear()->timestamp,
        ];

        Curl::to($apiUrl)
            ->withData([
                'wstoken' => env('P2KB_API_TOKEN', ''),
                'wsfunction' => 'enrol_manual_enrol_users',
                'moodlewsrestformat' => 'json',
                'enrolments' => [$enrolment]
                ])
            ->enableDebug(storage_path('logs/curl-EnrolUserToMoodle' . Carbon::now()->format('YmdHis') . '.log'))
            ->post();
    }
}
