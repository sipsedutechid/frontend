<?php

namespace App\WP;

use Illuminate\Database\Eloquent\Model;

class PostMeta extends Model
{

    public $connection = 'mysql_web';
    public $timestamps = false;
	public $primaryKey = 'meta_id';
	protected $table = 'postmeta';

	public function getMetaValueAttribute($value)
	{
		switch ($this->meta_key) {
			case '_wp_attachment_metadata':
				$value = unserialize($value);
				break;
		}

		return $value;
	}

}
