<?php

namespace App\WP;

use Illuminate\Database\Eloquent\Model;

class TermTaxonomy extends Model
{
	public $connection = 'mysql_web';
  public $timestamps = false;
	public $primaryKey = 'term_taxonomy_id';
	protected $table = 'term_taxonomy';

	public function term()
	{
		return $this->belongsTo('App\WP\Term', 'term_id');
	}

	public function category()
	{
		return $this->belongsTo('App\WP\Term', 'term_id')->category();
	}

	public function tag()
	{
		return $this->belongsTo('App\WP\Term', 'term_id')->tag();
	}

  public function posts()
  {
      return $this->belongsToMany('\App\WP\Post', 'term_relationships', 'term_taxonomy_id', 'object_id');
  }

	public function scopeCategories($query)
	{
		return $query->where('taxonomy', 'category');
	}
}
