<?php

namespace App\WP;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    public $connection = 'mysql_web';
    public $timestamps = false;
    public $primaryKey = 'ID';

    public function metas()
    {
        return $this->hasMany('\App\WP\PostMeta', 'post_id', 'id');
    }

    public function attachmentMetadata()
    {
        return $this->hasOne('\App\WP\PostMeta', 'post_id', 'id')
            ->where('meta_key', '_wp_attachment_metadata');
    }

    public function featuredImages()
    {
    	return $this->belongsToMany('\App\WP\Post', 'postmeta', 'post_id', 'meta_value')
    		->select(['ID as id', 'guid as url', 'post_mime_type as mime_type'])
    		->where('meta_key', '_thumbnail_id');
    }

    public function featuredImageSizeFile($size)
    {
        if ($this->featuredImages->count() == 0) {
            return false;
        }
        $size = collect($this->featuredImages[0]->attachmentMetadata->meta_value['sizes'])->get($size);
        if (!$size) 
            return false;
        return $size['file'];
    }

    public function subheading()
    {
    	return $this->hasOne('\App\WP\PostMeta', 'post_id')
    		->select(['post_id', 'meta_value as content'])
    		->where('meta_key', 'subheading');
    }

    public function author()
    {
    	return $this->belongsTo('\App\WP\User', 'post_author')
    		->select(['id', 'display_name']);
    }

    public function categoryTaxonomies()
    {
        return $this->belongsToMany('\App\WP\TermTaxonomy', 'term_relationships', 'object_id', 'term_taxonomy_id')
            ->whereHas('category');
    }

    public function tagTaxonomies()
    {
        return $this->belongsToMany('\App\WP\TermTaxonomy', 'term_relationships', 'object_id', 'term_taxonomy_id')
            ->whereHas('tag');
    }

    public function popularPostsData()
    {
        return $this->hasOne('App\WP\PopularPostsData', 'postid');
    }

    public function scopeArticles($query)
    {
        $query->where('post_type', 'post');
    }

    public function scopePublished($query)
    {
    	$query->where('post_status', 'publish')
            ->orderBy('post_date_gmt', 'desc');
    }
}
