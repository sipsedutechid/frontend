<?php

namespace App\WP;

use Illuminate\Database\Eloquent\Model;

class PostSticky extends Model
{
    public $connection = 'mysql_web';
    public $timestamps = false;
	public $primaryKey = 'option_id';
	protected $table = 'options';

	public function scopePost()
	{
        $sticky = $this->where('option_name', 'sticky_posts')->first();
        $ids = $sticky ? unserialize($sticky->option_value) : [];

        return \App\WP\Post::whereIn('ID', $ids)
            ->articles()
            ->with(['featuredImages', 'author', 'subheading','categoryTaxonomies', 'categoryTaxonomies.term'])
            ->published()
            ->get();
	}
}
