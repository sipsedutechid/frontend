<?php

namespace App\Vault;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
  protected $connection = 'mysql_vault';

  public function topicContents()
  {
    return $this->hasMany('App\Vault\TopicContent');
  }

  public function getUrlJwAttribute($value)
  {
  	if (
  		$value == null
  		|| strlen(trim($value)) == 0
  		)
  		return null;

  	$secret = env('JWPLAYER_SECRET', '');
  	$expires = round((time() + 3600) / 300) * 300;

		$path = "manifests/" . $value . ".m3u8";
		$signature = md5($path . ":" . $expires . ":" . $secret);

		return "https://content.jwplatform.com/" . $path . "?sig=" . $signature . "&exp=" . $expires;
  }

  public function scopeCatalog($query)
  {
    return $query
      ->where('type', '<>', 'link')
      ->orWhere('appearance', '<>', 'download');
  }

  public function scopeFile($query)
  {
    return $query->where('type', 'file');
  }

}
