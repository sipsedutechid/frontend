<?php

namespace App\Vault;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Journal extends Model
{

	use SoftDeletes;

	public $connection = 'mysql_vault';

	public function getDescriptionAttribute($value)
	{
		return clean($value);
	}

	public function category() {
		return $this->belongsTo('App\Vault\JournalCategory', 'journal_category_id');
	}

	public function scopePublished($query)
	{
		return $query->where('post_status', true);
	}


	public function getTicketedURL($user)
	{
		if (
			!env('ELSEVIER_ORIGIN_ID', false)
			|| !env('ELSEVIER_SALT', false))
			return null;

		$original = '_ob=TicketedURL' .
			'&_origin=' . env('ELSEVIER_ORIGIN_ID') .
			'&_originUser=' . $user->id .
			'&_target=' . urlencode($this->ext_url) .
			// '&_ts=' . \Carbon\Carbon::now('UTC')->timestamp .
			'&_ts=' .  \Carbon\Carbon::now('UTC')->format('Ymdhis') .
			'&_version=1';

		$unhashed = $original . env('ELSEVIER_SALT');

		return 'http://www.sciencedirect.com/science?' .
			$original .
			'&md5=' . md5($unhashed);
	}

}
