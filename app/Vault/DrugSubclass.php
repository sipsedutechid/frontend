<?php

namespace App\Vault;

use Illuminate\Database\Eloquent\Model;

class DrugSubclass extends Model
{
  protected $connection = 'mysql_vault';

  public function drugs()
  {
  	return $this->hasMany('App\Vault\Drug');
  }
}
