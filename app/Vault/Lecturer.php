<?php

namespace App\Vault;

use Illuminate\Database\Eloquent\Model;

class Lecturer extends Model
{

  protected $connection = 'mysql_vault';
  protected $table = 'lectures';
  protected $appends = [
  	'nameWithTitles'
  ];

  public function getNameWithTitlesAttribute()
  {
  	$name = $this->front_title ? $this->front_title . ' ' : '';
  	$name .= $this->name;
  	$name .= $this->back_title ? ', ' . $this->back_title : '';

  	return $name;
  }

}
