<?php

namespace App\Vault;

use Illuminate\Database\Eloquent\Model;

class Drug extends Model
{
  protected $connection = 'mysql_vault';

  public function drugClass()
  {
    return $this->belongsTo('App\Vault\DrugClass');
  }

  public function drugSubclass()
  {
    return $this->belongsTo('App\Vault\DrugSubclass');
  }

  public function manufacturer()
  {
  	return $this->belongsTo('App\Vault\DrugManufacturer', 'drug_manufacturer_id');
  }

  public function marketer()
  {
  	return $this->belongsTo('App\Vault\DrugManufacturer', 'drug_marketer_id');
  }

  public function generics()
  {
    return $this->hasMany('App\Vault\DrugGeneric');
  }

  public function dosages()
  {
  	return $this->hasMany('App\Vault\DrugDosage');
  }

  public function forms()
  {
    return $this->belongsToMany('App\Vault\DrugForm', 'drug_drug_forms');
  }

  public function packagings()
  {
    return $this->hasMany('App\Vault\DrugPackaging');
  }
}
