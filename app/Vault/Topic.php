<?php

namespace App\Vault;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Topic extends Model
{

  use SoftDeletes;

  protected $connection = 'mysql_vault';
  protected $hidden = [
    'created_at', 'created_by', 'deleted_at', 'id', 'is_open_access', 'is_p2kb', 'meta_desc',
    'post_status', 'preview', 'updated_at'
  ];

  public function contents()
  {
    return $this->belongsToMany('App\Vault\Content', 'topic_content')->orderBy('order');
  }

  public function lecturers()
  {
    return $this->belongsToMany('App\Vault\Lecturer', 'topic_lecture', 'topic_id', 'lecture_id');
  }

  public function topicContents()
  {
    return $this->hasMany('App\Vault\TopicContent');
  }

  public function userTopics()
  {
    return $this->hasMany('App\Accounts\UserTopic');
  }

  public function getSummaryAttribute($value)
  {
    return clean($value);
  }

  public function scopeP2kb($query)
  {
  	$query->where('category', 'doctor');
  }

  public function scopeP3kgb($query)
  {
  	$query->where('category', 'dentist');
  }

  public function scopePublished($query)
  {
  	$query->where('post_status', true)
  		->orderBy('publish_at', 'desc');
  }

  public function scopeBacknumbers($query, $category)
  {
    if (in_array($category, ['doctor', 'dentist'])) {
      $index = \App\Accounts\AppSetting::findOrFail('topic.current_index_' . $category)->value;
      $query->whereBetween('release', [$index - 13, $index - 1]);
    }
  }

  public function scopeNewRelease($query, $category)
  {
    if (in_array($category, ['doctor', 'dentist'])) {
      $query->where('release', \App\Accounts\AppSetting::findOrFail('topic.current_index_' . $category)->value);
    }
  }

  public function sponsorContent() {
    return $this->morphOne('App\Vault\SponsorContent', 'content');
  }

  public function sponsor() {
    return $this->morphedByMany('App\Vault\Sponsor', 'content', 'sponsor_content', 'content_id', 'sponsor_id');
    // 'sponsor_content', 'content_id', 'sponsor_id'
  }

  public function subscriptions() {
    return $this->hasMany('App\Vault\TopicSubscription');
  }

}
