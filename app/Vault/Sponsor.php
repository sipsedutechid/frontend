<?php

namespace App\Vault;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sponsor extends Model
{
    use SoftDeletes;
    protected $connection = 'mysql_vault';

    public function sponsorContent() {
        return $this->hasMany('App\Vault\SponsorContent');
    }
}
