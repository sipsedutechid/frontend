<?php

namespace App\Vault;

use Illuminate\Database\Eloquent\Model;

class DrugDosage extends Model
{
  protected $connection = 'mysql_vault';
  
  protected $hidden = [
  	'created_at', 'updated_at'
  ];

  public function form()
  {
  	return $this->belongsTo('App\Vault\DrugForm', 'drug_form_id');
  }
}
