<?php

namespace App\Vault;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SponsorContent extends Model
{
    use SoftDeletes;
    protected $connection = 'mysql_vault';
    protected $table = 'sponsor_content';

    public function content()
    {
        return $this->morphTo(null, 'content_type', 'content_id');
    }

    public function sponsor() {
        return $this->belongsTo('App\Vault\Sponsor');
    }
}
