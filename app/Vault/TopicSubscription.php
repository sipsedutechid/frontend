<?php

namespace App\Vault;

use Illuminate\Database\Eloquent\Model;

class TopicSubscription extends Model
{
    protected $connection = 'mysql_vault';

    public function topic()
    {
        return $this->hasOne('App\Vault\Topic', 'id', 'topic_id');
    }

    public function subscription()
    {
        return $this->hasOne('App\Accounts\Subscription', 'id', 'subscription_id');
    }
}
