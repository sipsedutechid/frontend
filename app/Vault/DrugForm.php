<?php

namespace App\Vault;

use Illuminate\Database\Eloquent\Model;

class DrugForm extends Model
{
  protected $connection = 'mysql_vault';
  
  protected $hidden = [
  	'created_at', 'updated_at'
  ];
  
}
