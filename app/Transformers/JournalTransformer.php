<?php

namespace App\Transformers;

use App\Vault\Journal;
use League\Fractal\TransformerAbstract;

class JournalTransformer extends TransformerAbstract
{

    protected $complete;

    public function __construct($complete = false)
    {
        $this->complete = $complete;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Journal $journal)
    {
        $transformer = [
            'slug' => $journal->slug,
            'title' => $journal->title,
            'issn' => $journal->issn,
            'publisher' => $journal->publisher,
            'category' => [
                'name' => $journal->category->labels,
                'slug' => $journal->category->slug,
            ],
            'imageUrl' =>
                (
                    starts_with($journal->image_url, 'http://')
                    || starts_with($journal->image_url, 'https://')
                ) ?
                $journal->image_url :
                asset('images/journals/' . $journal->image_url),
        ];

        if ($this->complete) {
            $transformer = array_merge($transformer, [
                'eIssn' => $journal->e_issn,
                'editor' => $journal->editor,
                'description' => $journal->description,
                'readUrl' => route('journal.read', ['slug' => $journal->slug]),
                ]);
        }

        return $transformer;
    }
}
