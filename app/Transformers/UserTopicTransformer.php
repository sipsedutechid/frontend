<?php

namespace App\Transformers;

use Fractal;
use Carbon\Carbon;
use App\Accounts\UserTopic;
use League\Fractal\TransformerAbstract;

class UserTopicTransformer extends TransformerAbstract
{

    protected $context;
    protected $topic;

    public function __construct($context = 'app-index', $topic = null)
    {
        $this->context = $context;
        $this->topic = $topic;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(UserTopic $userTopic)
    {
        $topic = $this->topic ?: $userTopic->topic;

        $baseContext = [
            'title' => $topic->title,
            'slug' => $topic->slug,
        ];
        switch ($this->context) {
            case 'app-index':
                $contents = collect(
                    fractal()
                        ->collection($topic->topicContents()->with('content')->get())
                        ->transformWith(new ContentsListTransformer($userTopic, $topic))
                        ->toArray()['data']
                    )
                    ->sortBy('order')
                    ->values()
                    ->map(function ($value)
                    {
                        return collect($value)->except('order')->all();
                    })
                    ->all();
                return array_merge($baseContext, [
                    'contents' => $contents,
                    'lecturer' => $topic->lecturers[0]->nameWithTitles,
                    'summary' => $topic->summary,
                    'passedAt' => $userTopic->passed_at,
                    'isTrial' => $userTopic->is_trial == 1,
                    'image' => $topic->featured_image,
                    'release' => Carbon::parse($topic->publish_at)->format('M Y'),
                    'category' => $topic->category,
                    'certified' => $userTopic->certificate_no !== null
                    ]);
                break;
            case 'show':
                $baseContext = array_merge($baseContext, [
                    'lecturer' => $topic->lecturers[0]->nameWithTitles,
                    'summary' => $topic->summary,
                    ]);
            case 'index':
                return array_merge($baseContext, [
                    'grade' => $userTopic->grade,
                    'isTrial' => $userTopic->is_trial == 1,
                    'isPassed' => $userTopic->passed_at != null,
                    'lecturer' => $topic->lecturers[0]->nameWithTitles,
                    'passedAt' => $userTopic->passed_at == null ? null : Carbon::parse($userTopic->passed_at)->format('j/m/Y'),
                    'releaseMonth' => Carbon::parse($userTopic->topic->publish_at)->format('M Y'),
                    'featuredImage' => env('VAULT_URL', '/') . '/topic/files/' . $topic->featured_image,
                    ]);
            case 'show-user':
                return [
                    'slug' => $topic->slug,
                    'grade' => $userTopic->grade,
                    'topic' => $topic->title,
                    'status' => $userTopic->passed_at != null ? 'Passed' : ($userTopic->grade > 0 ? 'Not passed' : 'Test not taken'),
                    'passedAt' => $userTopic->passed_at == null ? null : Carbon::parse($userTopic->passed_at)->diffForHumans(),
                    'certificate' => $userTopic->certificate_no,
                ];
            default: return $baseContext;
        }
    }
}
