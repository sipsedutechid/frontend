<?php

namespace App\Transformers;

use App\Vault\Drug;
use League\Fractal\TransformerAbstract;

class CompactDrugTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Drug $drug)
    {
        return [
            'slug' => $drug->slug,
            'name' => $drug->name,
            'manufacturer' => $drug->manufacturer->name,
        ];
    }
}
