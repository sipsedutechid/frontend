<?php

namespace App\Transformers;

use App\Vault\DrugClass;
use App\Transformers\DrugTransformer;
use App\Transformers\DrugSubclassTransformer;
use League\Fractal\TransformerAbstract;

class DrugClassTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(DrugClass $class)
    {
        return [
            'code' => $class->code,
            'name' => $class->name,

            'subclasses' => fractal()->collection($class->subclasses)->transformWith(new DrugSubclassTransformer)->toArray(),
            'drugs' => fractal()->collection($class->directDrugs)->transformWith(new DrugTransformer)->toArray(),
        ];
    }
}
