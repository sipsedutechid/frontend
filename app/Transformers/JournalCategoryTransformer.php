<?php

namespace App\Transformers;

use App\Vault\JournalCategory;
use League\Fractal\TransformerAbstract;

class JournalCategoryTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(JournalCategory $jc)
    {
        return [
            'slug'  => $jc->slug,
            'name'  => $jc->labels,
        ];
    }
}
