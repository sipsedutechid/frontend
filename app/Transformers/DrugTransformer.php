<?php

namespace App\Transformers;

use App\Transformers\DrugDosageTransformer;
use App\Transformers\DrugFormTransformer;
use App\Transformers\DrugGenericTransformer;
use App\Transformers\DrugPackagingTransformer;
use App\Vault\Drug;
use Fractal;
use League\Fractal\TransformerAbstract;

class DrugTransformer extends TransformerAbstract
{

    protected $complete;

    public function __construct($complete = false)
    {
        $this->complete = $complete;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Drug $drug)
    {
        $transformer = [
            'slug' => $drug->slug,
            'name' => $drug->name,
            'manufacturer' => $drug->manufacturer->name,
            'indication' => $drug->indication,
            'drugSubclass' => $drug->drugSubclass ? $drug->drugSubclass->name : null,
        ];

        if ($this->complete) {
            $transformer = array_merge($transformer, [
                'marketer' => $drug->marketer ? $drug->marketer->name : null,
                'drugClass' => $drug->drugClass->name,
                'classification' => $drug->classification,
                'classificationImage' => asset('images/drug-classification-' . $drug->classification . '.svg'),
                'contraindication' => $drug->contraindication,
                'precautions' => $drug->precautions,
                'adverseReactions' => $drug->adverse_reactions,
                'interactions' => $drug->interactions,
                'pregnancyCategory' => $drug->pregnancy_category,
                'forms' => Fractal::collection($drug->forms)->transformWith(new DrugFormTransformer)->toArray()['data'],
                'generics' => Fractal::collection($drug->generics()->with(['form', 'generic'])->orderBy('drug_form_id')->get())->transformWith(new DrugGenericTransformer)->toArray()['data'],
                'dosages' => Fractal::collection($drug->dosages()->with(['form'])->orderBy('drug_form_id')->get())->transformWith(new DrugDosageTransformer)->toArray()['data'],
                'packagings' => Fractal::collection($drug->packagings()->with(['form'])->orderBy('drug_form_id')->get())->transformWith(new DrugPackagingTransformer)->toArray()['data'],
                ]);
        }

        return $transformer;
    }
}
