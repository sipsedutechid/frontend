<?php

namespace App\Transformers;

use Carbon\Carbon;
use App\Accounts\Role;
use App\Accounts\Ability;
use League\Fractal\TransformerAbstract;

class RoleTransformer extends TransformerAbstract
{

    protected $context;

    public function __construct($context = 'index')
    {
        $this->context = $context;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Role $role)
    {
        $baseContext = [
            'id' => $role->id,
            'name' => $role->name,
            'backendCapable' => !$role->is_user && !$role->is_subscriber,
        ];

        switch ($this->context) {
            case 'index':
                return array_merge($baseContext, [
                    'createdAt' => Carbon::parse($role->created_at)->diffForHumans(),
                    'usersCount' => $role->users_count,
                ]);
            case 'single':
                $abilities = $role->abilities;
                return array_merge($baseContext, [
                    'abilities' => Ability::orderBy('name')->get()->map(function ($ability) use ($abilities)
                    {
                        return [
                            'app' => $ability->app,
                            'action' => $ability->action,
                            'module' => $ability->module,
                            'namespace' => $ability->name,
                            'isPermitted' => $abilities->where('name', $ability->name)->count() == 1
                        ];
                    })
                ]);
            case 'single-belongs-to':
            default: return $baseContext;
        }
    }
}
