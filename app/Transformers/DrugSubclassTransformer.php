<?php

namespace App\Transformers;

use App\Vault\DrugSubclass;
use App\Transformers\CompactDrugTransformer;
use League\Fractal\TransformerAbstract;

class DrugSubclassTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(DrugSubclass $subclass)
    {
        return [
            'code' => $subclass->code,
            'name' => $subclass->name,

            'drugs' => fractal()->collection($subclass->drugs)->transformWith(new CompactDrugTransformer(false, true))->toArray(),
        ];
    }
}
