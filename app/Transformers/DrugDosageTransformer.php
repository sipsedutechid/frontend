<?php

namespace App\Transformers;

use App\Vault\DrugDosage;
use League\Fractal\TransformerAbstract;

class DrugDosageTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(DrugDosage $drugDosage)
    {
        return [
            'slug' => str_random(6),
            'form' => $drugDosage->drug_form_id ? [
                'slug' => $drugDosage->form->slug,
                'name' => $drugDosage->form->name,
            ] : null,
            'name' => $drugDosage->name,
            'description' => $drugDosage->description,
            'administration' => $drugDosage->administration
        ];
    }
}
