<?php

namespace App\Transformers;

use App\Vault\Content;
use League\Fractal\TransformerAbstract;

class CatalogContentTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Content $content)
    {
        return [
            'slug'  => $content->slug,
            'title' => $content->label
        ];
    }
}
