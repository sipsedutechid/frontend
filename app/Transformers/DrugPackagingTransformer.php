<?php

namespace App\Transformers;

use App\Vault\DrugPackaging;
use League\Fractal\TransformerAbstract;

class DrugPackagingTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(DrugPackaging $drugPackaging)
    {
        return [
            'slug' => str_random(6),
            'form' => [
                'slug' => $drugPackaging->form->slug,
                'name' => $drugPackaging->form->name,
            ],
            'name' => $drugPackaging->name,
            'price' => "IDR " . number_format($drugPackaging->price, 0, '.', ','),
            'quantity' => $drugPackaging->quantity,
            'quantity2' => $drugPackaging->quantity_2,
            'quantity3' => $drugPackaging->quantity_3,
        ];
    }
}
