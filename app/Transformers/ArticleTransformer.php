<?php

namespace App\Transformers;

use App\WP\Post;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class ArticleTransformer extends TransformerAbstract
{

    protected $context;

    public function __construct($context = 'index')
    {
        $this->context = $context;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Post $post)
    {
        $featuredImageBaseDir = isset($post->featuredImages[0]) ? implode('/', array_slice(explode('/', $post->featuredImages[0]->url), 0, -1)) . '/' : null;
        $baseContext = [
            'title' => $post->post_title,
            'author' => $post->author->display_name,
            'featuredImage' => [
                'thumbnail' => isset($post->featuredImages[0]->attachmentMetadata['meta_value']['sizes']['thumbnail']) ?
                    $featuredImageBaseDir . $post->featuredImages[0]->attachmentMetadata['meta_value']['sizes']['thumbnail']['file'] : null,
                'medium' => isset($post->featuredImages[0]->attachmentMetadata['meta_value']['sizes']['medium']) ? 
                    $featuredImageBaseDir . $post->featuredImages[0]->attachmentMetadata['meta_value']['sizes']['medium']['file'] : null,
                'medium_large' => isset($post->featuredImages[0]->attachmentMetadata['meta_value']['sizes']['medium_large']) ? 
                    $featuredImageBaseDir . $post->featuredImages[0]->attachmentMetadata['meta_value']['sizes']['medium_large']['file'] : null,
                'large' =>  isset($post->featuredImages[0]->attachmentMetadata['meta_value']['sizes']['large']) ? 
                    $featuredImageBaseDir . $post->featuredImages[0]->attachmentMetadata['meta_value']['sizes']['large']['file'] : null,
                ],
            'publishedAt' => Carbon::parse($post->post_date_gmt)->diffForHumans()
        ];

        switch ($this->context) {
            case 'index':
                return array_merge($baseContext, [
                    'slug' => $post->post_name,
                    'subheading' => strip_tags($post->subheading->content),
                    ]);
                break;
            
            case 'show':
                return array_merge($baseContext, [
                    'content'           => str_replace(PHP_EOL, '', clean($post->post_content)),
                    'relatedArticles'   => fractal()
                        ->collection($post->categoryTaxonomies()->first()->posts()->where('id', '<>', $post->id)->take(5)->get())
                        ->transformWith(new ArticleTransformer)
                        ->toArray()
                    ]);
                break;

            default:
                return $baseContext;
        }
    }
}
