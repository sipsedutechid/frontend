<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Vault\Topic;
use App\Accounts\User;
use App\Accounts\UserTopic;
use App\Accounts\AppSetting;
use App\Accounts\Subscription;
use Illuminate\Console\Command;
use App\Accounts\UserSubscription;

class SubscribeUserToPlan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:subscribe {email : The user\'s email address} {plan : The slug identifier of the plan to subscribe}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribe a user to an existing plan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::where('email', $this->argument('email'))->first();
        if (!$user) return $this->error('Invalid user');
        $plan = Subscription::where('name', $this->argument('plan'))->first();
        if (!$plan) return $this->error('Plan does not exist');

        $userSubscription = new UserSubscription;
        $userSubscription->user_id = $user->id;
        $userSubscription->subscription_id = $plan->id;
        $userSubscription->starts_at = Carbon::now();

        $expireMethod = 'add' . ucfirst($plan->period);
        $userSubscription->expired_at = Carbon::now()->$expireMethod($plan->duration);
        $userSubscription->save();

        $currentTopicIndex = AppSetting::find('topic.current_index_' . ($user->profession ?: 'doctor'))->value;
        $subscribeTopic = new UserTopic;
        $subscribeTopic->topic_id = Topic::where('category', $user->profession ?: 'doctor')->where('release', $currentTopicIndex)->first()->id;
        $subscribeTopic->is_accessible = true;
        if ($subscribeTopic) {
            $user->userTopics()->save($subscribeTopic);
        }
        $this->info('User subscribed until ' . Carbon::parse($userSubscription->expired_at)->diffForHumans());
    }
}
