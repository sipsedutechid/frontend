<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GenerateBcryptString extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bcrypt {string : The string to be hashed}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate hashed bcrypt string';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Hash: ' . bcrypt($this->argument('string')));
    }
}
