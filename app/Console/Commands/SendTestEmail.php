<?php

namespace App\Console\Commands;

use Mail;
use App\Accounts\User;
use Illuminate\Console\Command;

class SendTestEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:test {class} {--to=endy0611@gmail.com}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a test email to a recipient';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!in_array($this->argument('class'), [
            'SubscriptionActive', 
            'SubscriptionEnding',
            'SubscriptionOrdered',
            'TopicPassed', 
            'UserPasswordChanged', 
            'UserRegistered', 
            'UserVerified',
            ])) {
            $this->error('Mailable class is not recognized.');
            return;
        }

        $class = '\\App\\Mail\\' . $this->argument('class');
        switch ($this->argument('class')) {
            case 'SubscriptionEnding':
            case 'SubscriptionActive':
                $userSubscription = \App\Accounts\UserSubscription::findOrFail(1);
                Mail::queue(new $class($userSubscription));
                break;
            case 'SubscriptionOrdered':
                $subscriptionOrder = \App\Accounts\SubscriptionOrder::findOrFail(5);
                Mail::queue(new $class($user));
                break;
            case 'TopicPassed':
                $userTopic = \App\Accounts\UserTopic::findOrFail(792);
                Mail::queue(new $class($userTopic));
                break;
            case 'UserPasswordChanged':
            case 'UserRegistered':
            case 'UserVerified':
                $user = User::findOrFail(2);
                Mail::to($this->option('to'))->queue(new $class($user));
                break;
        }
        $this->info('Mail sent to ' . $this->option('to'));
    }
}
