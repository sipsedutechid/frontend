<?php

namespace App\Console\Commands;

use App\Events\UserEnrolled;
use Illuminate\Console\Command;

class EnrolUsersToMoodleCourse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:enrol-monthly {--E|email=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enrol subscribing users to newly released Moodle topics';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currentTopics = [
            'doctor' => \App\Vault\Topic::newRelease('doctor')->p2kb()->first(),
            'dentist' => \App\Vault\Topic::newRelease('dentist')->p3kgb()->first(),
        ];

        $users = $this->option('email') ?
            \App\Accounts\User::whereNotNull('profession')->where('email', $this->option('email'))->get() :
            \App\Accounts\User::whereNotNull('profession')->get();

        $total = 0;
        foreach ($users as $user) {
            $topic = $currentTopics[$user->profession];
            if (!$topic)
                continue;
            $activeSubscription = $user->userSubscriptions()->active()->first();
            if (!$activeSubscription)
                continue;
            if (\Carbon\Carbon::parse($activeSubscription->expired_at)->format('Ym') == \Carbon\Carbon::now()->format('Ym'))
                continue;
            if (\App\Accounts\UserTopic::where('user_id', $user->id)->where('topic_id', $topic->id)->count())
                continue;

            $userTopic = new \App\Accounts\UserTopic;
            $userTopic->user_id = $user->id;
            $userTopic->topic_id = $topic->id;
            $userTopic->is_accessible = true;
            $userTopic->is_trial = $activeSubscription->is_trial;
            $userTopic->save();

            event(new UserEnrolled($userTopic));
            $this->line('Enrolled user ' . $user->email . " for '" . $topic->title . "' (" . $topic->category . ')');
            $total++;
        }

        if ($total > 0)
            return $this->info('Enrolled ' . $total . ' users');
        return $this->info('No users enrolled');
    }
}
