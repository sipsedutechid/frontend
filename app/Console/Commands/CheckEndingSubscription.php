<?php

namespace App\Console\Commands;

use Mail;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Mail\SubscriptionEnding;
use App\Accounts\UserSubscription;

class CheckEndingSubscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:check-ending-sub {--N|notify : Send notification email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks and evaluates all active subscriptions whether they are ending within a week';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $activeSubscriptions = UserSubscription::active()->get();
        foreach ($activeSubscriptions as $sub) {
            if (!Carbon::parse($sub->expired_at)->subWeek()->isToday())
                continue;

            $this->info('Subscription expiring for ' . $sub->user->name);
            // Notify by email
            if ($this->option('notify')) {
                Mail::queue(new SubscriptionEnding($sub));
                $this->line('Notified ' . $sub->user->name . ' on ' . $sub->user->email);
            }
        }
    }
}
