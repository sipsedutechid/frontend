<?php

namespace App\Console\Commands;

use App\Accounts\User;
use App\Accounts\Role;
use App\Events\UserRegistered;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create 
        {email : Email address of the user} 
        {--N|name= : The user\'s full name} 
        {--P|password= : The assigned password for the user (leave blank to generate random password)} 
        {--D|pro= : The user\'s profession (doctor or dentist)} 
        {--I|id= : The user\'s professional ID number} 
        {--R|verify : Whether to verify the account}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new user account manually';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!$this->option('name'))
            return $this->error('Please supply user name with --name option');
        if (!in_array($this->option('pro'), ['doctor', 'dentist']))
            return $this->error('Invalid profession type');
        $this->line('Creating new user');

        if (User::where('email', $this->argument('email'))->count() != 0)
            return $this->error('The specified email already exists. Aborting.');

        $user = new User;
        $user->email = $this->argument('email');
        $user->name = $this->option('name');
        $password = $this->option('password') ?: str_random(8);
        $this->line('Generated password: ' . $password);

        if ($this->option('pro') && $this->option('id')) {
            $user->profession = $this->option('pro');
            $user->id_no = $this->option('id');
        }

        $user->password = bcrypt($password);
        if ($this->option('verify')) {
            $user->verified_at = Carbon::now();
            $user->verify_token = '';
        }
        else $user->verify_token = str_random(64);
        $user->role_id = Role::where('is_user', true)->first()->id;

        $user->save();
        event(new UserRegistered($user));

        $this->info('User created successfully');
    }
}
