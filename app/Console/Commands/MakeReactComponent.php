<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;

class MakeReactComponent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:component 
        {name : Fully qualified component name} 
        {--T|type=module : Whether the component is a module or reusable component (module|component)} 
        {--F|function : Whether the component is a functional component} 
        {--A|axios : Whether the component uses Axios for AJAX}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new React component';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!in_array($this->option('type'), ['component', 'module']))
            $this->error('Invalid component type: ' . $this->option('type'));

        $template = 'react-component';
        if ($this->option('function'))
            $template .= '-func';
        elseif ($this->option('axios'))
            $template .= '-axios';

        $componentName = explode('/', $this->argument('name'));
        $componentName = $componentName[count($componentName) - 1];

        $file = Storage::disk('local-res')->get('/templates/' . $template);
        $file = str_replace('[COMPNAME]', $componentName, $file);
        Storage::disk('local-res')->put('/assets/js/' . $this->option('type') . '/' . $this->argument('name') . '.jsx', $file);
        $this->info('React component created successfully!');
    }
}
