<?php

namespace App\Mail;

use App\Accounts\UserTopic;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TopicPassed extends Mailable
{
    use Queueable, SerializesModels;

    public $userTopic;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(UserTopic $userTopic)
    {
        $this->userTopic = $userTopic;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from('support@gakken-idn.co.id', 'Gakken Support')
            ->to($this->userTopic->user->email)
            ->subject('[Gakken] Selamat, Anda telah melulusi topik GakkenP2KB!')
            ->markdown('emails.topic.passed');
    }
}
