<?php

namespace App\Mail;

use App\Accounts\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRegistered extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->to($this->user->email)
            ->from('support@gakken-idn.co.id', 'Gakken Indonesia')
            ->subject('Selamat Bergabung di Gakken Indonesia')
            ->markdown('emails.user.registered');
    }
}
