<?php

namespace App\Accounts;

use Carbon\Carbon;
use Laravel\Passport\HasApiTokens;
use App\Notifications\PasswordReset;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $connection = 'mysql_accounts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'verify_token', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function groups()
    {
        return $this->belongsToMany('App\Accounts\UserGroup', 'user_group_members');
    }

    public function role()
    {
        return $this->belongsTo('App\Accounts\Role');
    }

    public function userSubscriptions()
    {
        return $this->hasMany('App\Accounts\UserSubscription');
    }

    public function userTopics()
    {
        return $this->hasMany('App\Accounts\UserTopic');
    }

    public function topics()
    {
        return $this->belongsToMany('App\Vault\Topic',  config('database.connections.mysql_accounts.database') . '.user_topics');
    }

    public function isAdmin()
    {
        return !$this->role->is_user && !$this->role->is_subscriber;
    }

    public static function getFirstAndLastNames($name)
    {
        $parts = explode(" ", $name);
        $lastname = array_pop($parts);
        $firstname = implode(" ", $parts);

        return [
            'firstName' => $firstname,
            'lastName' => $lastname,
        ];
    }

    public function verify()
    {
        $this->verified_at = \Carbon\Carbon::now();
        $this->verify_token = '';
        return $this->save();
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordReset($this, $token));
    }

    public function isTrialEnableEligible()
    {
        $activeSubscription = $this->userSubscriptions()->active()->first();
        if (!$activeSubscription)
            return false;
        if (!$activeSubscription->is_trial)
            return false;
//        if (!Carbon::now()->between(
//            Carbon::parse($activeSubscription->expired_at)->startOfMonth(), 
//            Carbon::parse($activeSubscription->expired_at)
//        ))
//            return false;

        if (
            $this->userTopics()->where('is_trial', true)->where('is_accessible', true)->count() !==
            $this->userTopics()->where('is_trial', true)->where('is_accessible', true)
                ->whereDate('passed_at', '>=', Carbon::parse($activeSubscription->starts_at))
                ->whereDate('passed_at', '<=', Carbon::parse($activeSubscription->expired_at))
                ->count()
            )
            return false;

        return true;
    }

    public function apps()
    {
        return $this->morphToMany('App\Accounts\App', 'model', 'model_has_apps', 'model_id', 'app_id');
    }
}
