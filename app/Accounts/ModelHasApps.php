<?php

namespace App\Accounts;

use Illuminate\Database\Eloquent\Model;

class ModelHasApps extends Model
{
    public $timestamps = false;
}
