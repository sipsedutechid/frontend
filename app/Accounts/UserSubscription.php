<?php

namespace App\Accounts;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserSubscription extends Model
{
    public $connection = 'mysql_accounts';

    public function subscription()
    {
    	return $this->belongsTo('App\Accounts\Subscription');
    }

    public function user()
    {
        return $this->belongsTo('App\Accounts\User');
    }

    public function scopeActive($query)
    {
    	return $query->where('starts_at', '<', Carbon::now())
    		->where('expired_at', '>', Carbon::now());
    }

    public function unsubscribe()
    {
        $this->expired_at = Carbon::now()->subSeconds(1);
        return $this->save();
    }
    
}
