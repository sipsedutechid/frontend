<?php

namespace App\Accounts;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
  
  protected $connection = 'mysql_accounts';

  public function abilities()
  {
  	return $this->belongsToMany('App\Accounts\Ability', 'role_abilities');
  }

  public function users()
  {
  	return $this->hasMany('App\Accounts\User');
  }
  
}
