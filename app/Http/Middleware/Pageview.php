<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Pageview
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $pageview = new \App\Accounts\PageView;

            $pageview->url = url()->full();
            $pageview->user_id = auth()->user()->id;

            $urlParse = explode("/", str_replace( url('/'), '', url()->full() ) ); 
            $section = isset($urlParse[1]) && !empty($urlParse[1]) ? $urlParse[1] : 'home' ;

            $allowSection = [
                'home', 
                'p2kb', 
                'articles', 
                'journals', 
                'events', 
                'drugs', 
                'jobs', 
                'job' , 
                'events', 
                'event', 
                'services', 
                'profile', 
                'paramountbed' ,
                'pit-pogi' ,
                'marine'
            ];

            if (
                in_array($section, $allowSection)
            ) {
                $pageview->category = $section;
                $pageview->save();
            }
        }

        return $next($request);
    }
}
