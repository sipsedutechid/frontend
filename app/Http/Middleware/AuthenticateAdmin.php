<?php

namespace App\Http\Middleware;

use Closure;

class AuthenticateAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->check())
            return abort(404);

        if (
            $request->user()->role->is_user
            || $request->user()->role->is_subscriber
            )
            return abort(404);

        return $next($request);
    }
}
