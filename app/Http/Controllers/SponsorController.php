<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SponsorController extends Controller
{

    public function index(Request $request)
    {
        return [
            'data' => \App\Vault\Sponsor::with(['sponsorContent.content'])->first() 
        ];
    }

    public function view(Request $request)
    {
        return view('new.ads.single');
    }

    public function prodia(Request $request)
    {
        $subscription = \App\Accounts\Subscription::where('name', 'premium')->firstOrFail();
        $premiumTopic = $subscription->topics()->with('topic')->take(6)->get();
        $freeTopic = \App\Vault\Topic::whereHas('sponsor', function($query) {
            $query->where('slug', 'prodia');
        })->doesntHave('subscriptions')->take(6)->get();

        return view('new.ads.prodia-single', [
            'subscription' => $subscription ,
            'premiumTopic' => $premiumTopic ,
            'freeTopic' => $freeTopic
        ]);
    }
}
