<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Image;
use Fractal;
use App\Accounts\User;
use App\Mail\UserVerified;
use Illuminate\Http\Request;
use App\Transformers\UserTransformer;

use App\Events\UserRegistered;

class ProfileController extends Controller
{

  public function index(Request $request)
  {
    $user = $request->user();
    if ($request->wantsJson())
      return $this->makeJsonResponse($user);

    return view('profile.home', [
      'user' => $user, 
      'inverseNavbar' => true,
      'title' => page_title('Profil Saya')
      ]);
  }

  public function edit(Request $request)
  {
  	$user = $request->user();
    return view('profile.edit', [
      'user' => $user, 
      'inverseNavbar' => true,
      'title' => page_title('Edit Profil')]);
  }

  public function patch(Request $request)
  {
    $this->validate($request, [
      'name' => 'required',
      'profession' => 'in:doctor,dentist',
      'idNo' => 'required_with:profession',
      'avatar' => 'image|max:2048'
      ]);
    $user = $request->user();
    $currEmail = $user->email;

    $user->name = $request->name;
    if ($request->profession) {
      $user->profession = $request->profession;
      $user->id_no = $request->idNo;
    }

    if ($request->email && !filter_var($currEmail, FILTER_VALIDATE_EMAIL )) {
      $user->email = $request->email;
    }

    if ($avatar = $request->file('avatar')) {
      if ($avatar->isValid()) {
        $filename = str_random(32) . '.' . $avatar->extension();
        Image::make($avatar)
          ->fit(500)
          ->save(public_path('avatars/' . $filename));
        $user->avatar_url = env('APP_URL') . '/avatars/' . $filename;
      }
    }
    $user->save();
    
    if (filter_var($request->email, FILTER_VALIDATE_EMAIL ) && !filter_var($currEmail, FILTER_VALIDATE_EMAIL )) {
      event(new UserRegistered($user));
    }

    if ($request->wantsJson())
      return $this->makeJsonResponse($user);

    $request->session()->flash('alert-success', 'Profil Anda berhasil diupdate.');
    return redirect(route('profile'));
  }

  public function patchProfession(Request $request)
  {
    $this->validate($request, [
      'profession' => 'required|in:doctor,dentist',
      'idNo' => 'required_with:profession',
      ]);
    $user = $request->user();
    $user->profession = $request->profession;
    $user->id_no = $request->idNo;
    $user->save();

    return response()->json(['status' => 'success']);
  }

  private function makeJsonResponse($user)
  {
    return response()->json(Fractal::item($user)->transformWith(new UserTransformer)->toArray()['data']);
  }

  public function verify(\Illuminate\Http\Request $request)
  {
      if (!$request->token)
          return abort(404);

      $user = User::where('verify_token', $request->token)->firstOrFail();
      $user->verify();
      Auth::login($user);
      Mail::queue(new UserVerified($user));

      return redirect(route('home'));
  }

}
