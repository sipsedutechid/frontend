<?php

namespace App\Http\Controllers\BNI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\BNI\ECollection;

class ECollectionController extends Controller
{

	public function __construct()
	{
        ECollection::$clientKey = env('BNI_ECOLLECTION_CLIENT');
        ECollection::$secretKey = env('BNI_ECOLLECTION_SECRET');
        ECollection::$isProduction = env('BNI_ECOLLECTION_PRODUCTION', false);
	}

    public function createBilling(Request $request)
    {
    	$this->validate($request, [
            'subscription' => 'required',
            'method' => 'required|in:banktransfer',
            'phone' => 'required|string'
            ]);

    	$subscription = \App\Accounts\Subscription::where('name', $request->subscription)->firstOrFail();
    	$order = new \App\Accounts\SubscriptionOrder;
    	$expiration = \Carbon\Carbon::now()->addDays(2);

    	$order->user_id = $request->user()->id;
    	$order->subscription_id = $subscription->id;
    	$order->order_no = \App\Accounts\SubscriptionOrder::generateOrderNo();
    	$order->quantity = 1;
    	$order->total = $subscription->price * $order->quantity;
    	$order->method = $request->method;
    	$order->client_ip = $request->ip();
    	$order->expired_at = $expiration;

    	$order->save();

    	$billingData = [
    		'type' => 'createbilling',
    		'client_id' => ECollection::$clientKey,
    		'trx_id' => $order->order_no,
    		'trx_amount' => $order->total,
    		'billing_type' => 'c',
    		'datetime_expired' => $expiration->toIso8601String(),
    		'customer_name' => $request->user()->name,
    		'customer_email' => $request->user()->email,
    		'customer_phone' => $request->phone,
    	];

    	$postData = ECollection::hashData($billingData);

    	$data = [
    		'client_id' => ECollection::$clientKey,
    		'data' => $postData,
    	];

    	$response = ECollection::remoteCall(json_encode($data));
    	$response = json_decode($response, true);

    	if ($response['status'] !== '000') {
    		$order->delete();
    		return response()->json(['status' => 'error', 'data' => '']);
    	}
    	else {
    		$responseData = ECollection::parseData($response['data']);
    		$order->va_no = $responseData['virtual_account'];
    		$order->save();
    		return response()->json([
    			'status' => 'success',
    			'result' => [
    				'va_no' => $order->va_no,
    				'total_amount' => $order->total
				]
    		]);
    	}
    }
}
