<?php

namespace App\Http\Controllers\API;

use Fractal;
use App\Vault\Topic;
use App\Accounts\User;
use App\Accounts\UserTopic;
use App\Events\UserEnrolled;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\TopicTransformer;
use App\Transformers\UserTopicTransformer;
use App\Transformers\ContentsListTransformer;

class TopicController extends Controller
{

	public function index(Request $request)
	{
		$userTopics = $request->user()->userSubscriptions()->active()->count() ?
			$request->user()->userTopics : collect([]);

		return response()->json(['data' =>
			collect(
				fractal()
					->collection($userTopics)
					->transformWith(new UserTopicTransformer('index'))
					->toArray()['data']
				)
				->reverse()
				->sortBy('isPassed')
				->values()
				->all()
			]);
	}

	public function show(Request $request, $slug)
	{
		$topic = Topic::where('slug', $slug)->firstOrFail();
		$userTopic = $request->user()->userTopics()->where('topic_id', $topic->id)->first();
		if (!$userTopic) {
			$userTopic = new UserTopic;
			$userTopic->user_id = $request->user()->id;
			$userTopic->topic_id = $topic->id;
			$userTopic->is_accessible = true;
			$userTopic->save();

		}
		event(new UserEnrolled($userTopic));

		$activeSub = $request->user()->userSubscriptions()->active()->first();

		return response()->json(
			[	'data' =>
				[
					'topic' => Fractal::item($userTopic)->transformWith(new UserTopicTransformer('show'))->toArray(),
					'content' => Fractal::collection($topic->topicContents()->with('content')->get()->sortBy('content.order')->values())->transformWith(new ContentsListTransformer($userTopic, null, 'mobile'))->toArray(),
					'passed' => $userTopic->passed_at !== null,
					'certified' => $userTopic->certificate_no !== null,
					'remainingQuota' => $activeSub ? $activeSub->remaining_quota : 0,
				]
			]);
	}

	public function redeemCertificate(Request $request)
	{
		if ($request->email) {
			$user = User::where('email', $request->email)->first();
			$activeSub = $user->userSubscriptions()->active()->first();
		} else {
			$activeSub = $request->user()->userSubscriptions()->active()->first();
		}
		
		$topic = \App\Vault\Topic::where('slug', $request->topic)->firstOrFail();
		$userTopic = $topic->userTopics()->where('user_id', $request->email ? $user->id : $request->user()->id)->firstOrFail();
		
		if (!$topic->is_free_certificate && !$activeSub)
			return abort(403);
		
		if (!$topic->is_free_certificate && $activeSub->remaining_quota < 1)
			return abort(403);

		$userTopic->certificate_no = $userTopic->generateCertificateNumber();
		$userTopic->save();

		if (!$topic->is_free_certificate) {
			$activeSub->remaining_quota = $activeSub->remaining_quota - 1;
			$activeSub->save();
		}

		if ($request->email) return ["isSuccess" => true, "certficateNo" => $userTopic->certificate_no];
		else return response('ok');
	}

}
