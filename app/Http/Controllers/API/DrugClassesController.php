<?php

namespace App\Http\Controllers\API;

use App\Vault\DrugClass;
use App\Transformers\DrugClassTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DrugClassesController extends Controller
{
    public function index(Request $request)
    {
        return fractal()
            ->collection(
                DrugClass::orderBy('name')
                    ->with([
                        'subclasses',
                        'subclasses.drugs',
                        'directDrugs',
                    ])
                    ->get()
                    ->filter(function ($drugClass)
                    {
                        return
                            count($drugClass->subclasses) > 0
                            || count($drugClass->directDrugs) > 0;
                    })
                    ->values()
            )
            ->transformWith(new DrugClassTransformer)
            ->respond();
    }
}
