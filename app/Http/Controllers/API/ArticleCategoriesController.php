<?php

namespace App\Http\Controllers\API;

use App\WP\TermTaxonomy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\ArticleCategoryTransformer;

class ArticleCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return fractal()
            ->collection(TermTaxonomy::categories()->with(['term', 'posts', 'posts.featuredImages'])->where('count', '>', 3)->get())
            ->transformWith(new ArticleCategoryTransformer)
            ->respond();
    }

}
