<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Vault\JournalCategory;
use App\Http\Controllers\Controller;
use App\Transformers\JournalTransformer;
use App\Transformers\JournalCategoryTransformer;

class JournalCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return fractal()
            ->collection(
                JournalCategory::orderBy('labels')
                ->whereHas('journals', function ($query)
                {
                    $query->where('post_status', true);
                })
                ->get()
            )
            ->transformWith(new JournalCategoryTransformer)
            ->respond();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $journals = JournalCategory::where('slug', $slug)->firstOrFail()->journals()->published()->with('category')->where('post_status', true);
        return fractal()
            ->collection($journals->orderBy('slug', 'asc')->get()->reverse()->values())
            ->transformWith(new JournalTransformer)
            ->respond();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
