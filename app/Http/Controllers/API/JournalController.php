<?php

namespace App\Http\Controllers\API;

use App\Vault\Journal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\JournalTransformer;

class JournalController extends Controller
{

  public function index(Request $request)
  {
    $journals = Journal::with('category')->where('post_status', true)->orderBy('title', 'asc');
    return fractal()
      ->collection(
        $request->term ? $journals->where('title', 'like', '%' . $request->term . '%')->get() : $journals->get()
      )
      ->transformWith(new JournalTransformer)
      ->respond();
  }

  public function show(Request $request, $slug)
  {
    return fractal()
      ->item(Journal::where('slug', $slug)->firstOrFail())
      ->transformWith(new JournalTransformer(true))
      ->respond();
  }

  public function getTicketedURL(Request $request, $slug)
  {
    $journal = Journal::where('slug', $slug)->firstOrFail();
    $url = $journal->getTicketedURL($request->user());
    if (!$url)
      return abort(403, 'Unauthenticated');

    return response()->json(['ticketedURL' => $url]);
  }

}
