<?php

namespace App\Http\Controllers\API;

use App\Vault\Topic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\TopicCatalogTransformer;

class TopicCatalogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $category = $user ? $user->profession : null;
        $userTopics = $user ? $user->userTopics : null;
        $topics = Topic::published()->orderBy('release', 'desc');

        if ($category)
            $topics = $topics->where('category', $category);

        $topics = $topics->get();
        if ($userTopics)
            $topics->map(function ($topic) use ($userTopics)
                {
                    if ($userTopic = $userTopics->where('topic_id', $topic->id)->first()) {
                        if ($userTopic->passed_at)
                            $topic->alert = "Telah dilulusi";
                        if ($userTopic->certificate_no)
                            $topic->alert = "Unduh sertifikat";
                    }
                    return $topic;
                }
            );


        return fractal()
            ->collection($topics)
            ->transformWith(new TopicCatalogTransformer)
            ->respond();
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        return fractal()
            ->item(
                Topic::published()
                    ->where('slug', $slug)
                    ->first()
            )
            ->transformWith(new TopicCatalogTransformer('show'))
            ->respond();
    }

}
