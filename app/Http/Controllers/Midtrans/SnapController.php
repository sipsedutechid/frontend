<?php

namespace App\Http\Controllers\Midtrans;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Veritrans\Midtrans;

class SnapController extends Controller
{
    public function __construct()
    {
        Midtrans::$serverKey = env('MIDTRANS_SERVER');
        Midtrans::$isProduction = env('MIDTRANS_PRODUCTION', false);
    }

    public function token(Request $request)
    {
        $this->validate($request, [
            'topic' => 'required_if:type,certificate',
            'subscription' => 'required_if:type,topic',
            'type' => 'required|in:certificate,subscription',
            'method' => 'required|in:mt-cc,mt-ccinstallment',
        ]);

        $midtrans = new Midtrans;
        $expiration = \Carbon\Carbon::now()->addDays(2);
        
        $order = new \App\Accounts\SubscriptionOrder;
        $order->user_id = $request->user()->id;
        $order->order_no = \App\Accounts\SubscriptionOrder::generateOrderNo();
        $order->quantity = 1;
        $order->method = $request->method;
        $order->client_ip = $request->ip();
        $order->expired_at = $expiration;

        switch ($request->type) {
            case 'certificate':
                $topic = \App\Vault\Topic::where('slug', $request->topic)->firstOrFail();
                $userTopic = \App\Accounts\UserTopic::where('user_id', $request->user()->id)->where('topic_id', $topic->id)->firstOrFail();
                $order->user_topic_id = $userTopic->id;     
                $order->total = 200000;
                // Populate items
                $items = [[
                    'id'        => 'sert-' . $userTopic->id,
                    'price'     => 200000,
                    'quantity'  => $order->quantity,
                    'name'      => 'Sertifikat - ' . substr($userTopic->topic->title, 0, 37)
                ]];
                break;
            
            case 'subscription':
            default:
                $subscription = \App\Accounts\Subscription::find($request->subscription);
                $order->subscription_id = $subscription->id;
                $order->total = $subscription->price;

                $items = [[
                    'id'        => 'subs-' . $subscription->id,
                    'price'     => $subscription->price,
                    'quantity'  => $order->quantity,
                    'name'      => substr($subscription->label, 0, 37)
                ]];
                break;
        }

        $order->save();

        $transaction_details = [
            'order_id'      => $order->order_no,
            'gross_amount'  => $order->total
        ];

        // Populate customer's billing address
        $userNames = \App\Accounts\User::getFirstAndLastNames($request->user()->name);
        $billing_address = [
            'first_name' => $userNames['firstName'],
            'last_name' => $userNames['lastName'],
            'address' => 'Jl. Dr. Sam Ratulangi, No. 7/C9',
            'city' => 'Makassar',
            'postal_code' => '90113',
            'phone' => '04118111255',
            'country_code' => 'IDN'
        ];

        // Populate customer's Info
        $customer_details = [
            'first_name' => $userNames['firstName'],
            'last_name' => $userNames['lastName'],
            'email' => $request->user()->email,
            'phone' => '04118111255',
            'billing_address' => $billing_address,
            'shipping_address' => $billing_address
        ];

        $credit_card['secure'] = true;

        $custom_expiry = [
            'start_time' => \Carbon\Carbon::now()->format("Y-m-d H:i:s O"),
            'unit'       => 'hour',
            'duration'   => 2
        ];

        $transaction_data = [
            'transaction_details' => $transaction_details,
            'item_details' => $items,
            'customer_details' => $customer_details,
            'credit_card' => $credit_card,
            'expiry' => $custom_expiry
        ];

        try {
            $snap_token = $midtrans->getSnapToken($transaction_data);
            echo $snap_token;
        }
        catch (Exception $e) {
            return $e->getMessage;
        }
    }
}