<?php

namespace App\Http\Controllers\Backend;

use Artisan;
use Fractal;
use App\Vault\Topic;
use App\Accounts\Role;
use App\Accounts\User;
use App\Accounts\UserTopic;
use Illuminate\Http\Request;
use App\Events\UserEnrolled;
use App\Events\UserRegistered;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Transformers\RoleTransformer;
use App\Transformers\UserTransformer;

class UserController extends Controller
{
	public function index(Request $request)
	{
		$users = User::orderBy('created_at', 'desc')->with(['role', 'groups']);
		if ($request->term)
			$users->where('name', 'like', '%' . $request->term . '%')
				->orWhere('email', 'like', '%' . $request->term . '%');

		if ($request->wantsJson()) {
			return response()->json(
				Fractal::collection($users->get())
					->transformWith(new UserTransformer('index'))
				);
		}
	}

	public function show(Request $request, $email)
	{
		$user = User::where('email', $email)
			->with(['userTopics', 'userTopics.topic'])
			->firstOrFail();
    return response()->json([
      'user' => Fractal::item($user)->transformWith(new UserTransformer('single-admin'))->toArray()['data'],
      'availableRoles' => Fractal::collection(Role::all())->transformWith(new RoleTransformer)->toArray()['data']
      ]);
	}

	public function store(Request $request)
	{
		$this->validate($request, [
			'email' => 'required|email|unique:mysql_accounts.users',
			'name' => 'required|string',
			'role.id' => 'required|exists:mysql_accounts.roles,id'
		]);

		$user = new User;
		$user->email = $request->email;
		$user->name = clean($request->name, ['HTML.Allowed' => '']);
		$user->role_id = $request->input('role.id');
		$user->password = bcrypt(str_random(8));
		$user->verify_token = '';
		$user->save();

    event(new UserRegistered($user));
		return response()->json([
      'user' => Fractal::item($user)->transformWith(new UserTransformer('single-admin'))->toArray()['data'],
      'availableRoles' => Fractal::collection(Role::all())->transformWith(new RoleTransformer)->toArray()['data']
      ]);
	}

	public function update(Request $request, $email)
	{
		$user = User::where('email', $email)->firstOrFail();

		if (in_array($request->action, ['resetPassword', 'subscribe', 'unsubscribe', 'enrol'], true))
			return call_user_func(__NAMESPACE__ . '\UserController::' . $request->action, $user, $request);

		$this->validate($request, [
			'email' => ['required', 'email', Rule::unique('mysql_accounts.users')->ignore($user->id)],
			'name' => 'required|string',
			'nameOnCertificate' => 'required|string|max:30',
			'role.id' => 'required|exists:mysql_accounts.roles,id',
			'profession.slug' => 'in:doctor,dentist'
			]);

		$user->email = $request->email;
		$user->name = $request->name;
		$user->name_on_certificate = $request->nameOnCertificate;
		$user->role_id = $request->role['id'];
		$user->profession = $request->profession['slug'];
		$user->id_no = $request->idNo;
		$user->save();

		return $this->show($request, $user->email);
	}

	public function resetPassword($user)
	{
		return response()->json(['message' => "This function is under construction."]);
	}

	public function subscribe($user, $request)
	{
		Artisan::call('user:subscribe', ['email' => $user->email, 'plan' => $request->input('plan.slug')]);
		return response()->json(['message' => "success"]);
	}

	public function unsubscribe($user)
	{
		$subscription = $user->userSubscriptions()->active()->first();
		if (!$subscription) return;

		$subscription->unsubscribe();
		return response()->json(['message' => "success"]);
	}

	public function enrol($user, $request)
	{
		$topic = Topic::where('slug', $request->input('topic.slug'))->firstOrFail();
		if ($user->userTopics()->where('topic_id', $topic->id)->first()) return;

		$userTopic = new UserTopic;
		$userTopic->user_id = $user->id;
		$userTopic->topic_id = $topic->id;
		$userTopic->is_accessible = true;
		$userTopic->is_trial = false;
		$userTopic->save();
    event(new UserEnrolled($userTopic));

		return response()->json(['message' => "success"]);
	}

}
