<?php

namespace App\Http\Controllers\Backend;

use Fractal;
use App\Accounts\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\RoleTransformer;

class RoleController extends Controller
{
	public function index(Request $request)
  {
  	return response()->json(
  		Fractal::collection(Role::withCount('users')->get())->transformWith(new RoleTransformer)->toArray()
  	);
  }

  public function show(Role $role)
  {
  	return fractal()->item($role, new RoleTransformer('single'))->respond();
  }
}
