<?php

namespace App\Http\Controllers\Backend;

use Fractal;
use App\Vault\Topic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\TopicTransformer;

class TopicController extends Controller
{
  
	public function index(Request $request)
	{
		return response()->json(Fractal::collection(Topic::all())->transformWith(new TopicTransformer('index'))->toArray());
	}

}
