<?php

namespace App\Http\Controllers;

use App\Vault\Topic;
use App\Accounts\UserTopic;
use App\Transformers\TopicCatalogTransformer;
use App\Transformers\UserTopicTransformer;
use App\Events\UserEnrolled;
use Illuminate\Http\Request;
use Carbon\Carbon;

class P2KBController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('json')) {
            $topics = Topic::where('post_status', true)->orderBy('release', 'desc')->with(['userTopics', 'subscriptions']);
            if ($request->term)
                $topics->where('title', 'like', '%' . trim($request->term) . '%');

            $userSubscriptions = auth()->check() ? auth()->user()->userSubscriptions()->get() : [];

            return fractal()
                ->collection(
                    $topics->get()->map(function ($topic) use ($userSubscriptions)
                    {
                        // check subscription needs
                        $needSubscription = count($topic->subscriptions) > 0;
                        $topic->isSubscribe = !$needSubscription;

                        if (auth()->check()) {
                            $userTopic = $topic->userTopics->where('user_id', auth()->user()->id)->first();
                            if ($userTopic) $topic->passed = $userTopic->passed_at !== null;

                            if ($needSubscription) {
                                $sumSubscribe = 0;
                                //if topic need subscription to access, check user subscription
                                foreach ($topic->subscriptions as $topicSubscription) {
                                    $find = array_search($topicSubscription->subscription_id, array_column($userSubscriptions->toArray(), 'subscription_id'));
                                    $isExpire = $find !== false ? 
                                        !empty($userSubscriptions[$find]->expired_at) && 
                                        Carbon::now()->gt(Carbon::parse($userSubscriptions[$find]->expired_at)) : false;

                                    $sumSubscribe = $find !== false && !$isExpire ? $sumSubscribe + 1 : $sumSubscribe;  
                                }
                            }

                            $topic->isSubscribe = !$needSubscription ?: $sumSubscribe > 0;
                        }
                        return $topic;
                    })
                )
                ->transformWith(new TopicCatalogTransformer)
                ->respond();
        }

        return view('new.p2kb.home');
    }

    public function show(Request $request, $slug)
    {
        $topic = Topic::where('slug', $slug)->with(['sponsorContent.sponsor'])->firstOrFail();
        return view('new.p2kb.single', ['topic' => $topic]);
    }

    public function learn(Request $request, $slug)
    {
        $topic = Topic::where('slug', $slug)->firstOrFail();
		$userTopic = $request->user()->userTopics()->where('topic_id', $topic->id)->first();
		if (!$userTopic) {
            $userTopic = new UserTopic;
			$userTopic->user_id = $request->user()->id;
			$userTopic->topic_id = $topic->id;
			$userTopic->is_accessible = true;
			$userTopic->save();
        }
        event(new UserEnrolled($userTopic));
        if ($request->has('json')) {
            return fractal()->item($userTopic)->transformWith(new UserTopicTransformer('app-index'))->respond();
        }
        $activeSub = $request->user()->userSubscriptions()->active()->first();
        return view('new.p2kb.learn', [
            'userTopic' => fractal()->item($userTopic)->transformWith(new UserTopicTransformer('app-index'))->toArray()['data'],
            'remainingQuota' => $activeSub ? $activeSub->remaining_quota : 0
            ]);
    }
}
