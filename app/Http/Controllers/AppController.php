<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Accounts\User;
use Illuminate\Http\Request;

class AppController extends Controller
{

  public function __construct()
  {
  }

  public function index(Request $request)
  {
  	return response(view('app')->with(['promptEnableTrialTopic' => $this->getPromptEnableTrialTopic($request->user())]))
      ->header('X-Frame-Options', 'ALLOW-FROM ' . env('P2KB_URL') . '/');
  }

  public function getUserStatus(Request $request)
  {
    if (!auth()->check()) {
      return response()->json(['status' => 'unauthenticated']);
    }

    $response = [
      'userName' => auth()->user()->name,
      'userEmail' => auth()->user()->email,
      'userProfession' => auth()->user()->profession,
      'userIdNo' => auth()->user()->id_no,
      'status' => 'unsubscribed',
    ];

    if ($sub = auth()->user()->userSubscriptions()->active()->first()) {
      $response['subscription'] = $sub->toArray();
      $response['status'] = 'subscribed';
    }
    return response()->json($response);
  }

  public function getJWPlayerScript($videoUrl = false)
  {
    // $this->middleware('auth');

  	$playerKey = "jHONf2kl";
  	$secret = env('JWPLAYER_SECRET', '');
  	$expires = round((time() + 3600) / 300) * 300;

		$path = "libraries/" . $playerKey . ".js";
		$signature = md5($path . ":" . $expires . ":" . $secret);

    $data = [
			'playerKey' => $playerKey,
			'url' => "https://content.jwplatform.com/" . $path . "?sig=" . $signature . "&exp=" . $expires
    ];
    if ($videoUrl) {
      $path = "manifests/" . $videoUrl . ".m3u8";
      $signature = md5($path . ':' . $expires . ":" . $secret);
      $videoUrl = "https://content.jwplatform.com/" . $path . "?sig=" . $signature . "&exp=" . $expires;
      return view('video-player', array_merge($data, ['videoUrl' => $videoUrl]));
    }

		return response()->json($data);
  }

  public function getPromptEnableTrialTopic(User $user)
  {
    if ($user->isTrialEnableEligible()) {
      $activeSubscription = $user->userSubscriptions()->where('is_trial', true)->active()->first();
      $passedTopics = $user->userTopics()->where('is_trial', true)->where('is_accessible', true)
        ->whereDate('passed_at', '>=', Carbon::parse($activeSubscription->starts_at))
        ->whereDate('passed_at', '<=', Carbon::parse($activeSubscription->expired_at))
        ->get();
      return fractal()->collection($passedTopics)->transformWith(new \App\Transformers\UserTopicTransformer('index'))->toArray()['data'];
    }

    return false;
  }

}
