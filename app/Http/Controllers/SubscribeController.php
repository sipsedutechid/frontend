<?php

namespace App\Http\Controllers;

use App\Mail\SubscriptionActive;
use Auth;
use Illuminate\Http\Request;

class SubscribeController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');

	}

	public function index(Request $request)
	{
		$activeSubscription = $request->user()->userSubscriptions()->active()->first();
		if ($activeSubscription)
			return view('subscribe-restrict', [
				'subscription' => $activeSubscription,
				'title' => page_title('Berlangganan')
				]);
		if (!$request->user()->profession || !$request->user()->id_no)
			return redirect(route('profile'));

		return view('pay.checkout', [
			'subscriptions' => \App\Accounts\Subscription::notCustom()->get(),
			'title' => page_title('Berlangganan')
			]);
	}

	public function redeem(Request $request)
	{
		if ($request->user()->userSubscriptions()->active()->first())
			return redirect(route('subscribe'));

		$subscription = \App\Accounts\Subscription::where('redeem_code', $request->code)->first();
		if ($subscription) {
			$user = $request->user();
			$userSubscription = new \App\Accounts\UserSubscription;
			$userSubscription->user_id = $user->id;
			$userSubscription->subscription_id = $subscription->id;
			$userSubscription->starts_at = \Carbon\Carbon::now()->subSeconds(1);
			$userSubscription->is_trial = $subscription->is_trial;

			$expireMethod = 'add' . ucfirst($subscription->period);
			$userSubscription->expired_at = \Carbon\Carbon::now()->$expireMethod($subscription->duration);
			$userSubscription->save();

			$currentTopicIndex = \App\Accounts\AppSetting::find('topic.current_index_' . ($user->profession ?: 'doctor'))->value;
			$topic = \App\Vault\Topic::where('category', $user->profession ?: 'doctor')->where('release', $currentTopicIndex)->first();
			if (\App\Accounts\UserTopic::where('user_id', $user->id)->where('topic_id', $topic->id)->count() == 0) {
				$subscribeTopic = new \App\Accounts\UserTopic;
				$subscribeTopic->topic_id = $topic->id;
				$subscribeTopic->is_accessible = true;
				$subscribeTopic->is_trial = $subscription->is_trial;
				$user->userTopics()->save($subscribeTopic);
			}
      Mail::queue(new SubscriptionActive($userSubscription));

			return redirect(route('profile'));
		}
		$request->session()->flash('alert-danger', 'Kode tersebut tidak dapat kami temukan.');
		return redirect(route('subscribe'));
	}

	public function redeemCertificate(Request $request)
	{
		$activeSub = $request->user()->userSubscriptions()->active()->first();
		if (!$activeSub)
			return abort(403);
		if ($activeSub->remaining_quota < 1)
			return abort(403);

		$topic = \App\Vault\Topic::where('slug', $request->topic)->firstOrFail();
		$userTopic = $topic->userTopics()->where('user_id', $request->user()->id)->firstOrFail();

		$userTopic->certificate_no = $userTopic->generateCertificateNumber();
		$userTopic->save();
		$activeSub->remaining_quota = $activeSub->remaining_quota - 1;
		$activeSub->save();

		return response()->json(fractal()->item($userTopic)->transformWith(new \App\Transformers\UserTopicTransformer('app-index'))->toArray()['data']);
	}

}
