<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EventController extends Controller
{
    public function index(Request $request)
    {
        return view('new/event/list', [
            'events' => \App\Vault\Event::orderBy('end_at', 'desc')->take(10)->get()
		]);
    }

    public function single(Request $request)
    {
        return view('new/event/single', [
            'event' => \App\Vault\Event::where('slug', $request->slug)->first() ,
            'upcomingEvents' => \App\Vault\Event::where('slug', '<>',$request->slug)->take(4)->get()
		]);
    }
}
