<?php

namespace App\Http\Controllers;

use App\Transformers\DrugTransformer;
use App\Vault\Drug;
use App\Vault\DrugClass;
use App\Vault\DrugManufacturer;
use Fractal;
use Illuminate\Http\Request;

class DrugController extends Controller
{

	public function index(Request $request)
  {
    if ($request->has('json')) {
      $drugs = Drug::with(['manufacturer']);

      if ($request->term)
        $drugs = $drugs->where('name', 'like', '%' . $request->term . '%');
      if ($request->filterClass)
        $drugs = $drugs->whereHas('drugClass', function ($query) use ($request)
        {
          return $query->where('code', $request->filterClass);
        });
      $drugs = $drugs->orderBy('name')->paginate(18);
      $drugs->setCollection(collect(Fractal::collection($drugs->getCollection())->transformWith(new DrugTransformer)->toArray()['data']));
      return response()->json($drugs);
    }

    return view('new.drugs.home', ['classes' => DrugClass::orderBy('name')->get()->map(function ($cat)
		{
			return ['slug' => $cat->code, 'name' => $cat->name];
		})->all()]);
  }

  public function single(Request $request, $slug)
  {
    $drug = Drug::where('slug', $slug)->firstOrFail();
    return view('new.drugs.single', ['drug' => fractal()->item($drug)->transformWith(new DrugTransformer(true))->toArray()]);
  }

  public function related(Request $request, $slug)
  {
    $drug = Drug::where('slug', $slug)->firstOrFail();
    $subclassDrugs = $drug->drugSubclass->drugs()
      ->with(['manufacturer', 'drugSubclass'])
      ->where('slug', '<>', $slug)
      ->orderBy('name')
      ->get();
    $subclassDrugs = $subclassDrugs->random($subclassDrugs->count() > 10 ? 10 : $subclassDrugs->count());
    $manufacturerDrugs = $drug->manufacturer->drugs()
      ->with(['drugSubclass', 'manufacturer'])
      ->where('slug', '<>', $slug)
      ->orderBy('name')
      ->get();
    $manufacturerDrugs = $manufacturerDrugs->random($manufacturerDrugs->count() > 10 ? 10 : $manufacturerDrugs->count());

    return response()->json([
      'subclass' => Fractal::collection($subclassDrugs)->transformWith(new DrugTransformer)->toArray()['data'],
      'manufacturer' => Fractal::collection($manufacturerDrugs)->transformWith(new DrugTransformer)->toArray()['data'],
      ]);
  }

  public function manufacturers(Request $request)
  {
    return response()->json(DrugManufacturer::orderBy('name', 'asc')->get());
  }
}
