<?php

namespace App\Http\Controllers;

use Fractal;
use App\Accounts\Role;
use App\Accounts\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Transformers\RoleTransformer;
use App\Transformers\UserTransformer;
use League\Fractal\Pagination\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class UserController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth.admin');
	}

  public function index(Request $request)
  {
		$users = User::orderBy('created_at', 'desc');
    if ($request->term)
      $users->where('name', 'like', '%' . $request->term . '%')
        ->orWhere('email', 'like', '%' . $request->term . '%');
    $users = $users->paginate($request->perPage ?: 15)->appends(['term' => $request->term]);

    if ($request->wantsJson()) {
  		return response()->json(
  			Fractal::collection($users)
  				->transformWith(new UserTransformer('list'))
  				->paginateWith(new IlluminatePaginatorAdapter($users))
  			);
  	}
  }

  public function single(Request $request, $email)
  {
    $user = User::where('email', $email)->firstOrFail();
    return response()->json([
      'user' => Fractal::item($user)->transformWith(new UserTransformer('single-admin'))->toArray()['data'],
      'availableRoles' => Fractal::collection(Role::all())->transformWith(new RoleTransformer)->toArray()['data']
      ]);
  }

  public function patch(Request $request, $email)
  {
    $user = User::where('email', $email)->firstOrFail();
    $this->validate($request, [
      'name' => 'required|string',
      'nameOnCertificate' => 'required|string|max:30',
      'email' => ['required', 'email', Rule::unique('mysql_accounts.users')->ignore($user->id)],
      'role' => 'integer|exists:mysql_accounts.roles,id'
      ]);

    $user->name = $request->name;
    $user->email = $request->email;
    $user->role_id = $request->role;
    $user->name_on_certificate = $request->nameOnCertificate;
    $user->save();

    if ($user->email != $email)
      return response()->json(['refresh' => true]);
  }

  public function patchAction(Request $request, $email, $action)
  {
    if (!in_array($action, ['reset-password'], true))
      return abort(404);
    $user = User::where('email', $email)->firstOrFail();
    $function = camel_case('patch-' . $action);
    return $this->$function($user);
  }

  public function patchResetPassword($user)
  {
    
  }
}
