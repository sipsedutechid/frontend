<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Mail;

use App\Veritrans\Midtrans;
use App\BNI\ECollection;

class TransactionController extends Controller
{
    public function __construct()
    {
        Midtrans::$serverKey = env('MIDTRANS_SERVER');
        Midtrans::$isProduction = env('MIDTRANS_PRODUCTION', false);
        ECollection::$clientKey = env('BNI_ECOLLECTION_CLIENT');
        ECollection::$secretKey = env('BNI_ECOLLECTION_SECRET');
        ECollection::$isProduction = env('BNI_ECOLLECTION_PRODUCTION', false);
    }

    public function handleMidtransCallback()
    {
        $midtrans = new Midtrans;
        $jsonResult = file_get_contents('php://input');
        $result = json_decode($jsonResult);

        if ($result) {
            $notif = $midtrans->status($result->order_id);
        }

        $order = \App\Accounts\SubscriptionOrder::where('order_no', $result->order_id)->firstOrFail();

        $transaction = $notif->transaction_status;
        $type = $notif->payment_type;
        $order_id = $notif->order_id;
        $fraud = $notif->fraud_status;

        if ($transaction == 'capture' || ($transaction == 'settlement' && $type == 'gopay')) {
            if ($type == 'credit_card' || $type == 'gopay') {
                if ($fraud == 'challenge') {
                    $order->status = 'challenge';
                    // TODO send email to admins
                }
                else {
                    if ($order->user_topic_id) {
                        $userTopic = $order->userTopic;
                        $certNo = $userTopic->generateCertificateNumber();
                        $userTopic->certificate_no = $certNo;
                        $userTopic->save();   
                    } else {
                        $subscription = $order->subscription()->get();
                        $userSubscription = new \App\Accounts\UserSubscription;
                        $userSubscription->user_id = $order->user_id;
                        $userSubscription->subscription_id = $order->subscription->id;
                        $userSubscription->starts_at = \Carbon\Carbon::now()->subSeconds(1);
                        $userSubscription->is_trial = $subscription->is_trial;

                        $expireMethod = 'add' . ucfirst($subscription->period);
                        if ($subscription->duration)
                            $userSubscription->expired_at = \Carbon\Carbon::now()->$expireMethod($subscription->duration); 
                        $userSubscription->save();

                        Mail::queue(new \App\Mail\SubscriptionActive($userSubscription));
                    }

                    $order->status = 'success';
                    // if ($userSubscription = $order->intoSubscription()) {
                    //     Mail::queue(new \App\Mail\SubscriptionActive($userSubscription));
                    // }
                }
            }
        }
        else if ($transaction == 'settlement') {
            $order->status = 'settled';
            $order->settled_at = \Carbon\Carbon::now();
        }
        else if ($transaction == 'pending') {
            $order->status = 'pending';
        }
        else if ($transaction == 'deny') {
            $order->status = 'denied';
        }

        $order->save();
    }

    public function handleBNICallback(Request $request)
    {
        $jsonResult = file_get_contents('php://input');
        $result = json_decode($jsonResult);

        if (!$result) {
            return response()->json(['status' => '999', 'message' => 'Fatal error']);
        }
        if ($result->client_id != ECollection::$clientKey) {
            return response()->json(['status' => '999', 'message' => 'Fatal error']);
        }

        $result = ECollection::parseData($result->data);
        $order = \App\Accounts\SubscriptionOrder::where('va_no', $result['virtual_account'])->first();
        if (!$order) {
            return json_encode(['status' => '400']);
        }

        if ($order->total == $result['cumulative_payment_amount']) {
            if ($userSubscription = $order->intoSubscription()) {
                Mail::queue(new \App\Mail\SubscriptionActive($userSubscription));
            }
            $order->status = 'settled';
            $order->settled_at = \Carbon\Carbon::now();
            $order->save();
        }
        return json_encode(['status' => '000']);
    }

    public function done(Request $request)
    {
        $result = json_decode($request->result_data);
        if ($request->payment_method == 'midtrans') {
            switch ($result->status_code) {
                case '200': $status = 'success'; break;
                case '201': $status = 'pending'; break;
                case '202': $status = 'denied'; break;
                default: $status = 'error'; break;
            }
            return view('pay.done', [
                'method' => $request->payment_method,
                'status' => $status
                ]);
        } else {
            return view('pay.done', [
                'method' => $request->payment_method,
                'status' => $request->result_type,
                'vaNo' => $result->va_no,
                'totalAmount' => $result->total_amount
            ]);
        }
    }

}