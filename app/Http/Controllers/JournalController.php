<?php

namespace App\Http\Controllers;

use App\Transformers\JournalTransformer;
use App\Vault\Journal;
use App\Vault\JournalCategory;
use Fractal;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;

class JournalController extends Controller
{

	public function index(Request $request)
	{
		if ($request->has('json')) {
			$journals = Journal::published()->with('category');
			if ($request->term)
				$journals->where('title', 'like', '%' . $request->term . '%');
			if ($request->category)
				$journals->whereHas('category', function ($query) use ($request)
				{
					$query->where('slug', $request->category);
				});

			$journals = $journals->orderBy('title')->paginate(20);
			$journals->setCollection(collect(Fractal::collection($journals->getCollection())->transformWith(new JournalTransformer)->toArray()['data']));
			return response()->json($journals);
		}

		return view('new.journals.home', [
			'categories' =>
				JournalCategory::orderBy('labels')
					->whereHas('journals', function ($query)
					{
						$query->where('post_status', true);
					})
					->get()
					->map(function ($cat)
					{
						return ['slug' => $cat->slug, 'label' => $cat->labels];
					})->all()
			]);
	}

	public function single(Request $request, $slug)
	{
		return view('new.journals.single', ['journal' => Journal::where('slug', $slug)->firstOrFail()]);
	}

	public function getTicketedURL(Request $request, $slug)
	{
		$journal = Journal::where('slug', $slug)->firstOrFail();
		$url = $journal->getTicketedURL($request->user());
		if (!$url)
			return abort(403, 'Unauthenticated');

		return response($url);
	}


}
